// AUTO GENERATED LIBRARY, DO NOT EDIT BY HAND.

library rpc_models;

import 'package:sv_models/shared_models.dart';

part 'src/rpc_models/accounts.dart';
part 'src/rpc_models/service_accounts.dart';
part 'src/rpc_models/apps.dart';
part 'src/rpc_models/certs.dart';
part 'src/rpc_models/containers.dart';
part 'src/rpc_models/disks.dart';
part 'src/rpc_models/envs.dart';
part 'src/rpc_models/orgs.dart';
part 'src/rpc_models/pools.dart';
part 'src/rpc_models/scms.dart';
part 'src/rpc_models/services.dart';
part 'src/rpc_models/service_users.dart';
part 'src/rpc_models/status.dart';

