// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class DomainLogic extends DomainCore {
  // Add custom logic here
}
*/

class DomainCore extends Object with Id {
  String cert_id = "";
  bool verified = false;
  String tls_compatibility_mode = "";
  String domain = "";
  String subdomain = "";
}

class Domain extends DomainLogic {
  String cert_id = "";
  bool verified = false;
  String tls_compatibility_mode = "";
  String domain = "";
  String subdomain = "";

  Domain();

  Domain.fromJson(Map j) {
    cert_id = j['cert_id'] ?? "";
    verified = j['verified'] ?? false;
    tls_compatibility_mode = j['tls_compatibility_mode'] ?? "";
    domain = j['domain'] ?? "";
    subdomain = j['subdomain'] ?? "";
  }

  Map toJson() {
    return {
      'cert_id': cert_id ?? "",
      'verified': verified ?? false,
      'tls_compatibility_mode': tls_compatibility_mode ?? "",
      'domain': domain ?? "",
      'subdomain': subdomain ?? "",
    };
  }
}

