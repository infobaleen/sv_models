// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OperationLogic extends OperationCore {
  // Add custom logic here
}
*/

class OperationCore extends Object with Id {
  String operation_id = "";
  RpcAuthContext context = new RpcAuthContext();
  ResourceIds resource_ids = new ResourceIds();
  DateTime start = new DateTime(0).toUtc();
  DateTime end = new DateTime(0).toUtc();
  String state = "";
  int status_code = 0;
  String status_phrase = "";
}

class Operation extends OperationLogic {
  String operation_id = "";
  RpcAuthContext context = new RpcAuthContext();
  ResourceIds resource_ids = new ResourceIds();
  DateTime start = new DateTime(0).toUtc();
  DateTime end = new DateTime(0).toUtc();
  String state = "";
  int status_code = 0;
  String status_phrase = "";

  Operation();

  Operation.fromJson(Map j) {
    operation_id = j['operation_id'] ?? "";
    context = new RpcAuthContext.fromJson(j['context']) ?? new RpcAuthContext();
    resource_ids = new ResourceIds.fromJson(j['resource_ids']) ?? new ResourceIds();
    start = (j['start'] != null) ? DateTime.parse(j['start']).toUtc() : new DateTime(0).toUtc();
    end = (j['end'] != null) ? DateTime.parse(j['end']).toUtc() : new DateTime(0).toUtc();
    state = j['state'] ?? "";
    status_code = j['status_code'] ?? 0;
    status_phrase = j['status_phrase'] ?? "";
  }

  Map toJson() {
    return {
      'operation_id': operation_id ?? "",
      'context': context ?? new RpcAuthContext(),
      'resource_ids': resource_ids ?? new ResourceIds(),
      'start': (start != null) ? start.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'end': (end != null) ? end.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'state': state ?? "",
      'status_code': status_code ?? 0,
      'status_phrase': status_phrase ?? "",
    };
  }
}

