// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ScmAccountLogic extends ScmAccountCore {
  // Add custom logic here
}
*/

class ScmAccountCore extends Object with Id {
  String scm_account_id = "";
  String provider = "";
  bool authenticated = false;
  String username = "";
  List<String> scopes = new List<String>();
  DateTime expires = new DateTime(0).toUtc();
}

class ScmAccount extends ScmAccountLogic {
  String scm_account_id = "";
  String provider = "";
  bool authenticated = false;
  String username = "";
  List<String> scopes = new List<String>();
  DateTime expires = new DateTime(0).toUtc();

  ScmAccount();

  ScmAccount.fromJson(Map j) {
    scm_account_id = j['scm_account_id'] ?? "";
    provider = j['provider'] ?? "";
    authenticated = j['authenticated'] ?? false;
    username = j['username'] ?? "";
    j['scopes']?.forEach((val) => scopes.add(val)); // NOTE: Newly changed for Dart 2.0
    expires = (j['expires'] != null) ? DateTime.parse(j['expires']).toUtc() : new DateTime(0).toUtc();
  }

  Map toJson() {
    return {
      'scm_account_id': scm_account_id ?? "",
      'provider': provider ?? "",
      'authenticated': authenticated ?? false,
      'username': username ?? "",
      'scopes': scopes ?? new List<String>(),
      'expires': (expires != null) ? expires.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
    };
  }
}

