// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ScmRepoLogic extends ScmRepoCore {
  // Add custom logic here
}
*/

class ScmRepoCore extends Object with Id {
  String provider = "";
  String owner = "";
  String name = "";
  String full_name = "";
  String language = "";
  bool fork = false;
  String organization = "";
  bool private = false;
  String description = "";
}

class ScmRepo extends ScmRepoLogic {
  String provider = "";
  String owner = "";
  String name = "";
  String full_name = "";
  String language = "";
  bool fork = false;
  String organization = "";
  bool private = false;
  String description = "";

  ScmRepo();

  ScmRepo.fromJson(Map j) {
    provider = j['provider'] ?? "";
    owner = j['owner'] ?? "";
    name = j['name'] ?? "";
    full_name = j['full_name'] ?? "";
    language = j['language'] ?? "";
    fork = j['fork'] ?? false;
    organization = j['organization'] ?? "";
    private = j['private'] ?? false;
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'owner': owner ?? "",
      'name': name ?? "",
      'full_name': full_name ?? "",
      'language': language ?? "",
      'fork': fork ?? false,
      'organization': organization ?? "",
      'private': private ?? false,
      'description': description ?? "",
    };
  }
}

