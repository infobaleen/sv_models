// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ServiceUserLogic extends ServiceUserCore {
  // Add custom logic here
}
*/

class ServiceUserCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  bool system_user = false;
  String service_username = "";
  String service_password = "";
  String description = "";
}

class ServiceUser extends ServiceUserLogic {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  bool system_user = false;
  String service_username = "";
  String service_password = "";
  String description = "";

  ServiceUser();

  ServiceUser.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    system_user = j['system_user'] ?? false;
    service_username = j['service_username'] ?? "";
    service_password = j['service_password'] ?? "";
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'system_user': system_user ?? false,
      'service_username': service_username ?? "",
      'service_password': service_password ?? "",
      'description': description ?? "",
    };
  }
}

