// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class StaticSettingsLogic extends StaticSettingsCore {
  // Add custom logic here
}
*/

class StaticSettingsCore extends Object with Id {
  List<Handler> server = new List<Handler>();
  List<Handler> client = new List<Handler>();
  List<Handler> error = new List<Handler>();
  String index = "";
}

class StaticSettings extends StaticSettingsLogic {
  List<Handler> server = new List<Handler>();
  List<Handler> client = new List<Handler>();
  List<Handler> error = new List<Handler>();
  String index = "";

  StaticSettings();

  StaticSettings.fromJson(Map j) {
    j['server']?.map((item) => new Handler.fromJson(item))?.forEach(server.add);
    j['client']?.map((item) => new Handler.fromJson(item))?.forEach(client.add);
    j['error']?.map((item) => new Handler.fromJson(item))?.forEach(error.add);
    index = j['index'] ?? "";
  }

  Map toJson() {
    return {
      'server': server?.map((item) => item.toJson())?.toList() ?? new List<Handler>(),
      'client': client?.map((item) => item.toJson())?.toList() ?? new List<Handler>(),
      'error': error?.map((item) => item.toJson())?.toList() ?? new List<Handler>(),
      'index': index ?? "",
    };
  }
}

