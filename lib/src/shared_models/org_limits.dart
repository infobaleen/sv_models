// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OrgLimitsLogic extends OrgLimitsCore {
  // Add custom logic here
}
*/

class OrgLimitsCore extends Object with Id {
  int max_members_per_org = 0;
  int max_pools_per_org = 0;
  int max_service_accounts_per_org = 0;
  int max_su_per_org = 0;
  int max_gb_per_org = 0;
}

class OrgLimits extends OrgLimitsLogic {
  int max_members_per_org = 0;
  int max_pools_per_org = 0;
  int max_service_accounts_per_org = 0;
  int max_su_per_org = 0;
  int max_gb_per_org = 0;

  OrgLimits();

  OrgLimits.fromJson(Map j) {
    max_members_per_org = j['max_members_per_org'] ?? 0;
    max_pools_per_org = j['max_pools_per_org'] ?? 0;
    max_service_accounts_per_org = j['max_service_accounts_per_org'] ?? 0;
    max_su_per_org = j['max_su_per_org'] ?? 0;
    max_gb_per_org = j['max_gb_per_org'] ?? 0;
  }

  Map toJson() {
    return {
      'max_members_per_org': max_members_per_org ?? 0,
      'max_pools_per_org': max_pools_per_org ?? 0,
      'max_service_accounts_per_org': max_service_accounts_per_org ?? 0,
      'max_su_per_org': max_su_per_org ?? 0,
      'max_gb_per_org': max_gb_per_org ?? 0,
    };
  }
}

