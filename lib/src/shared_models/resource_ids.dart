// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ResourceIdsLogic extends ResourceIdsCore {
  // Add custom logic here
}
*/

class ResourceIdsCore extends Object with Id {
  String org_id = "";
  String service_account_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String disk_id = "";
  String cert_id = "";
  String invoice_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";
}

class ResourceIds extends ResourceIdsLogic {
  String org_id = "";
  String service_account_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String disk_id = "";
  String cert_id = "";
  String invoice_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";

  ResourceIds();

  ResourceIds.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
    cert_id = j['cert_id'] ?? "";
    invoice_id = j['invoice_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'disk_id': disk_id ?? "",
      'cert_id': cert_id ?? "",
      'invoice_id': invoice_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
    };
  }
}

