// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OrgUsageLogic extends OrgUsageCore {
  // Add custom logic here
}
*/

class OrgUsageCore extends Object with Id {
  List<BillingUsage> container_usage = new List<BillingUsage>();
  List<BillingUsage> storage_usage = new List<BillingUsage>();
  List<BillingUsage> bandwidth_usage = new List<BillingUsage>();
  List<BillingUsage> support_usage = new List<BillingUsage>();
}

class OrgUsage extends OrgUsageLogic {
  List<BillingUsage> container_usage = new List<BillingUsage>();
  List<BillingUsage> storage_usage = new List<BillingUsage>();
  List<BillingUsage> bandwidth_usage = new List<BillingUsage>();
  List<BillingUsage> support_usage = new List<BillingUsage>();

  OrgUsage();

  OrgUsage.fromJson(Map j) {
    j['container_usage']?.map((item) => new BillingUsage.fromJson(item))?.forEach(container_usage.add);
    j['storage_usage']?.map((item) => new BillingUsage.fromJson(item))?.forEach(storage_usage.add);
    j['bandwidth_usage']?.map((item) => new BillingUsage.fromJson(item))?.forEach(bandwidth_usage.add);
    j['support_usage']?.map((item) => new BillingUsage.fromJson(item))?.forEach(support_usage.add);
  }

  Map toJson() {
    return {
      'container_usage': container_usage?.map((item) => item.toJson())?.toList() ?? new List<BillingUsage>(),
      'storage_usage': storage_usage?.map((item) => item.toJson())?.toList() ?? new List<BillingUsage>(),
      'bandwidth_usage': bandwidth_usage?.map((item) => item.toJson())?.toList() ?? new List<BillingUsage>(),
      'support_usage': support_usage?.map((item) => item.toJson())?.toList() ?? new List<BillingUsage>(),
    };
  }
}

