// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class AppLogic extends AppCore {
  // Add custom logic here
}
*/

class AppCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String disk_id = "";
  String name = "";
  bool maintenance_mode = false;
  bool stage_domain_update = false;
  Domain platform_domain = new Domain();
  Domain custom_domain = new Domain();
  AppCheck liveness_check = new AppCheck();
  AppCheck readiness_check = new AppCheck();
  Map<String, String> env_vars = new Map<String, String>();
  String description = "";
  List<Env> envs = new List<Env>();
  List<String> disk_ids = new List<String>();
}

class App extends AppLogic {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String disk_id = "";
  String name = "";
  bool maintenance_mode = false;
  bool stage_domain_update = false;
  Domain platform_domain = new Domain();
  Domain custom_domain = new Domain();
  AppCheck liveness_check = new AppCheck();
  AppCheck readiness_check = new AppCheck();
  Map<String, String> env_vars = new Map<String, String>();
  String description = "";
  List<Env> envs = new List<Env>();
  List<String> disk_ids = new List<String>();

  App();

  App.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
    name = j['name'] ?? "";
    maintenance_mode = j['maintenance_mode'] ?? false;
    stage_domain_update = j['stage_domain_update'] ?? false;
    platform_domain = new Domain.fromJson(j['platform_domain']) ?? new Domain();
    custom_domain = new Domain.fromJson(j['custom_domain']) ?? new Domain();
    liveness_check = new AppCheck.fromJson(j['liveness_check']) ?? new AppCheck();
    readiness_check = new AppCheck.fromJson(j['readiness_check']) ?? new AppCheck();
    j['env_vars']?.forEach((key, val) => env_vars[key] = val); // NOTE: Newly changed for Dart 2.0
    description = j['description'] ?? "";
    j['envs']?.map((item) => new Env.fromJson(item))?.forEach(envs.add);
    j['disk_ids']?.forEach((val) => disk_ids.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'disk_id': disk_id ?? "",
      'name': name ?? "",
      'maintenance_mode': maintenance_mode ?? false,
      'stage_domain_update': stage_domain_update ?? false,
      'platform_domain': platform_domain ?? new Domain(),
      'custom_domain': custom_domain ?? new Domain(),
      'liveness_check': liveness_check ?? new AppCheck(),
      'readiness_check': readiness_check ?? new AppCheck(),
      'env_vars': env_vars ?? new Map<String, String>(),
      'description': description ?? "",
      'envs': envs?.map((item) => item.toJson())?.toList() ?? new List<Env>(),
      'disk_ids': disk_ids ?? new List<String>(),
    };
  }
}

