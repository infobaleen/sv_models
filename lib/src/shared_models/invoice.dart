// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class InvoiceLogic extends InvoiceCore {
  // Add custom logic here
}
*/

class InvoiceCore extends Object with Id {
  String org_id = "";
  String invoice_id = "";
  DateTime invoice_date = new DateTime(0).toUtc();
  DateTime invoice_from = new DateTime(0).toUtc();
  DateTime invoice_to = new DateTime(0).toUtc();
  bool invoice_paid = false;
  bool invoice_forgiven = false;
  bool tax_exempt = false;
  bool valid_vat_id = false;
  String vat_id = "";
  num vat_rate = 0;
  int amount_in_usd_bp = 0;
  int amount_in_usd_cents_rounded = 0;
  OrgUsage usage = new OrgUsage();
}

class Invoice extends InvoiceLogic {
  String org_id = "";
  String invoice_id = "";
  DateTime invoice_date = new DateTime(0).toUtc();
  DateTime invoice_from = new DateTime(0).toUtc();
  DateTime invoice_to = new DateTime(0).toUtc();
  bool invoice_paid = false;
  bool invoice_forgiven = false;
  bool tax_exempt = false;
  bool valid_vat_id = false;
  String vat_id = "";
  num vat_rate = 0;
  int amount_in_usd_bp = 0;
  int amount_in_usd_cents_rounded = 0;
  OrgUsage usage = new OrgUsage();

  Invoice();

  Invoice.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    invoice_id = j['invoice_id'] ?? "";
    invoice_date = (j['invoice_date'] != null) ? DateTime.parse(j['invoice_date']).toUtc() : new DateTime(0).toUtc();
    invoice_from = (j['invoice_from'] != null) ? DateTime.parse(j['invoice_from']).toUtc() : new DateTime(0).toUtc();
    invoice_to = (j['invoice_to'] != null) ? DateTime.parse(j['invoice_to']).toUtc() : new DateTime(0).toUtc();
    invoice_paid = j['invoice_paid'] ?? false;
    invoice_forgiven = j['invoice_forgiven'] ?? false;
    tax_exempt = j['tax_exempt'] ?? false;
    valid_vat_id = j['valid_vat_id'] ?? false;
    vat_id = j['vat_id'] ?? "";
    vat_rate = j['vat_rate'] ?? 0;
    amount_in_usd_bp = j['amount_in_usd_bp'] ?? 0;
    amount_in_usd_cents_rounded = j['amount_in_usd_cents_rounded'] ?? 0;
    usage = new OrgUsage.fromJson(j['usage']) ?? new OrgUsage();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'invoice_id': invoice_id ?? "",
      'invoice_date': (invoice_date != null) ? invoice_date.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'invoice_from': (invoice_from != null) ? invoice_from.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'invoice_to': (invoice_to != null) ? invoice_to.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'invoice_paid': invoice_paid ?? false,
      'invoice_forgiven': invoice_forgiven ?? false,
      'tax_exempt': tax_exempt ?? false,
      'valid_vat_id': valid_vat_id ?? false,
      'vat_id': vat_id ?? "",
      'vat_rate': vat_rate ?? 0,
      'amount_in_usd_bp': amount_in_usd_bp ?? 0,
      'amount_in_usd_cents_rounded': amount_in_usd_cents_rounded ?? 0,
      'usage': usage ?? new OrgUsage(),
    };
  }
}

