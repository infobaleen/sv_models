// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class PoolLogic extends PoolCore {
  // Add custom logic here
}
*/

class PoolCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  int pool_type = 0;
  String name = "";
  String pooldb_service_id = "";
  String monogdb_service_id = "";
  PoolLimits system_pool_limits = new PoolLimits();
  PoolLimits custom_pool_limits = new PoolLimits();
  String description = "";
}

class Pool extends PoolLogic {
  String org_id = "";
  String pool_id = "";
  int pool_type = 0;
  String name = "";
  String pooldb_service_id = "";
  String monogdb_service_id = "";
  PoolLimits system_pool_limits = new PoolLimits();
  PoolLimits custom_pool_limits = new PoolLimits();
  String description = "";

  Pool();

  Pool.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    pool_type = j['pool_type'] ?? 0;
    name = j['name'] ?? "";
    pooldb_service_id = j['pooldb_service_id'] ?? "";
    monogdb_service_id = j['monogdb_service_id'] ?? "";
    system_pool_limits = new PoolLimits.fromJson(j['system_pool_limits']) ?? new PoolLimits();
    custom_pool_limits = new PoolLimits.fromJson(j['custom_pool_limits']) ?? new PoolLimits();
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'pool_type': pool_type ?? 0,
      'name': name ?? "",
      'pooldb_service_id': pooldb_service_id ?? "",
      'monogdb_service_id': monogdb_service_id ?? "",
      'system_pool_limits': system_pool_limits ?? new PoolLimits(),
      'custom_pool_limits': custom_pool_limits ?? new PoolLimits(),
      'description': description ?? "",
    };
  }
}

