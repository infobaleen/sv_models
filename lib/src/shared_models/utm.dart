// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class UtmLogic extends UtmCore {
  // Add custom logic here
}
*/

class UtmCore extends Object with Id {
  String source = "";
  String medium = "";
  String campaign = "";
  String term = "";
  String content = "";
}

class Utm extends UtmLogic {
  String source = "";
  String medium = "";
  String campaign = "";
  String term = "";
  String content = "";

  Utm();

  Utm.fromJson(Map j) {
    source = j['source'] ?? "";
    medium = j['medium'] ?? "";
    campaign = j['campaign'] ?? "";
    term = j['term'] ?? "";
    content = j['content'] ?? "";
  }

  Map toJson() {
    return {
      'source': source ?? "",
      'medium': medium ?? "",
      'campaign': campaign ?? "",
      'term': term ?? "",
      'content': content ?? "",
    };
  }
}

