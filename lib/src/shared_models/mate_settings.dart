// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class MateSettingsLogic extends MateSettingsCore {
  // Add custom logic here
}
*/

class MateSettingsCore extends Object with Id {
  int su = 0;
  int count = 0;
  String build_id = "";
}

class MateSettings extends MateSettingsLogic {
  int su = 0;
  int count = 0;
  String build_id = "";

  MateSettings();

  MateSettings.fromJson(Map j) {
    su = j['su'] ?? 0;
    count = j['count'] ?? 0;
    build_id = j['build_id'] ?? "";
  }

  Map toJson() {
    return {
      'su': su ?? 0,
      'count': count ?? 0,
      'build_id': build_id ?? "",
    };
  }
}

