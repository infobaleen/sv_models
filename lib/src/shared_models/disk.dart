// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class DiskLogic extends DiskCore {
  // Add custom logic here
}
*/

class DiskCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";
  String disk_type = "";
  String name = "";
  Map<String, String> zfs_properties = new Map<String, String>();
  String disk_dir = "";
  bool disk_rw = false;
  String description = "";
  ZfsStats stats = new ZfsStats();
}

class Disk extends DiskLogic {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";
  String disk_type = "";
  String name = "";
  Map<String, String> zfs_properties = new Map<String, String>();
  String disk_dir = "";
  bool disk_rw = false;
  String description = "";
  ZfsStats stats = new ZfsStats();

  Disk();

  Disk.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
    disk_type = j['disk_type'] ?? "";
    name = j['name'] ?? "";
    j['zfs_properties']?.forEach((key, val) => zfs_properties[key] = val); // NOTE: Newly changed for Dart 2.0
    disk_dir = j['disk_dir'] ?? "";
    disk_rw = j['disk_rw'] ?? false;
    description = j['description'] ?? "";
    stats = new ZfsStats.fromJson(j['stats']) ?? new ZfsStats();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_id': disk_id ?? "",
      'disk_type': disk_type ?? "",
      'name': name ?? "",
      'zfs_properties': zfs_properties ?? new Map<String, String>(),
      'disk_dir': disk_dir ?? "",
      'disk_rw': disk_rw ?? false,
      'description': description ?? "",
      'stats': stats ?? new ZfsStats(),
    };
  }
}

