// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ScmSettingsLogic extends ScmSettingsCore {
  // Add custom logic here
}
*/

class ScmSettingsCore extends Object with Id {
  String provider = "";
  String type = "";
  String subtype = "";
  String repo_owner = "";
  String repo_name = "";
  String repo_ref = "";
}

class ScmSettings extends ScmSettingsLogic {
  String provider = "";
  String type = "";
  String subtype = "";
  String repo_owner = "";
  String repo_name = "";
  String repo_ref = "";

  ScmSettings();

  ScmSettings.fromJson(Map j) {
    provider = j['provider'] ?? "";
    type = j['type'] ?? "";
    subtype = j['subtype'] ?? "";
    repo_owner = j['repo_owner'] ?? "";
    repo_name = j['repo_name'] ?? "";
    repo_ref = j['repo_ref'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'type': type ?? "",
      'subtype': subtype ?? "",
      'repo_owner': repo_owner ?? "",
      'repo_name': repo_name ?? "",
      'repo_ref': repo_ref ?? "",
    };
  }
}

