// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class CertLogic extends CertCore {
  // Add custom logic here
}
*/

class CertCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String cert_id = "";
  String name = "";
  String description = "";
  String certificate = "";
  String common_name = "";
  List<String> san_dns_names = new List<String>();
  DateTime expires = new DateTime(0).toUtc();
  String issuer = "";
}

class Cert extends CertLogic {
  String org_id = "";
  String pool_id = "";
  String cert_id = "";
  String name = "";
  String description = "";
  String certificate = "";
  String common_name = "";
  List<String> san_dns_names = new List<String>();
  DateTime expires = new DateTime(0).toUtc();
  String issuer = "";

  Cert();

  Cert.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    cert_id = j['cert_id'] ?? "";
    name = j['name'] ?? "";
    description = j['description'] ?? "";
    certificate = j['certificate'] ?? "";
    common_name = j['common_name'] ?? "";
    j['san_dns_names']?.forEach((val) => san_dns_names.add(val)); // NOTE: Newly changed for Dart 2.0
    expires = (j['expires'] != null) ? DateTime.parse(j['expires']).toUtc() : new DateTime(0).toUtc();
    issuer = j['issuer'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'cert_id': cert_id ?? "",
      'name': name ?? "",
      'description': description ?? "",
      'certificate': certificate ?? "",
      'common_name': common_name ?? "",
      'san_dns_names': san_dns_names ?? new List<String>(),
      'expires': (expires != null) ? expires.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'issuer': issuer ?? "",
    };
  }
}

