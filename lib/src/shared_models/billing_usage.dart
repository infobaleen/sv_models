// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class BillingUsageLogic extends BillingUsageCore {
  // Add custom logic here
}
*/

class BillingUsageCore extends Object with Id {
  String resource_type = "";
  String resource_subtype = "";
  String resource_category = "";
  int price_usd_bp = 0;
  int billing_period = 0;
  int billing_units = 0;
  int amount_in_usd_bp = 0;
  String unit = "";
  int size = 0;
  int quantity = 0;
  DateTime start = new DateTime(0).toUtc();
  DateTime stop = new DateTime(0).toUtc();
  String org_id = "";
  String pool_id = "";
  String schedule_type = "";
  String schedule_id = "";
  String app_id = "";
  String env_id = "";
  String service_id = "";
  String disk_id = "";
  String support_id = "";
  String org_name = "";
  String pool_name = "";
  String app_name = "";
  String env_name = "";
  String disk_name = "";
  String service_name = "";
}

class BillingUsage extends BillingUsageLogic {
  String resource_type = "";
  String resource_subtype = "";
  String resource_category = "";
  int price_usd_bp = 0;
  int billing_period = 0;
  int billing_units = 0;
  int amount_in_usd_bp = 0;
  String unit = "";
  int size = 0;
  int quantity = 0;
  DateTime start = new DateTime(0).toUtc();
  DateTime stop = new DateTime(0).toUtc();
  String org_id = "";
  String pool_id = "";
  String schedule_type = "";
  String schedule_id = "";
  String app_id = "";
  String env_id = "";
  String service_id = "";
  String disk_id = "";
  String support_id = "";
  String org_name = "";
  String pool_name = "";
  String app_name = "";
  String env_name = "";
  String disk_name = "";
  String service_name = "";

  BillingUsage();

  BillingUsage.fromJson(Map j) {
    resource_type = j['resource_type'] ?? "";
    resource_subtype = j['resource_subtype'] ?? "";
    resource_category = j['resource_category'] ?? "";
    price_usd_bp = j['price_usd_bp'] ?? 0;
    billing_period = j['billing_period'] ?? 0;
    billing_units = j['billing_units'] ?? 0;
    amount_in_usd_bp = j['amount_in_usd_bp'] ?? 0;
    unit = j['unit'] ?? "";
    size = j['size'] ?? 0;
    quantity = j['quantity'] ?? 0;
    start = (j['start'] != null) ? DateTime.parse(j['start']).toUtc() : new DateTime(0).toUtc();
    stop = (j['stop'] != null) ? DateTime.parse(j['stop']).toUtc() : new DateTime(0).toUtc();
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    schedule_type = j['schedule_type'] ?? "";
    schedule_id = j['schedule_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    service_id = j['service_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
    support_id = j['support_id'] ?? "";
    org_name = j['org_name'] ?? "";
    pool_name = j['pool_name'] ?? "";
    app_name = j['app_name'] ?? "";
    env_name = j['env_name'] ?? "";
    disk_name = j['disk_name'] ?? "";
    service_name = j['service_name'] ?? "";
  }

  Map toJson() {
    return {
      'resource_type': resource_type ?? "",
      'resource_subtype': resource_subtype ?? "",
      'resource_category': resource_category ?? "",
      'price_usd_bp': price_usd_bp ?? 0,
      'billing_period': billing_period ?? 0,
      'billing_units': billing_units ?? 0,
      'amount_in_usd_bp': amount_in_usd_bp ?? 0,
      'unit': unit ?? "",
      'size': size ?? 0,
      'quantity': quantity ?? 0,
      'start': (start != null) ? start.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'stop': (stop != null) ? stop.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'schedule_type': schedule_type ?? "",
      'schedule_id': schedule_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'service_id': service_id ?? "",
      'disk_id': disk_id ?? "",
      'support_id': support_id ?? "",
      'org_name': org_name ?? "",
      'pool_name': pool_name ?? "",
      'app_name': app_name ?? "",
      'env_name': env_name ?? "",
      'disk_name': disk_name ?? "",
      'service_name': service_name ?? "",
    };
  }
}

