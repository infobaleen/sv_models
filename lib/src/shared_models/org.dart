// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OrgLogic extends OrgCore {
  // Add custom logic here
}
*/

class OrgCore extends Object with Id {
  String org_id = "";
  String account_id = "";
  String name = "";
  String country = "";
  String url = "";
  List<Credit> credits = new List<Credit>();
  List<Discount> discounts = new List<Discount>();
  bool trial_enabled = false;
  DateTime trial_end = new DateTime(0).toUtc();
  int trial_upgrade = 0;
  bool credits_enabled = false;
  bool billing_enabled = false;
  String billing_email = "";
  int support_plan = 0;
  String support_id = "";
  bool tax_exempt = false;
  bool valid_vat_id = false;
  String vat_id = "";
  num vat_rate = 0;
  Map<String, String> pool_types = new Map<String, String>();
  List<OrgMember> members = new List<OrgMember>();
  Map<String, Map<String, bool>> system_action_roles = new Map<String, Map<String, bool>>();
  Map<String, Map<String, bool>> custom_action_roles = new Map<String, Map<String, bool>>();
  Map<String, List<ResourceIds>> system_access_roles = new Map<String, List<ResourceIds>>();
  Map<String, List<ResourceIds>> custom_access_roles = new Map<String, List<ResourceIds>>();
  Map<String, OrgRole> roles = new Map<String, OrgRole>();
  OrgLimits system_org_limits = new OrgLimits();
  OrgLimits custom_org_limits = new OrgLimits();
  String description = "";
}

class Org extends OrgLogic {
  String org_id = "";
  String account_id = "";
  String name = "";
  String country = "";
  String url = "";
  List<Credit> credits = new List<Credit>();
  List<Discount> discounts = new List<Discount>();
  bool trial_enabled = false;
  DateTime trial_end = new DateTime(0).toUtc();
  int trial_upgrade = 0;
  bool credits_enabled = false;
  bool billing_enabled = false;
  String billing_email = "";
  int support_plan = 0;
  String support_id = "";
  bool tax_exempt = false;
  bool valid_vat_id = false;
  String vat_id = "";
  num vat_rate = 0;
  Map<String, String> pool_types = new Map<String, String>();
  List<OrgMember> members = new List<OrgMember>();
  Map<String, Map<String, bool>> system_action_roles = new Map<String, Map<String, bool>>();
  Map<String, Map<String, bool>> custom_action_roles = new Map<String, Map<String, bool>>();
  Map<String, List<ResourceIds>> system_access_roles = new Map<String, List<ResourceIds>>();
  Map<String, List<ResourceIds>> custom_access_roles = new Map<String, List<ResourceIds>>();
  Map<String, OrgRole> roles = new Map<String, OrgRole>();
  OrgLimits system_org_limits = new OrgLimits();
  OrgLimits custom_org_limits = new OrgLimits();
  String description = "";

  Org();

  Org.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    account_id = j['account_id'] ?? "";
    name = j['name'] ?? "";
    country = j['country'] ?? "";
    url = j['url'] ?? "";
    j['credits']?.map((item) => new Credit.fromJson(item))?.forEach(credits.add);
    j['discounts']?.map((item) => new Discount.fromJson(item))?.forEach(discounts.add);
    trial_enabled = j['trial_enabled'] ?? false;
    trial_end = (j['trial_end'] != null) ? DateTime.parse(j['trial_end']).toUtc() : new DateTime(0).toUtc();
    trial_upgrade = j['trial_upgrade'] ?? 0;
    credits_enabled = j['credits_enabled'] ?? false;
    billing_enabled = j['billing_enabled'] ?? false;
    billing_email = j['billing_email'] ?? "";
    support_plan = j['support_plan'] ?? 0;
    support_id = j['support_id'] ?? "";
    tax_exempt = j['tax_exempt'] ?? false;
    valid_vat_id = j['valid_vat_id'] ?? false;
    vat_id = j['vat_id'] ?? "";
    vat_rate = j['vat_rate'] ?? 0;
    j['pool_types']?.forEach((key, val) => pool_types[key] = val); // NOTE: Newly changed for Dart 2.0
    j['members']?.map((item) => new OrgMember.fromJson(item))?.forEach(members.add);
    j['system_action_roles']?.forEach((key, val) => system_action_roles[key] = Map<String, bool>.from(val)); // NOTE: Changed for Dart 2.0
    j['custom_action_roles']?.forEach((key, val) => custom_action_roles[key] = Map<String, bool>.from(val)); // NOTE: Changed for Dart 2.0
    for(var key in j['system_access_roles']?.keys ?? new Map<String, List<ResourceIds>>().keys) {
      system_access_roles[key] = j['system_access_roles'][key]?.map<ResourceIds>((item) => new ResourceIds.fromJson(item))?.toList() ?? new List<ResourceIds>();
    }
    for(var key in j['custom_access_roles']?.keys ?? new Map<String, List<ResourceIds>>().keys) {
      custom_access_roles[key] = j['custom_access_roles'][key]?.map<ResourceIds>((item) => new ResourceIds.fromJson(item))?.toList() ?? new List<ResourceIds>();
    }
    j['roles']?.forEach((key, val) => roles[key] = new OrgRole.fromJson(val)); // NOTE: Changed for Dart 2.0
    system_org_limits = new OrgLimits.fromJson(j['system_org_limits']) ?? new OrgLimits();
    custom_org_limits = new OrgLimits.fromJson(j['custom_org_limits']) ?? new OrgLimits();
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'account_id': account_id ?? "",
      'name': name ?? "",
      'country': country ?? "",
      'url': url ?? "",
      'credits': credits?.map((item) => item.toJson())?.toList() ?? new List<Credit>(),
      'discounts': discounts?.map((item) => item.toJson())?.toList() ?? new List<Discount>(),
      'trial_enabled': trial_enabled ?? false,
      'trial_end': (trial_end != null) ? trial_end.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'trial_upgrade': trial_upgrade ?? 0,
      'credits_enabled': credits_enabled ?? false,
      'billing_enabled': billing_enabled ?? false,
      'billing_email': billing_email ?? "",
      'support_plan': support_plan ?? 0,
      'support_id': support_id ?? "",
      'tax_exempt': tax_exempt ?? false,
      'valid_vat_id': valid_vat_id ?? false,
      'vat_id': vat_id ?? "",
      'vat_rate': vat_rate ?? 0,
      'pool_types': pool_types ?? new Map<String, String>(),
      'members': members?.map((item) => item.toJson())?.toList() ?? new List<OrgMember>(),
      'system_action_roles': system_action_roles ?? new Map<String, Map<String, bool>>(),
      'custom_action_roles': custom_action_roles ?? new Map<String, Map<String, bool>>(),
      'system_access_roles': system_access_roles ?? new Map<String, List<ResourceIds>>(),
      'custom_access_roles': custom_access_roles ?? new Map<String, List<ResourceIds>>(),
      'roles': roles ?? new Map<String, OrgRole>(),
      'system_org_limits': system_org_limits ?? new OrgLimits(),
      'custom_org_limits': custom_org_limits ?? new OrgLimits(),
      'description': description ?? "",
    };
  }
}

