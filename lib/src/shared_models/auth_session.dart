// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class AuthSessionLogic extends AuthSessionCore {
  // Add custom logic here
}
*/

class AuthSessionCore extends Object with Id {
  String session_id = "";
  String api_client = "";
  String client_ip = "";
  String user_agent = "";
  Duration ttl = new Duration();
  DateTime issued = new DateTime(0).toUtc();
  DateTime renewed = new DateTime(0).toUtc();
  DateTime expires = new DateTime(0).toUtc();
}

class AuthSession extends AuthSessionLogic {
  String session_id = "";
  String api_client = "";
  String client_ip = "";
  String user_agent = "";
  Duration ttl = new Duration();
  DateTime issued = new DateTime(0).toUtc();
  DateTime renewed = new DateTime(0).toUtc();
  DateTime expires = new DateTime(0).toUtc();

  AuthSession();

  AuthSession.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
    api_client = j['api_client'] ?? "";
    client_ip = j['client_ip'] ?? "";
    user_agent = j['user_agent'] ?? "";
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
    issued = (j['issued'] != null) ? DateTime.parse(j['issued']).toUtc() : new DateTime(0).toUtc();
    renewed = (j['renewed'] != null) ? DateTime.parse(j['renewed']).toUtc() : new DateTime(0).toUtc();
    expires = (j['expires'] != null) ? DateTime.parse(j['expires']).toUtc() : new DateTime(0).toUtc();
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
      'api_client': api_client ?? "",
      'client_ip': client_ip ?? "",
      'user_agent': user_agent ?? "",
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
      'issued': (issued != null) ? issued.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'renewed': (renewed != null) ? renewed.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'expires': (expires != null) ? expires.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
    };
  }
}

