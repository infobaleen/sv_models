// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class AccountLogic extends AccountCore {
  // Add custom logic here
}
*/

class AccountCore extends Object with Id {
  String account_id = "";
  String first_name = "";
  String last_name = "";
  String email = "";
  String phone = "";
  String phone_country = "";
  String phone_country_code = "";
  String company = "";
  String url = "";
  bool news_letter = false;
  int max_orgs = 0;
  List<ScmAccount> scm_accounts = new List<ScmAccount>();
}

class Account extends AccountLogic {
  String account_id = "";
  String first_name = "";
  String last_name = "";
  String email = "";
  String phone = "";
  String phone_country = "";
  String phone_country_code = "";
  String company = "";
  String url = "";
  bool news_letter = false;
  int max_orgs = 0;
  List<ScmAccount> scm_accounts = new List<ScmAccount>();

  Account();

  Account.fromJson(Map j) {
    account_id = j['account_id'] ?? "";
    first_name = j['first_name'] ?? "";
    last_name = j['last_name'] ?? "";
    email = j['email'] ?? "";
    phone = j['phone'] ?? "";
    phone_country = j['phone_country'] ?? "";
    phone_country_code = j['phone_country_code'] ?? "";
    company = j['company'] ?? "";
    url = j['url'] ?? "";
    news_letter = j['news_letter'] ?? false;
    max_orgs = j['max_orgs'] ?? 0;
    j['scm_accounts']?.map((item) => new ScmAccount.fromJson(item))?.forEach(scm_accounts.add);
  }

  Map toJson() {
    return {
      'account_id': account_id ?? "",
      'first_name': first_name ?? "",
      'last_name': last_name ?? "",
      'email': email ?? "",
      'phone': phone ?? "",
      'phone_country': phone_country ?? "",
      'phone_country_code': phone_country_code ?? "",
      'company': company ?? "",
      'url': url ?? "",
      'news_letter': news_letter ?? false,
      'max_orgs': max_orgs ?? 0,
      'scm_accounts': scm_accounts?.map((item) => item.toJson())?.toList() ?? new List<ScmAccount>(),
    };
  }
}

