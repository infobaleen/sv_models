// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OrgRoleLogic extends OrgRoleCore {
  // Add custom logic here
}
*/

class OrgRoleCore extends Object with Id {
  String name = "";
  bool system_role = false;
  List<String> actions = new List<String>();
  List<String> scopes = new List<String>();
  String description = "";
}

class OrgRole extends OrgRoleLogic {
  String name = "";
  bool system_role = false;
  List<String> actions = new List<String>();
  List<String> scopes = new List<String>();
  String description = "";

  OrgRole();

  OrgRole.fromJson(Map j) {
    name = j['name'] ?? "";
    system_role = j['system_role'] ?? false;
    j['actions']?.forEach((val) => actions.add(val)); // NOTE: Newly changed for Dart 2.0
    j['scopes']?.forEach((val) => scopes.add(val)); // NOTE: Newly changed for Dart 2.0
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'name': name ?? "",
      'system_role': system_role ?? false,
      'actions': actions ?? new List<String>(),
      'scopes': scopes ?? new List<String>(),
      'description': description ?? "",
    };
  }
}

