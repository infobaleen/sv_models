// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ServiceLogic extends ServiceCore {
  // Add custom logic here
}
*/

class ServiceCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String name = "";
  bool enabled = false;
  List<String> disk_ids = new List<String>();
  String description = "";
  MateSettings service_mates = new MateSettings();
}

class Service extends ServiceLogic {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String name = "";
  bool enabled = false;
  List<String> disk_ids = new List<String>();
  String description = "";
  MateSettings service_mates = new MateSettings();

  Service();

  Service.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    name = j['name'] ?? "";
    enabled = j['enabled'] ?? false;
    j['disk_ids']?.forEach((val) => disk_ids.add(val)); // NOTE: Newly changed for Dart 2.0
    description = j['description'] ?? "";
    service_mates = new MateSettings.fromJson(j['service_mates']) ?? new MateSettings();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'name': name ?? "",
      'enabled': enabled ?? false,
      'disk_ids': disk_ids ?? new List<String>(),
      'description': description ?? "",
      'service_mates': service_mates ?? new MateSettings(),
    };
  }
}

