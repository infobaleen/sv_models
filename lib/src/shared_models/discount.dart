// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class DiscountLogic extends DiscountCore {
  // Add custom logic here
}
*/

class DiscountCore extends Object with Id {
  String discount_id = "";
  String resource_type = "";
  DateTime from = new DateTime(0).toUtc();
  DateTime to = new DateTime(0).toUtc();
  bool active = false;
  num percentage = 0;
}

class Discount extends DiscountLogic {
  String discount_id = "";
  String resource_type = "";
  DateTime from = new DateTime(0).toUtc();
  DateTime to = new DateTime(0).toUtc();
  bool active = false;
  num percentage = 0;

  Discount();

  Discount.fromJson(Map j) {
    discount_id = j['discount_id'] ?? "";
    resource_type = j['resource_type'] ?? "";
    from = (j['from'] != null) ? DateTime.parse(j['from']).toUtc() : new DateTime(0).toUtc();
    to = (j['to'] != null) ? DateTime.parse(j['to']).toUtc() : new DateTime(0).toUtc();
    active = j['active'] ?? false;
    percentage = j['percentage'] ?? 0;
  }

  Map toJson() {
    return {
      'discount_id': discount_id ?? "",
      'resource_type': resource_type ?? "",
      'from': (from != null) ? from.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'to': (to != null) ? to.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'active': active ?? false,
      'percentage': percentage ?? 0,
    };
  }
}

