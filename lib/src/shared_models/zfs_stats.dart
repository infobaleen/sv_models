// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ZfsStatsLogic extends ZfsStatsCore {
  // Add custom logic here
}
*/

class ZfsStatsCore extends Object with Id {
  DateTime updated = new DateTime(0).toUtc();
  Duration ttl = new Duration();
  int used = 0;
  int available = 0;
  String compressratio = "";
  int quota = 0;
}

class ZfsStats extends ZfsStatsLogic {
  DateTime updated = new DateTime(0).toUtc();
  Duration ttl = new Duration();
  int used = 0;
  int available = 0;
  String compressratio = "";
  int quota = 0;

  ZfsStats();

  ZfsStats.fromJson(Map j) {
    updated = (j['updated'] != null) ? DateTime.parse(j['updated']).toUtc() : new DateTime(0).toUtc();
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
    used = j['used'] ?? 0;
    available = j['available'] ?? 0;
    compressratio = j['compressratio'] ?? "";
    quota = j['quota'] ?? 0;
  }

  Map toJson() {
    return {
      'updated': (updated != null) ? updated.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
      'used': used ?? 0,
      'available': available ?? 0,
      'compressratio': compressratio ?? "",
      'quota': quota ?? 0,
    };
  }
}

