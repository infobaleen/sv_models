// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class AppCheckLogic extends AppCheckCore {
  // Add custom logic here
}
*/

class AppCheckCore extends Object with Id {
  String path = "";
  int timeout_sec = 0;
  int check_interval_sec = 0;
  int failure_threshold = 0;
  int success_threshold = 0;
  int initial_delay_sec = 0;
}

class AppCheck extends AppCheckLogic {
  String path = "";
  int timeout_sec = 0;
  int check_interval_sec = 0;
  int failure_threshold = 0;
  int success_threshold = 0;
  int initial_delay_sec = 0;

  AppCheck();

  AppCheck.fromJson(Map j) {
    path = j['path'] ?? "";
    timeout_sec = j['timeout_sec'] ?? 0;
    check_interval_sec = j['check_interval_sec'] ?? 0;
    failure_threshold = j['failure_threshold'] ?? 0;
    success_threshold = j['success_threshold'] ?? 0;
    initial_delay_sec = j['initial_delay_sec'] ?? 0;
  }

  Map toJson() {
    return {
      'path': path ?? "",
      'timeout_sec': timeout_sec ?? 0,
      'check_interval_sec': check_interval_sec ?? 0,
      'failure_threshold': failure_threshold ?? 0,
      'success_threshold': success_threshold ?? 0,
      'initial_delay_sec': initial_delay_sec ?? 0,
    };
  }
}

