// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class NewEnvLogic extends NewEnvCore {
  // Add custom logic here
}
*/

class NewEnvCore extends Object with Id {
  String name = "";
  ScmSettings scm_settings = new ScmSettings();
  int su = 0;
  int count = 0;
  String source_root = "";
  String domain_prefix = "";
  String path_prefix = "";
}

class NewEnv extends NewEnvLogic {
  String name = "";
  ScmSettings scm_settings = new ScmSettings();
  int su = 0;
  int count = 0;
  String source_root = "";
  String domain_prefix = "";
  String path_prefix = "";

  NewEnv();

  NewEnv.fromJson(Map j) {
    name = j['name'] ?? "";
    scm_settings = new ScmSettings.fromJson(j['scm_settings']) ?? new ScmSettings();
    su = j['su'] ?? 0;
    count = j['count'] ?? 0;
    source_root = j['source_root'] ?? "";
    domain_prefix = j['domain_prefix'] ?? "";
    path_prefix = j['path_prefix'] ?? "";
  }

  Map toJson() {
    return {
      'name': name ?? "",
      'scm_settings': scm_settings ?? new ScmSettings(),
      'su': su ?? 0,
      'count': count ?? 0,
      'source_root': source_root ?? "",
      'domain_prefix': domain_prefix ?? "",
      'path_prefix': path_prefix ?? "",
    };
  }
}

