// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class MateAddrLogic extends MateAddrCore {
  // Add custom logic here
}
*/

class MateAddrCore extends Object with Id {
  String hostname = "";
  String mate_ip = "";
  String mate_cidr = "";
}

class MateAddr extends MateAddrLogic {
  String hostname = "";
  String mate_ip = "";
  String mate_cidr = "";

  MateAddr();

  MateAddr.fromJson(Map j) {
    hostname = j['hostname'] ?? "";
    mate_ip = j['mate_ip'] ?? "";
    mate_cidr = j['mate_cidr'] ?? "";
  }

  Map toJson() {
    return {
      'hostname': hostname ?? "",
      'mate_ip': mate_ip ?? "",
      'mate_cidr': mate_cidr ?? "",
    };
  }
}

