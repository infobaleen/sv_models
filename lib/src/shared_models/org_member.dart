// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class OrgMemberLogic extends OrgMemberCore {
  // Add custom logic here
}
*/

class OrgMemberCore extends Object with Id {
  String first_name = "";
  String last_name = "";
  String member_id = "";
  bool service_account = false;
  bool active = false;
  bool suspended = false;
  bool ttl_enabled = false;
  Duration ttl = new Duration();
  DateTime ttl_expiry_date = new DateTime(0).toUtc();
  List<String> action_roles = new List<String>();
  List<String> access_roles = new List<String>();
  List<String> roles = new List<String>();
}

class OrgMember extends OrgMemberLogic {
  String first_name = "";
  String last_name = "";
  String member_id = "";
  bool service_account = false;
  bool active = false;
  bool suspended = false;
  bool ttl_enabled = false;
  Duration ttl = new Duration();
  DateTime ttl_expiry_date = new DateTime(0).toUtc();
  List<String> action_roles = new List<String>();
  List<String> access_roles = new List<String>();
  List<String> roles = new List<String>();

  OrgMember();

  OrgMember.fromJson(Map j) {
    first_name = j['first_name'] ?? "";
    last_name = j['last_name'] ?? "";
    member_id = j['member_id'] ?? "";
    service_account = j['service_account'] ?? false;
    active = j['active'] ?? false;
    suspended = j['suspended'] ?? false;
    ttl_enabled = j['ttl_enabled'] ?? false;
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
    ttl_expiry_date = (j['ttl_expiry_date'] != null) ? DateTime.parse(j['ttl_expiry_date']).toUtc() : new DateTime(0).toUtc();
    j['action_roles']?.forEach((val) => action_roles.add(val)); // NOTE: Newly changed for Dart 2.0
    j['access_roles']?.forEach((val) => access_roles.add(val)); // NOTE: Newly changed for Dart 2.0
    j['roles']?.forEach((val) => roles.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'first_name': first_name ?? "",
      'last_name': last_name ?? "",
      'member_id': member_id ?? "",
      'service_account': service_account ?? false,
      'active': active ?? false,
      'suspended': suspended ?? false,
      'ttl_enabled': ttl_enabled ?? false,
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
      'ttl_expiry_date': (ttl_expiry_date != null) ? ttl_expiry_date.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'action_roles': action_roles ?? new List<String>(),
      'access_roles': access_roles ?? new List<String>(),
      'roles': roles ?? new List<String>(),
    };
  }
}

