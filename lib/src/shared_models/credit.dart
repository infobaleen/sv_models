// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class CreditLogic extends CreditCore {
  // Add custom logic here
}
*/

class CreditCore extends Object with Id {
  String credit_id = "";
  String resource_type = "";
  DateTime from = new DateTime(0).toUtc();
  DateTime to = new DateTime(0).toUtc();
  bool active = false;
  int balance_in_usd_bp = 0;
}

class Credit extends CreditLogic {
  String credit_id = "";
  String resource_type = "";
  DateTime from = new DateTime(0).toUtc();
  DateTime to = new DateTime(0).toUtc();
  bool active = false;
  int balance_in_usd_bp = 0;

  Credit();

  Credit.fromJson(Map j) {
    credit_id = j['credit_id'] ?? "";
    resource_type = j['resource_type'] ?? "";
    from = (j['from'] != null) ? DateTime.parse(j['from']).toUtc() : new DateTime(0).toUtc();
    to = (j['to'] != null) ? DateTime.parse(j['to']).toUtc() : new DateTime(0).toUtc();
    active = j['active'] ?? false;
    balance_in_usd_bp = j['balance_in_usd_bp'] ?? 0;
  }

  Map toJson() {
    return {
      'credit_id': credit_id ?? "",
      'resource_type': resource_type ?? "",
      'from': (from != null) ? from.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'to': (to != null) ? to.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'active': active ?? false,
      'balance_in_usd_bp': balance_in_usd_bp ?? 0,
    };
  }
}

