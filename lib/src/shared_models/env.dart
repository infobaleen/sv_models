// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class EnvLogic extends EnvCore {
  // Add custom logic here
}
*/

class EnvCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String name = "";
  String runtime = "";
  bool enabled = false;
  bool primary = false;
  int deploy = 0;
  int promote = 0;
  DateTime deployed = new DateTime(0).toUtc();
  bool ttl_enabled = false;
  Duration ttl = new Duration();
  DateTime ttl_shutdown = new DateTime(0).toUtc();
  ScmSettings scm_settings = new ScmSettings();
  String source_root = "";
  String domain_prefix = "";
  String path_prefix = "";
  bool static_enabled = false;
  StaticSettings static_settings = new StaticSettings();
  Map<String, String> env_vars = new Map<String, String>();
  List<String> disk_ids = new List<String>();
  bool stage_deploy = false;
  bool stage_build = false;
  bool stage_mate_sync = false;
  bool stage_reload = false;
  String description = "";
  String sourcevoid_checksum = "";
  String sourcevoid_yaml = "";
  MateSettings build_mates = new MateSettings();
  MateSettings start_mates = new MateSettings();
}

class Env extends EnvLogic {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String name = "";
  String runtime = "";
  bool enabled = false;
  bool primary = false;
  int deploy = 0;
  int promote = 0;
  DateTime deployed = new DateTime(0).toUtc();
  bool ttl_enabled = false;
  Duration ttl = new Duration();
  DateTime ttl_shutdown = new DateTime(0).toUtc();
  ScmSettings scm_settings = new ScmSettings();
  String source_root = "";
  String domain_prefix = "";
  String path_prefix = "";
  bool static_enabled = false;
  StaticSettings static_settings = new StaticSettings();
  Map<String, String> env_vars = new Map<String, String>();
  List<String> disk_ids = new List<String>();
  bool stage_deploy = false;
  bool stage_build = false;
  bool stage_mate_sync = false;
  bool stage_reload = false;
  String description = "";
  String sourcevoid_checksum = "";
  String sourcevoid_yaml = "";
  MateSettings build_mates = new MateSettings();
  MateSettings start_mates = new MateSettings();

  Env();

  Env.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    name = j['name'] ?? "";
    runtime = j['runtime'] ?? "";
    enabled = j['enabled'] ?? false;
    primary = j['primary'] ?? false;
    deploy = j['deploy'] ?? 0;
    promote = j['promote'] ?? 0;
    deployed = (j['deployed'] != null) ? DateTime.parse(j['deployed']).toUtc() : new DateTime(0).toUtc();
    ttl_enabled = j['ttl_enabled'] ?? false;
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
    ttl_shutdown = (j['ttl_shutdown'] != null) ? DateTime.parse(j['ttl_shutdown']).toUtc() : new DateTime(0).toUtc();
    scm_settings = new ScmSettings.fromJson(j['scm_settings']) ?? new ScmSettings();
    source_root = j['source_root'] ?? "";
    domain_prefix = j['domain_prefix'] ?? "";
    path_prefix = j['path_prefix'] ?? "";
    static_enabled = j['static_enabled'] ?? false;
    static_settings = new StaticSettings.fromJson(j['static_settings']) ?? new StaticSettings();
    j['env_vars']?.forEach((key, val) => env_vars[key] = val); // NOTE: Newly changed for Dart 2.0
    j['disk_ids']?.forEach((val) => disk_ids.add(val)); // NOTE: Newly changed for Dart 2.0
    stage_deploy = j['stage_deploy'] ?? false;
    stage_build = j['stage_build'] ?? false;
    stage_mate_sync = j['stage_mate_sync'] ?? false;
    stage_reload = j['stage_reload'] ?? false;
    description = j['description'] ?? "";
    sourcevoid_checksum = j['sourcevoid_checksum'] ?? "";
    sourcevoid_yaml = j['sourcevoid_yaml'] ?? "";
    build_mates = new MateSettings.fromJson(j['build_mates']) ?? new MateSettings();
    start_mates = new MateSettings.fromJson(j['start_mates']) ?? new MateSettings();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'name': name ?? "",
      'runtime': runtime ?? "",
      'enabled': enabled ?? false,
      'primary': primary ?? false,
      'deploy': deploy ?? 0,
      'promote': promote ?? 0,
      'deployed': (deployed != null) ? deployed.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'ttl_enabled': ttl_enabled ?? false,
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
      'ttl_shutdown': (ttl_shutdown != null) ? ttl_shutdown.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'scm_settings': scm_settings ?? new ScmSettings(),
      'source_root': source_root ?? "",
      'domain_prefix': domain_prefix ?? "",
      'path_prefix': path_prefix ?? "",
      'static_enabled': static_enabled ?? false,
      'static_settings': static_settings ?? new StaticSettings(),
      'env_vars': env_vars ?? new Map<String, String>(),
      'disk_ids': disk_ids ?? new List<String>(),
      'stage_deploy': stage_deploy ?? false,
      'stage_build': stage_build ?? false,
      'stage_mate_sync': stage_mate_sync ?? false,
      'stage_reload': stage_reload ?? false,
      'description': description ?? "",
      'sourcevoid_checksum': sourcevoid_checksum ?? "",
      'sourcevoid_yaml': sourcevoid_yaml ?? "",
      'build_mates': build_mates ?? new MateSettings(),
      'start_mates': start_mates ?? new MateSettings(),
    };
  }
}

