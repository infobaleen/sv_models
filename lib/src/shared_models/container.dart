// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ContainerLogic extends ContainerCore {
  // Add custom logic here
}
*/

class ContainerCore extends Object with Id {
  String org_id = "";
  String pool_id = "";
  String schedule_id = "";
  String mate_id = "";
  bool public = false;
  int pool_type = 0;
  String mate_type = "";
  String hostname = "";
  String schedule_type = "";
  bool service_mate = false;
  String pipeline = "";
  int pipeline_index = 0;
  String build_id = "";
  String workdir = "";
  int cpu = 0;
  int mem = 0;
  int memory_swap = 0;
  int memory_swappiness = 0;
  MateAddr mate_addr = new MateAddr();
}

class Container extends ContainerLogic {
  String org_id = "";
  String pool_id = "";
  String schedule_id = "";
  String mate_id = "";
  bool public = false;
  int pool_type = 0;
  String mate_type = "";
  String hostname = "";
  String schedule_type = "";
  bool service_mate = false;
  String pipeline = "";
  int pipeline_index = 0;
  String build_id = "";
  String workdir = "";
  int cpu = 0;
  int mem = 0;
  int memory_swap = 0;
  int memory_swappiness = 0;
  MateAddr mate_addr = new MateAddr();

  Container();

  Container.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    schedule_id = j['schedule_id'] ?? "";
    mate_id = j['mate_id'] ?? "";
    public = j['public'] ?? false;
    pool_type = j['pool_type'] ?? 0;
    mate_type = j['mate_type'] ?? "";
    hostname = j['hostname'] ?? "";
    schedule_type = j['schedule_type'] ?? "";
    service_mate = j['service_mate'] ?? false;
    pipeline = j['pipeline'] ?? "";
    pipeline_index = j['pipeline_index'] ?? 0;
    build_id = j['build_id'] ?? "";
    workdir = j['workdir'] ?? "";
    cpu = j['cpu'] ?? 0;
    mem = j['mem'] ?? 0;
    memory_swap = j['memory_swap'] ?? 0;
    memory_swappiness = j['memory_swappiness'] ?? 0;
    mate_addr = new MateAddr.fromJson(j['mate_addr']) ?? new MateAddr();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'schedule_id': schedule_id ?? "",
      'mate_id': mate_id ?? "",
      'public': public ?? false,
      'pool_type': pool_type ?? 0,
      'mate_type': mate_type ?? "",
      'hostname': hostname ?? "",
      'schedule_type': schedule_type ?? "",
      'service_mate': service_mate ?? false,
      'pipeline': pipeline ?? "",
      'pipeline_index': pipeline_index ?? 0,
      'build_id': build_id ?? "",
      'workdir': workdir ?? "",
      'cpu': cpu ?? 0,
      'mem': mem ?? 0,
      'memory_swap': memory_swap ?? 0,
      'memory_swappiness': memory_swappiness ?? 0,
      'mate_addr': mate_addr ?? new MateAddr(),
    };
  }
}

