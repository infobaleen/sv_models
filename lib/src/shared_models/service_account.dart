// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of shared_models;

/*
// Copy and edit this base template to /lib/src/shared_model_logic

class ServiceAccountLogic extends ServiceAccountCore {
  // Add custom logic here
}
*/

class ServiceAccountCore extends Object with Id {
  String org_id = "";
  String service_account_id = "";
  String name = "";
  String description = "";
  List<AuthSession> sessions = new List<AuthSession>();
}

class ServiceAccount extends ServiceAccountLogic {
  String org_id = "";
  String service_account_id = "";
  String name = "";
  String description = "";
  List<AuthSession> sessions = new List<AuthSession>();

  ServiceAccount();

  ServiceAccount.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    name = j['name'] ?? "";
    description = j['description'] ?? "";
    j['sessions']?.map((item) => new AuthSession.fromJson(item))?.forEach(sessions.add);
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'name': name ?? "",
      'description': description ?? "",
      'sessions': sessions?.map((item) => item.toJson())?.toList() ?? new List<AuthSession>(),
    };
  }
}

