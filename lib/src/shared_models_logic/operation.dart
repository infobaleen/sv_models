part of shared_models_logic;

class OperationLogic extends OperationCore {
  String get service_styled {
    return context.service.replaceAll('Rpc', '');
  }

  String get method_styled {
    return context.method;
  }

  String get start_local {
    return start.toLocal().toString().substring(0, 19);
  }

  String get end_local {
    if(end.isBefore(new DateTime(1970))) {
      return '-';
    } else {
      return end.toLocal().toString().substring(0, 19);
    }
  }

  String get state_styled {
    return '${state.substring(0, 1)}${state.substring(1).toLowerCase()}';
  }

  String get status_phrase_styled {
    return status_phrase;
  }

  String get time {
    if(end.millisecond != 0) {
      var time = end.difference(start);

      if(time.inMilliseconds < 1000) {
        return "${time.inMilliseconds} ms";
      } else {
        return "${time.inSeconds}.${time.inMilliseconds % 1000} s";
      }
    } else {
      return "-";
    }
  }
}

class RpcAuthContext {
  String account_id;
  String session_id;
  String service;
  String method;
  String client_ip;
  String api_client;
  String user_agent;

  RpcAuthContext();

  RpcAuthContext.fromJson(Map json) {
    account_id = json["account_id"];
    session_id = json["session_id"];
    service = json["service"];
    method = json["method"];
    client_ip = json["client_ip"];
    api_client = json["api_client"];
    user_agent = json["user_agent"];
  }

  Map toJson() {
    return {
      "account_id": account_id,
      "session_id": session_id,
      "service": service,
      "method": method,
      "client_ip": client_ip,
      "api_client": api_client,
      "user_agent": user_agent
    };
  }
}

