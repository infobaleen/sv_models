part of shared_models_logic;

class BillingUsageLogic extends BillingUsageCore {
  String get name {
    var name = "";

    if(app_name != null && app_name != "" && app_name != "N/A") {
      name = app_name;
    } else if(disk_name != null && disk_name != "" && disk_name != "N/A") {
      name = disk_name;
    } else if(service_name != null && service_name != "" && service_name != "N/A") {
      name = service_name;
    } else if(resource_type == "support") {
      name = resource_subtype;
    }

    return name;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get charge_in_dollars_fixed_4 {
//    return num.parse((this.amount_in_usd_bp / 10000).toStringAsFixed(4), (_) => 0);
    return num.tryParse((this.amount_in_usd_bp / 10000).toStringAsFixed(4)) ?? 0;
  }

  // OBS: Only valid if 'from' is now and 'to' is from + 1 hour...
  // Hourly charge, divided by 1000 instead of 10000 because the we want the
  // hourly charge, divided by 10000 gives the price per 6 minutes.
  num get hourly_charge {
    if(billing_period == 360) {
      // Container billing period is 360 seconds
      return ((price_usd_bp * size * quantity) / 1000);
    } else if(billing_period == 3600) {
      // Storage billing period is 3600 seconds
      return ((price_usd_bp * size * quantity) / 10000);
    } else {
      throw("Error bad billing_period value");
    }
  }

  // Unit hours, device by 10 for containers since we our internal billing unit is 1/10 of an hour. Our system count
  // "how many 6 minute intervals have this resource been running" (6 min == 1/10 of 1 hour). Therefor to show number
  // of "SU hours" we divide the number by ten to get per hour and not be 1/10 of an hour. For storage that have a
  // billing period of 1 hours we simply return the billing units value.
  num get unit_hours {
    if(billing_period == 360) {
      // Container billing period is 360 seconds (1/10 hour)
      return (this.billing_units / 10);
    } else if(billing_period == 3600) {
      // Storage billing period is 3600 seconds (1 hour)
      return this.billing_units;
    } else {
      throw("Error bad billing_period value");
    }
  }

  // Getter used to render category only if it's build, in current usage we most
  // of the time don't need to show category so we only show it for build.
  String get isBuildCategory {
    if(resource_category == "build") {
      if(quantity > 1) {
        return "(build containers)";
      } else {
        return "(build container)";
      }
    } else {
      return "";
    }
  }

  String get resource_category_capitalized {
    return '${resource_category.substring(0, 1).toUpperCase()}${resource_category.substring(1)}';
  }

  String get diskName {
    var name = "";

    if(resource_subtype == "app") {
      name = "Application disk for app ${this.app_name}";
    } else if(resource_subtype == "mongodb") {
      name = "MongoDB disk";
    } else if(resource_subtype == "mysql") {
      name = "MySQL disk";
    } else if(resource_subtype == "postgresql") {
      name = "PostgreSQL disk";
    } else if(resource_subtype == "data") {
      name = "Data disk ${this.name}";
    }

    return name;
  }
}

