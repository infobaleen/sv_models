part of shared_models_logic;

// NOTE: Descriptions copied from; https://en.wikipedia.org/wiki/UTM_parameters

class UtmLogic extends UtmCore {
  parseUri(Uri uri) {
    source    = uri.queryParameters['utm_source'];
    medium    = uri.queryParameters['utm_medium'];
    campaign  = uri.queryParameters['utm_campaign'];
    term      = uri.queryParameters['utm_term'];
    content   = uri.queryParameters['utm_content'];
  }

  String toString() {
    var query = '';

    query = _addQueryParts(query, 'source', source);
    query = _addQueryParts(query, 'medium', medium);
    query = _addQueryParts(query, 'campaign', campaign);
    query = _addQueryParts(query, 'term', term);
    query = _addQueryParts(query, 'content', content);

    return query;
  }

  String _addQueryParts(String query, String key, String val) {
    if(val != null && val != '') {
      if(query == '') {
        query = '${key}=${val}';
      } else {
        query = '${query}&${key}=${val}';
      }
    }

    return query;
  }
}

