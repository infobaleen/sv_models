part of shared_models_logic;

class MateSettingsLogic extends MateSettingsCore {
  // **************************************
  // Mustache specific help members, should not be included in to/from json

  bool get isSize1Su => (this.su == 1);
  bool get isSize2Su => (this.su == 2);
  bool get isSize3Su => (this.su == 3);
  bool get isSize5Su => (this.su == 5);
  bool get isSize10Su => (this.su == 10);

  bool get isCount1 => (this.count == 1);
  bool get isCount2 => (this.count == 2);
  bool get isCount3 => (this.count == 3);
  bool get isCount4 => (this.count == 4);
  bool get isCount5 => (this.count == 5);
  bool get isCount6 => (this.count == 6);
  bool get isCount7 => (this.count == 7);
  bool get isCount8 => (this.count == 8);
  bool get isCount9 => (this.count == 9);
  bool get isCount10 => (this.count == 10);

  // **************************************

  void validate() {
    // Check container that su and count are int values
    if ((su is int) == false) {
      throw new ModelValidationException("Container su value must be an integer number");
    }
    if ((count is int) == false) {
      throw new ModelValidationException("Container count value must be an integer number");
    }

    // Check su interval
    if (su < 1) {
      throw new ModelValidationException("Container su value must be at least 1");
    }
    if (su > 128) {
      throw new ModelValidationException("Container su value can not be higher than 128");
    }

    // Check count interval
    if (count < 1) {
      throw new ModelValidationException("Container count value must be at least 1");
    }
    if (count > 10) {
      throw new ModelValidationException("Container count value can currently not be higher than 10, if you need more containers please contact support");
    }
  }
}

