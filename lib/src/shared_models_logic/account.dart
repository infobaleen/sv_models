part of shared_models_logic;

class AccountLogic extends AccountCore {
  // NOTE: Using 'github' directly in the mustache templates don't seem to work
  // in Chrome, only in Dartium?!? To workaround this we use the sample below
  // in account_settings.dart:
  //  model['authenticated'] = account.github.authenticated;
  //  model['username'] = account.github.username;

  ScmAccount get github {
    for (var scm_account in scm_accounts) {
      if(scm_account.provider == "github") {
        return scm_account;
      }
    }

    return new ScmAccount();
  }
}

