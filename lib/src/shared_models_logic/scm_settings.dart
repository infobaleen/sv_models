part of shared_models_logic;

class ScmSettingsLogic extends ScmSettingsCore {
  // Helper functions for mustache rendering
  bool isGitDeployment() => (type == "git");
  bool isArchiveDeployment() => (type == "archive");
  bool isGitTagDeployment() => (type == "git" && subtype == "tag");
  bool isGitBranchDeployment() => (type == "git" && subtype == "branch");

  // Regex to check for valid git ref, simplified check but should work 99% of
  // the times for us I think. Two or more of chars [0-9a-zA-Z._-], don't let
  // it end with a dot ".".
  var gitRefRegex = new RegExp(r'^([0-9a-zA-Z._-])+([0-9a-zA-Z_-])+$');

  void validate() {
    if (provider != "github" &&
        provider != "bitbucket" &&
        provider != "sourcevoid" &&
        provider != "gitlab") {
      throw new ModelValidationException("Selected scm provider $provider is not supported");
    }

    if (type != "git" && type != "archive") {
      throw new ModelValidationException("Selected scm type $type is not supported");
    }

    if (type == "git") {
      if (subtype != "branch" && subtype != "tag" && subtype != "commit") {
        throw new ModelValidationException("Selected scm subtype $subtype is not supported with type $type");
      }

      isNonEmptyString(repo_owner, 'Please provide an repository owner');
      isNonEmptyString(repo_name, 'Please provide an repository name');
      isNonEmptyString(repo_ref, 'Please provide an repository ref, if unsure, use "master" to deploy the master branch');

      if(gitRefRegex.hasMatch(repo_ref) == false) {
        throw new ModelValidationException('The git ref value "$repo_ref" is not valid, please try again with a valid ref value');
      }
    }

    if (type == "archive") {
      if (subtype == "zip" || subtype == "tar.xz" || subtype == "tar.gz" || subtype == "tar.bz2" || subtype == "tar") {
        // Do nothing
      } else {
        throw new ModelValidationException("Selected scm subtype \"${subtype}\" is not supported with type $type");
      }
    }
  }
}

