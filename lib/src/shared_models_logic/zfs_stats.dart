part of shared_models_logic;

class ZfsStatsLogic extends ZfsStatsCore {
  // Formatted in KB (KiB)
  int get used_kb => (used / 1024).round();
  int get available_kb => (available / 1024).round();
  int get quota_kb => (quota / 1024).round();

  // Formatted in MB (MiB)
  int get used_mb => (used / 1024 / 1024).round();
  int get available_mb => (available / 1024 / 1024).round();
  int get quota_mb => (quota / 1024 / 1024).round();

  // Formatted in GB (GiB)
  int get used_gb => (used / 1024 / 1024 / 1024).round();
  int get available_gb => (available / 1024 / 1024 / 1024).round();
  int get quota_gb => (quota / 1024 / 1024 / 1024).round();

  // Formatted in GB (GiB)
  String get used_auto => prefixBytes(used);
  String get available_auto => prefixBytes(available);
  String get quota_auto => prefixBytes(quota);

  // Prefix bytes dynamically
  String prefixBytes(num bytes) {
    if(bytes < 1048576) {
      // If less than 1 MB (MiB), return in KB format
      return "${(bytes / 1024).round()} KB";
    } else if(bytes < 1073741824) {
      // If less than 1 GB (GiB), return in MB format
      return "${(bytes / 1024 / 1024).round()} MB";
    } else {
      // If more than one 1 GB, return in GB format
      return "${(bytes / 1024 / 1024 / 1024).round()} GB";
    }
  }
}

