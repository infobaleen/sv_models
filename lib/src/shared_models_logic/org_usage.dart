part of shared_models_logic;

class OrgUsageLogic extends OrgUsageCore {
  // Calculate total hourly container usage charges
  String get total_charge_containers_hourly {
    var total = 0.00;

    container_usage?.forEach((usage) => total += usage.hourly_charge);

    return total.toStringAsFixed(3);
  }

  // NOTE: We return as fixed 4 here on purpose since the lowest hourly charge
  // we have for storage is 0.0007.
  // Calculate total hourly storage usage charges
  String get total_charge_storage_hourly {
    var total = 0.00;

    storage_usage?.forEach((usage) => total += usage.hourly_charge);

    return total.toStringAsFixed(4);
  }

  // Calculate total hourly bandwidth usage charges
  String get total_charge_bandwidth_hourly {
    var total = 0.00;

    bandwidth_usage?.forEach((usage) => total += usage.hourly_charge);

    return total.toStringAsFixed(3);
  }

  // Calculate total hourly support usage charges
  String get total_charge_support_hourly {
    var total = 0.00;

    support_usage?.forEach((usage) => total += usage.hourly_charge);

    return total.toStringAsFixed(3);
  }

  // Calculate total hourly support usage charges
  String get total_charge_all_hourly {
    var total = 0.00;

    container_usage?.forEach((usage) => total += usage.hourly_charge);
    storage_usage?.forEach((usage) => total += usage.hourly_charge);
    bandwidth_usage?.forEach((usage) => total += usage.hourly_charge);
    support_usage?.forEach((usage) => total += usage.hourly_charge);

    return total.toStringAsFixed(4);
  }

  // Calculate total container usage charges
  int get total_charge_containers_in_usd_bp {
    var total = 0;

    container_usage?.forEach((usage) => total += usage.amount_in_usd_bp);

    return total;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get total_charge_containers_in_usd_fixed_2 {
//    return num.parse((this.total_charge_containers_in_usd_bp / 10000).toStringAsFixed(2), (_) => 0);
    return num.tryParse((this.total_charge_containers_in_usd_bp / 10000).toStringAsFixed(2)) ?? 0;
  }

  // Calculate total storage usage charges
  int get total_charge_storage_in_usd_bp {
    var total = 0;

    storage_usage?.forEach((usage) => total += usage.amount_in_usd_bp);

    return total;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get total_charge_storage_in_usd_fixed_2 {
//    return num.parse((this.total_charge_storage_in_usd_bp / 10000).toStringAsFixed(2), (_) => 0);
    return num.tryParse((this.total_charge_storage_in_usd_bp / 10000).toStringAsFixed(2)) ?? 0;
  }

  // Calculate total bandwidth_ usage charges
  int get total_charge_bandwidth_in_usd_bp {
    var total = 0;

    bandwidth_usage?.forEach((usage) => total += usage.amount_in_usd_bp);

    return total;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get total_charge_bandwidth_in_usd_fixed_2 {
//    return num.parse((this.total_charge_bandwidth_in_usd_bp / 10000).toStringAsFixed(2), (_) => 0);
    return num.tryParse((this.total_charge_bandwidth_in_usd_bp / 10000).toStringAsFixed(2)) ?? 0;
  }

  // Calculate total support usage charges
  int get total_charge_support_in_usd_bp {
    var total = 0;

    support_usage?.forEach((usage) => total += usage.amount_in_usd_bp);

    return total;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get total_charge_support_in_usd_fixed_2 {
//    return num.parse((this.total_charge_support_in_usd_bp / 10000).toStringAsFixed(2), (_) => 0);
    return num.tryParse((this.total_charge_support_in_usd_bp / 10000).toStringAsFixed(2)) ?? 0;
  }

  // Calculate total for all usage charges,
  int get total_charge_all_in_usd_bp {
    var total = 0;

    // NOTE: Important that we use 'amount_in_usd_bp' here and not so we don't
    // lose any information by adding up rounded floats...
    container_usage?.forEach((usage) => total += usage.amount_in_usd_bp);
    storage_usage?.forEach((usage) => total += usage.amount_in_usd_bp);
    bandwidth_usage?.forEach((usage) => total += usage.amount_in_usd_bp);
    support_usage?.forEach((usage) => total += usage.amount_in_usd_bp);

    return total;
  }

  // To convert USD bp into dollar we divide by 10,000 (1 USD bp == 1 USD/10000)
  num get total_charge_all_in_usd_fixed_2 {
//    return num.parse((this.total_charge_all_in_usd_bp / 10000).toStringAsFixed(2), (_) => 0);
    return num.tryParse((this.total_charge_all_in_usd_bp / 10000).toStringAsFixed(2)) ?? 0;
  }

  // Convert Dart month integer into string
  static String getMonth(DateTime date) {
    var month = date.toUtc().month;

    if(month == DateTime.january) {
      return "January";
    } else if(month == DateTime.february) {
      return "February";
    } else if(month == DateTime.march) {
      return "March";
    } else if(month == DateTime.april) {
      return "April";
    } else if(month == DateTime.may) {
      return "May";
    } else if(month == DateTime.june) {
      return "June";
    } else if(month == DateTime.july) {
      return "July";
    } else if(month == DateTime.august) {
      return "August";
    } else if(month == DateTime.september) {
      return "September";
    } else if(month == DateTime.october) {
      return "October";
    } else if(month == DateTime.november) {
      return "November";
    } else if(month == DateTime.december) {
      return "December";
    } else {
      // To make analyser quite, should never happen...
      return "December";
    }
  }
}

