part of shared_models_logic;

class CertLogic extends CertCore {
  String get expires_formated {
    if(expires != null) {
      return expires.toString().substring(0, 10);
    } else {
      return '';
    }
  }
}

