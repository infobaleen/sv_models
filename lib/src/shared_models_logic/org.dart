part of shared_models_logic;

class OrgLogic extends OrgCore {
  bool get enabled {
    return (credits_enabled == true || billing_enabled == true);
  }

  int get total_credit_in_usd_bp {
    var total = 0;
    var now = new DateTime.now();
    
    for (var credit in credits) {
      if(credit.active == true && credit.from.isBefore(now) && credit.to.isAfter(now)) {
        total += credit.balance_in_usd_bp;
      }
    }
    
    return total;
  }

  String get support_plan_name {
    if(support_plan == 0) {
      return "Basic Support";
    } else if(support_plan == 1) {
      return "Standard Support";
    } else if(support_plan == 2) {
      return "Premium Support";
    } else if(support_plan == 3) {
      return "Platinum Support";
    } else {
      return "Invalid Support Level, please contact support";
    }
  }

//  List<OptionData> get countryOptions {
//    return generateCountryOptions(country);
//  }

  List<OptionData> get supportOptions {
    return [
      new OptionData("0", "Basic Support", "${support_plan}"),
      new OptionData("1", "Standard Support", "${support_plan}"),
      new OptionData("2", "Premium Support", "${support_plan}"),
      new OptionData("3", "Platinum Support", "${support_plan}"),
    ];
  }

  bool get isVatRequired {
//    return true;
    return VatLookup.isVatCountry("NO_VAT");
  }

  String get vatRatePercentage {
    return '${(vat_rate * 100) - 100}';
  }
}

// Helper class for make mustache rendering easier on "Orgs & Billing" view
class OrgAndUsage {
  Org org;
  OrgUsage hourlyUsage;
  OrgUsage monthlyUsage;
  String creditBalance;

  OrgAndUsage(this.org, this.hourlyUsage, this.monthlyUsage, this.creditBalance);
}
