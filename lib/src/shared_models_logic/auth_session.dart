part of shared_models_logic;

class AuthSessionLogic extends AuthSessionCore {
  // Private variable only used for mustache rendering
  bool isCurrent;

  String get issued_local {
    return issued.toLocal().toString().substring(0, 19);
  }

  String get renewed_local {
    return renewed.toLocal().toString().substring(0, 19);
  }

  String get expires_local {
    return expires.toLocal().toString().substring(0, 19);
  }

  List<String> get user_agent_split {
    var split = user_agent.split(')');

    // Add back the ")", note that the last part should not end with ")"
    for(var i = 0; i < (split.length - 1); i++) {
      split[i] = '${split[i]})';
    }

    return split;
  }
}

