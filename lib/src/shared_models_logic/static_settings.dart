part of shared_models_logic;

class StaticSettingsLogic extends StaticSettingsCore {
}

class Handler extends Object with Id {
  String url = "";
  String match = "";
  List<String> methods = new List<String>();

  // Client handler specific options
  String dir = "";
  String file = "";

  // NOTE: Not implemented yet
  // error handler specific options
  String type = ""; // Note: Implicit for server and client handlers, explicit for error handlers
  String code = "";

  // Internal parameters
  bool invalid;

  Handler();

  Handler.fromJson(Map json) {
    url       = json["url"];
    match     = json["match"];

    methods   = new List<String>();
    if(json["methods"] is List<String>) {
      methods.addAll(json["methods"]);
    }

    dir       = json["dir"];
    file      = json["file"];
    type      = json["type"];
    code      = json["code"];
    invalid   = json["invalid"];
  }

  Map toJson() {
    return {
      "url"     : url,
      "match"   : match,
      "methods" : methods,
      "dir"     : dir,
      "file"    : file,
      "type"    : type,
      "code"    : code,
      "invalid" : invalid
    };
  }
}

