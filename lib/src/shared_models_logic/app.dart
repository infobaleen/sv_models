part of shared_models_logic;

class AppLogic extends AppCore {

  // **************************************
  // Mustache specific help members, should not be included in to/from json

  bool get hasStaticEnvs {
    bool static_enabled;

    for (var env in envs) {
      if(env.static_enabled == true) {
        static_enabled = true;
        break;
      }
    }

    return static_enabled;
  }

  // Helper to itterate over a map in mustache
  List<KeyVal> _env_vars_list;

  List<KeyVal> get env_vars_list {
    // Clear any old values
    if(_env_vars_list == null) {
      _env_vars_list = new List<KeyVal>();
    }

    // Make env_vars is not null
    if(env_vars == null) {
      env_vars = new Map<String, String>();
    }

    // Add values, only do this once we both mustache and setup get the same
    // objects and not copied
    if(_env_vars_list.isEmpty == true) {
      // Add all app environment variables
      for(var key in env_vars.keys) {
        _env_vars_list.add(new KeyVal(key, env_vars[key], false));
      }

      // Add an extra empty item for UI
      _env_vars_list.add(new KeyVal('', '', true));
    }

    return _env_vars_list;
  }

  // **************************************

  /// Checks if a custom domain is used
  bool get customDomainIsUsed {
    if(custom_domain.domain == '' || custom_domain.domain == null) {
      return false;
    } else {
      return true;
    }
  }

  // Get index for the current app environment
  int getAppEnvIndex(String appEnvId) {
    int appEnvIndex;

    for(var i = 0; i < envs.length; i++) {
      if(appEnvId == envs[i].env_id) {
         appEnvIndex = i;
         break;
      }
    }

    return appEnvIndex;
  }

  // Regex to check FQDN
  // Source: http://stackoverflow.com/a/18012067
  var envRegExp = new RegExp(r'^[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]$');

  void validate() {
    var domainPrefixes = new Set();

    // Validate app
    isNonEmptyString(name, 'Please provide an app name');

    // Validate domains
    platform_domain.validate();
    custom_domain.validate();

    // Validate envs
    for (var env in envs) {
      env.validate();

      // Save domain_prefix
      domainPrefixes.add(env.domain_prefix);
    }

    // Make sure there are no duplicate domain_prefixes
    if (envs.length != domainPrefixes.length) {
      throw new ModelValidationException("Environment domain prefixes names must be unique, please make sure not to have duplicated domain names");
    }
  }
}

class KeyVal extends Object with Id {
  String key;
  String val;
  bool edit = false;

  KeyVal(this.key, this.val, this.edit);
}

