part of shared_models_logic;

class EnvLogic extends EnvCore {
  // **************************************
  // Mustache specific help members, should not be included in to/from json

  String get deployment {
    if(scm_settings.isArchiveDeployment() == true) {
      // Special case for old apps (TODO: Remove later when all archive uploads have a repo_ref)
      if(scm_settings.repo_ref != "") {
        return scm_settings.repo_ref;
      } else {
        return 'unnamed archive upload';
      }
    } else if(scm_settings.isGitDeployment() == true) {
      return '${scm_settings.repo_owner}/${scm_settings.repo_name} @ ${scm_settings.repo_ref}';
    } else {
      // We should never come here, added to suppress analyzer warning
      return '';
    }
  }

  String get runtimeCapitalized {
    if(runtime == null || runtime.length < 2) {
      return "[TBD]";
     } else {
      return runtime.substring(0, 1).toUpperCase() + runtime.substring(1);
    }
  }

  String get ttl_left {
    var now = new DateTime.now().toUtc();
    var diff = ttl_shutdown.difference(now);

    if(enabled == false) {
      return 'Off';
    } else if(diff.inMinutes > 60) {
      if(diff.inMinutes % 60 < 10) {
        return '${diff.inHours}:0${diff.inMinutes % 60} h';
      } else {
        return '${diff.inHours}:${diff.inMinutes % 60} h';
      }
    } else if(diff.inMinutes > 0 && diff.inMinutes < 60) {
      return '${diff.inMinutes} m';
    } else if(diff.inSeconds > 0 && diff.inSeconds < 60) {
      return '${diff.inMinutes} s';
    } else {
      return 'Off';
    }
  }

  // Helper to itterate over a map in mustache
  List<KeyVal> _env_vars_list;

  /// List of environment variables.
  List<KeyVal> get env_vars_list {
    // Clear any old values
    if (_env_vars_list == null) {
      _env_vars_list = new List<KeyVal>();
    }

    // Make env_vars is not null
    if (env_vars == null) {
      env_vars = new Map<String, String>();
    }

    // Add values, only do this once we both mustache and setup get the same
    // objects and not copied
    if (_env_vars_list.isEmpty == true) {
      // Add all app environment variables
      for (var key in env_vars.keys) {
        _env_vars_list.add(new KeyVal(key, env_vars[key], false));
      }

      // Add an extra empty item for UI
      _env_vars_list.add(new KeyVal('', '', true));
    }

    return _env_vars_list;
  }

  List<OptionData> get envContainerCountOptions {
    List<OptionData> options = new List<OptionData>();

    for(var i = 1; i <= 10; i++) {
//      options.add(new OptionData('$i', '$i Container${OptionData.isPlural(i)}', '${start_mates.count}'));
      options.add(new OptionData('$i', '$i', '${start_mates.count}'));
    }

    return options;
  }

  List<OptionData> get envContainerSizeStandardOptions {
    List<OptionData> options = new List<OptionData>();

    for (var i = 1; i <= 16; i++) {
      options.add(new OptionData('${i}',  '${i} SU', '${start_mates.su}'));
    }

    return options;
  }

  List<OptionData> get envContainerSizePremiumOptions {
    List<OptionData> options = new List<OptionData>();

    for (var i = 1; i <= 128; i++) {
      options.add(new OptionData('${i}',  '${i} SU', '${start_mates.su}'));
    }

    return options;
  }


  List<OptionData> get buildContainerSizeOptions {
    List<OptionData> options = new List<OptionData>();

    options.add(new OptionData('1',  '1 SU (250 MB RAM)', '${build_mates.su}'));
    options.add(new OptionData('2',  '2 SU (500 MB RAM)', '${build_mates.su}'));
    options.add(new OptionData('4',  '4 SU (1 GB RAM)', '${build_mates.su}'));
    options.add(new OptionData('8',  '8 SU (2 GB RAM)', '${build_mates.su}'));

    return options;
  }

  // NOTE: Since these options require the repo list, it can't be create here
  // statically. Instead it's created dynamicly in init() and then save back
  // to these variables for each env.
  String deploymentTypeDesc;
  List<OptionData> branchTagOptions;

  bool get isRepoDeploymentType {
    if(scm_settings.type == "git") {
      return true;
    } else {
      return false;
    }
  }

  // *****************************************************************************

  void validate() {
    isNonEmptyString(name, 'Please provide an environment name');

    // source_root
    if (SourcevoidRegex.posixPortablePath.hasMatch(source_root) == false) {
      throw new ModelValidationException('Environment source root "${source_root}" is not correctly formated (enter "/" if uncertain)');
    }

    // domain_prefix
    if(domain_prefix.length == 1) {
      if (SourcevoidRegex.domainPrefixSingleChar.hasMatch(domain_prefix) == false) {
        throw new ModelValidationException("Environment domain prefix \"${domain_prefix}\" is not correctly formated (enter the name of the env if uncertain)");
      }
    } else {
      if (SourcevoidRegex.domainPart.hasMatch(domain_prefix) == false) {
        throw new ModelValidationException("Environment domain prefix \"${domain_prefix}\" is not correctly formated (enter the name of the env if uncertain)");
      }
    }

    // path_prefix
    if (SourcevoidRegex.urlPathPrefix.hasMatch(path_prefix) == false) {
      throw new ModelValidationException('Environment path prefix "${path_prefix}" is not correctly formated (enter "/" if uncertain)');
    }

    // scm_settings
    scm_settings.validate();

    // Check container settings
    build_mates.validate();
    start_mates.validate();
  }

  // *****************************************************************************
  // TODO: Test helper getter

  var _access;
  bool get hasDatabaseAccess {
    if(_access == null) {
      _access = new Random.secure().nextBool();
    }

    return _access;
  }

  bool get hasDataDisks => (disk_ids.length > 0);

  // *****************************************************************************

}

// Helper class used in database user settings
class AppNameAndEnv {
  String app;
  Env env;
  bool appWide;
  bool envSpecific;

  AppNameAndEnv(this.app, this.env, {bool this.appWide: false, bool this.envSpecific: false});
}

// Helper class for console connection data
class ConsoleConnectionData {
  String api_client;
  String session_token;
  String user_agent;
  String wss_uri;
  int history;

  ConsoleConnectionData(this.api_client, this.session_token, this.user_agent, this.wss_uri, {this.history = 120});

  ConsoleConnectionData.fromJson(Map json) {
    api_client = json["api_client"];
    session_token = json["session_token"];
    user_agent = json["user_agent"];
    wss_uri = json["wss_uri"];
    history = json["history"];
  }

  Map toJson() {
    return {
      "api_client": api_client,
      "session_token": session_token,
      "user_agent": user_agent,
      "wss_uri": wss_uri,
      "history": history,
    };
  }
}

// Log entry class used in console
class LogEntry {
  String pipeline;
  String build_id;
  String mate_id;
  String stream;
  String data;

  LogEntry(this.pipeline, this.build_id, this.mate_id, this.stream, this.data);

  LogEntry.fromJson(Map json) {
    pipeline = json["pipeline"];
    build_id = json["build_id"];
    mate_id = json["mate_id"];
    stream = json["stream"];
    data = json["data"];
  }

  Map toJson() {
    return {
      "pipeline": pipeline,
      "build_id": build_id,
      "mate_id": mate_id,
      "stream": stream,
      "data": data
    };
  }
}

