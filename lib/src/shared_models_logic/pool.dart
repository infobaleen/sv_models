part of shared_models_logic;

class PoolLogic extends PoolCore {
  List<OptionData> generatePoolTypeOptions(Map<String, String> pool_types) {
    List<OptionData> options = new List<OptionData>();
    var currentPoolType = '${pool_type.toString().padLeft(3, "0")}';

    for (var key in pool_types.keys) {
      if(currentPoolType == '000' && key == '000') {
        options.add(new OptionData('${key}',  '${pool_types[key]} Pool', currentPoolType));
      } else if(key != '000') {
        options.add(new OptionData('${key}', '${pool_types[key]} Pool', currentPoolType));
      }
    }

    return options;
  }
}

class PoolLimits {
  int max_apps_per_pool;
  int max_app_envs_per_app;
  int max_app_funcs_per_app;
  int max_data_disks_per_pool;
  int max_certs_per_pool;

  int max_su_per_pool;
  int max_su_per_app;
  int max_su_per_env;
  int max_su_per_func;

  int max_gb_per_pool;
  int max_gb_per_disk;

  int max_su_per_app_mate;
  int max_su_per_func_mate;
  int max_su_per_service_mate;

  int max_count_per_app_env;
  int max_count_per_func_env;
  int max_count_per_service;

  PoolLimits();

  PoolLimits.fromJson(Map json) {
    max_apps_per_pool = json["max_apps_per_pool"];
    max_app_envs_per_app = json["max_app_envs_per_app"];
    max_app_funcs_per_app = json["max_app_funcs_per_app"];
    max_data_disks_per_pool = json["max_data_disks_per_pool"];
    max_certs_per_pool = json["max_certs_per_pool"];

    max_su_per_pool = json["max_su_per_pool"];
    max_su_per_app = json["max_su_per_app"];
    max_su_per_env = json["max_su_per_env"];
    max_su_per_func = json["max_su_per_func"];

    max_gb_per_pool = json["max_gb_per_pool"];
    max_gb_per_disk = json["max_gb_per_disk"];

    max_su_per_app_mate = json["max_su_per_app_mate"];
    max_su_per_func_mate = json["max_su_per_func_mate"];
    max_su_per_service_mate = json["max_su_per_service_mate"];

    max_count_per_app_env = json["max_count_per_app_env"];
    max_count_per_func_env = json["max_count_per_func_env"];
    max_count_per_service = json["max_count_per_service"];
  }

  Map toJson() {
    return {
      "max_apps_per_pool": max_apps_per_pool,
      "max_app_envs_per_app": max_app_envs_per_app,
      "max_app_funcs_per_app": max_app_funcs_per_app,
      "max_data_disks_per_pool": max_data_disks_per_pool,
      "max_certs_per_pool": max_certs_per_pool,

      "max_su_per_pool": max_su_per_pool,
      "max_su_per_app": max_su_per_app,
      "max_su_per_env": max_su_per_env,
      "max_su_per_func": max_su_per_func,

      "max_gb_per_pool": max_gb_per_pool,
      "max_gb_per_disk": max_gb_per_disk,

      "max_su_per_app_mate": max_su_per_app_mate,
      "max_su_per_func_mate": max_su_per_func_mate,
      "max_su_per_service_mate": max_su_per_service_mate,

      "max_count_per_app_env": max_count_per_app_env,
      "max_count_per_func_env": max_count_per_func_env,
      "max_count_per_service": max_count_per_service,
    };
  }
}

// Helper class for logs connection data
class PoolsLogsData {
  String api_client;
  String session_token;
  String user_agent;
  String wss_uri;
  int seconds;
  int limit;

  PoolsLogsData(this.api_client, this.session_token, this.user_agent, this.wss_uri, {this.seconds = 120, this.limit = 20});

  PoolsLogsData.fromJson(Map json) {
    api_client = json["api_client"];
    session_token = json["session_token"];
    user_agent = json["user_agent"];
    wss_uri = json["wss_uri"];
    seconds = json["seconds"];
    limit = json["limit"];
  }

  Map toJson() {
    return {
      "api_client": api_client,
      "session_token": session_token,
      "user_agent": user_agent,
      "wss_uri": wss_uri,
      "seconds": seconds,
      "limit": limit,
    };
  }
}

// Class representing the frames sent over websocket, corresponds to "LowRow"
// in the pooldb code base.
class PoolsLogsFrame {
  int id;
  String pipeline;
  String service;
  String mate;
  String stream;
  String data;
  int ts;

  PoolsLogsFrame(this.id, this.pipeline, this.service, this.mate, this.stream, this.data, this.ts);

  PoolsLogsFrame.fromJson(Map json) {
    id = json["id"];
    pipeline = json["pipeline"];
    service = json["service"];
    mate = json["mate"];
    stream = json["stream"];
    data = json["data"];
    ts = json["ts"];
  }

  Map toJson() {
    return {
      "id": id,
      "pipeline": pipeline,
      "service": service,
      "mate": mate,
      "stream": stream,
      "data": data,
      "ts": ts,
    };
  }
}
