part of shared_models_logic;

class DiskLogic extends DiskCore {
  int get size_gb {
    var quota = zfs_properties['quota'].replaceAll('GB', '');
    int size;

    try {
      size = int.parse(quota);
    } catch(_) {
      size = -1;
    }

    return size;
  }

  void set size_gb(int size) {
    zfs_properties['quota'] = '${size}GB';
  }
}
