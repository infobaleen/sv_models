part of shared_models_logic;

class NewEnvLogic extends NewEnvCore {
  void validate() {
    // NOTE: We now auto set the name if left empty on create
    // Validate name
//    isNonEmptyString(name, 'Please provide an environment name');

    // Validate mate_size
    if(su < 0 || su > 128) {
      throw new ModelValidationException('Please select a container size between 1 and 128 SU');
    }

    // Validate mate_count
    if(count < 0 || count > 10) {
      throw new ModelValidationException('Please select a container count between 1 and 10');
    }

    // root_prefix
    if (SourcevoidRegex.posixPortablePath.hasMatch(source_root) == false) {
      throw new ModelValidationException('Environment root path "${source_root}" is not correctly formated (enter "/" if uncertain)');
    }

    // NOTE: We now auto set domain_prefix if left empty on create
    // domain_prefix
    if(domain_prefix != "") {
      if(domain_prefix?.length == 1) {
        if (SourcevoidRegex.domainPrefixSingleChar.hasMatch(domain_prefix) == false) {
          throw new ModelValidationException("Environment domain prefix \"${domain_prefix}\" is not correctly formated (enter the name of the env if uncertain)");
        }
      } else {
        if (SourcevoidRegex.domainPart.hasMatch(domain_prefix) == false) {
          throw new ModelValidationException("Environment domain prefix \"${domain_prefix}\" is not correctly formated (enter the name of the env if uncertain)");
        }
      }
    }

    // path_prefix
    if (SourcevoidRegex.urlPathPrefix.hasMatch(path_prefix) == false) {
      throw new ModelValidationException('Environment path prefix "${path_prefix}" is not correctly formated (enter "/" if uncertain)');
    }
  }
}

