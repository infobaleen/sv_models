part of shared_models_logic;

const String DOMAIN_TYPE_APP_PLATFORM           = "app_platform";
const String DOMAIN_TYPE_APP_CUSTOM             = "app_custom";
const String DOMAIN_TYPE_ENV_PLATFORM           = "env_platform";
const String DOMAIN_TYPE_ENV_CUSTOM             = "env_custom";

class DomainLogic extends DomainCore {
  String type;

  String get FQDN {
    String fqdn = "";

    if (subdomain != null && subdomain != "") {
      fqdn = subdomain + ".";
    }

    if (domain != null && domain != "") {
      fqdn = fqdn + domain;
    }

    return fqdn.toLowerCase();
  }

  String get domain_link {
    var domain = this.FQDN;

    if(domain == "") {
      return "Click here to setup";
    } else {
      return domain;
    }
  }

  String get tls_compatibility_mode_capitalized {
    return tls_compatibility_mode.substring(0, 1).toUpperCase() + tls_compatibility_mode.substring(1);
  }

  // Helpers for mustache rendering
  bool get isTrueTrue => true;
  bool get isAppPlatformDomain  => type == DOMAIN_TYPE_APP_PLATFORM;
  bool get isAppCustomDomain    => type == DOMAIN_TYPE_APP_CUSTOM;
  bool get isEnvPlatformDomain  => type == DOMAIN_TYPE_ENV_PLATFORM;
  bool get isEnvCustomDomain    => type == DOMAIN_TYPE_ENV_CUSTOM;
  bool get isPlatformDomain  => type == DOMAIN_TYPE_APP_PLATFORM || type == DOMAIN_TYPE_ENV_PLATFORM;
  bool get isCustomDomain    => type == DOMAIN_TYPE_APP_CUSTOM   || type == DOMAIN_TYPE_ENV_CUSTOM;
  bool get isModeModern => tls_compatibility_mode == 'modern';
  bool get isModeIntermediate => tls_compatibility_mode == 'intermediate';
  bool get isModeOld => tls_compatibility_mode == 'old';

  String get subDomainAndDomain {
    String string = "";

    if (subdomain != null && subdomain != "") {
      string = string + subdomain + ".";
    }

    if (domain != null && domain != "") {
      string = string + domain;
    }

    return string.toLowerCase();
  }

  // Regex to check FQDN
  // Source: http://stackoverflow.com/a/18012067
  var fqdnRegExp = new RegExp(r'^(?=.{1,254}$)((?=[a-z0-9-]{1,63}\.)(xn--+)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,63}$');
  var envRegExp = new RegExp(r'^[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]$');

  void validate() {
    if(tls_compatibility_mode != 'modern' && tls_compatibility_mode != 'intermediate' && tls_compatibility_mode != 'old') {
      throw new ModelValidationException('Unsupported tls compatibility mode $tls_compatibility_mode');
    }

    // Validate domain values if provided
    if(subdomain != "" || domain != "") {
      // domain
      isNonEmptyString(domain, 'Domain value can\'t be empty');

      // Check how many dots the domain field contains
      var domainSplit = domain.split(".");

      // Make sure custom domain value is valid
      if(domainSplit.length != 2) {
        throw new ModelValidationException("Please provide a valid domain, eg. \"example.com\"");
      } else if(fqdnRegExp.hasMatch(domain) == false) {
        throw new ModelValidationException("Your domain value is not correctly formated");
      }

      // sub_domain
      isNonEmptyString(subdomain, 'Sub domain value can\'t be empty');
      if(subdomain.contains('.') == true) {
        throw new ModelValidationException("Sub domain can not contain any dots");
      }
    }
  }
}

