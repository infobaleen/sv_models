library models.mysql_database;

import 'package:sv_models/src/dart_only_models/id.dart';

class MysqlDatabase extends Object with Id {
  String org_id;
  String pool_id;
  String service_id;
  String database_name;

  MysqlDatabase();

  MysqlDatabase.fromJson(Map json) {
    org_id = json['org_id'];
    pool_id = json['pool_id'];
    service_id = json['service_id'];
    database_name = json['database_name'];
  }

  Map toJson() {
    return {
      "org_id": org_id,
      "pool_id": pool_id,
      "service_id": service_id,
      "database_name": database_name,
    };
  }

  String get database_name_encoded {
    return Uri.encodeComponent(database_name);
  }
}

