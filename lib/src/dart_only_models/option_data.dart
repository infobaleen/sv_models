library models.option_data;

// NOTE: This class is only used on client side to generate select items

class OptionData {
  String value;
  String label;
  String disabled;
  String settingsValue;

  OptionData(this.value, this.label, this.settingsValue, [this.disabled]);

//  OptionData.fromJson(Map json) {
//    value = json['value'];
//    label = json['label'];
//    selected = json['selected'];
//  }
//
//  Map toJson() {
//    return {
//      "value": value,
//      "label": label,
//      "selected": selected,
//    };
//  }

//  static String isSelected(val1, val2) {
//    if(val1 == val2) {
//      return 'selected';
//    } else {
//      return '';
//    }
//  }

  String get selected {
    if(value == settingsValue) {
      return 'selected';
    } else {
      return '';
    }
  }

  static String isPlural(int i) {
    if (i > 1) {
      return "s";
    } else {
      return "";
    }
  }
}

