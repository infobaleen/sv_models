library models.id;

/// Mixin used to generate unique id's used for css selectors.
/// Css selectors can not start with a number.
class Id {
  static int _id = 0;
  final String id = 'id-${++_id}';
}

