library models.vat;

// NOTE: Actual vat rate is looked up server side. We use this primarily to
//       render the view differently for VAT and non VAT orgs.

class VatLookup {
  static bool isVatCountry(String country_code) {
    var vatRequired = false;

    for(var vatCountry in euVatRates) {
      if(vatCountry.country_code == country_code) {
        vatRequired = true;
        break;
      }
    }

    return vatRequired;
  }
}

class CountryVat {
  String country;
  String country_code;
  num vat_rate;

  CountryVat(this.country, this.country_code, this.vat_rate);
}

// TODO: Refactor as map instead with country code as key? More efficient
// lookup...

// Rates are designed such that you can take "value * rate", a rate of 0% is
// therefor "1" and so on.
List<CountryVat> euVatRates = [
  new CountryVat('Austria',         'AT', 1.20),
  new CountryVat('Belgium',         'BE', 1.21),
  new CountryVat('Bulgaria',        'BG', 1.20),
  new CountryVat('Croatia',         'HR', 1.25),
  new CountryVat('Cyprus',          'CY', 1.19),
  new CountryVat('Czech Republic',  'CZ', 1.21),
  new CountryVat('Denmark',         'DK', 1.25),
  new CountryVat('Estonia',         'EE', 1.20),
  new CountryVat('Finland',         'FI', 1.24),
  new CountryVat('France',          'FR', 1.20),
  new CountryVat('Germany',         'DE', 1.19),
  new CountryVat('Greece',          'EL', 1.23),
  new CountryVat('Hungary',         'HU', 1.27),
  new CountryVat('Ireland',         'IE', 1.23),
  new CountryVat('Italy',           'IT', 1.22),
  new CountryVat('Latvia',          'LV', 1.21),
  new CountryVat('Lithuania',       'LT', 1.21),
  new CountryVat('Luxembourg',      'LU', 1.17),
  new CountryVat('Malta',           'MT', 1.18),
  new CountryVat('Netherlands',     'NL', 1.21),
  new CountryVat('Poland',          'PL', 1.23),
  new CountryVat('Portugal',        'PT', 1.23),
  new CountryVat('Romania',         'RO', 1.20),
  new CountryVat('Slovakia',        'SK', 1.20),
  new CountryVat('Slovenia',        'SI', 1.22),
  new CountryVat('Spain',           'ES', 1.21),
  new CountryVat('Sweden',          'SE', 1.25),
  new CountryVat('United Kingdom',  'UK', 1.20),
];

