library models.mysql_table;

import 'package:sv_models/src/dart_only_models/id.dart';

class MysqlTable extends Object with Id {
  String org_id;
  String pool_id;
  String service_id;
  String database_name;
  String table_name;

  MysqlTable([this.org_id, this.pool_id, this.service_id, this.database_name, this.table_name]);

  MysqlTable.fromJson(Map json) {
    org_id = json['org_id'];
    pool_id = json['pool_id'];
    service_id = json['service_id'];
    database_name = json['database_name'];
    table_name = json['table_name'];
  }

  Map toJson() {
    return {
      "org_id": org_id,
      "pool_id": pool_id,
      "service_id": service_id,
      "database_name": database_name,
      "table_name": table_name,
    };
  }

  String get database_name_encoded {
    return Uri.encodeComponent(database_name);
  }
}

