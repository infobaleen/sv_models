library models.mongodb_database;

import 'package:sv_models/src/dart_only_models/id.dart';

class MongodbDatabase extends Object with Id {
  String org_id;
  String pool_id;
  String service_id;
  String database_type; // TODO: Remove? Not used?

  String database_name;
  String description;

  // Runtime data, not stored or handled by Lockdown or RPC backend
  int collections;
  int indexes;
  num size;

  MongodbDatabase();

  MongodbDatabase.fromJson(Map json) {
    org_id = json['org_id'];
    pool_id = json['pool_id'];
    service_id = json['service_id'];
    database_type = json['database_type'];
    database_name = json['database_name'];
    description = json['description'];

    collections = json['collections'];
    indexes = json['Indexes'];
    size = json['size'];
  }

  Map toJson() {
    return {
      "org_id": org_id,
      "pool_id": pool_id,
      "service_id": service_id,
      "database_type": database_type,
      "database_name": database_name,
      "description": description,

      "collections": collections,
      "Indexes": indexes,
      "size": size
    };
  }

  // Show size in MB
  String get sizeMb => (size / (1024 * 1024)).toStringAsFixed(2);

  String get database_name_encoded {
    return Uri.encodeComponent(database_name);
  }
}

