library models.regex;

class SourcevoidRegex {
  // Regex for checking part of a domain name, can't start or end with "-"
  static RegExp domainPart = new RegExp(r'^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]$');

  // Regex for checking single char domain prefix, used if domain_prefix has
  // a length of 1
  static RegExp domainPrefixSingleChar = new RegExp(r'^[a-zA-Z0-9]$');

  // Regex for checking file system directory path, checking according to
  // POSIX "Fully portable filenames".
  static RegExp posixPortablePath = new RegExp(r'^(\/{1}([a-zA-Z0-9._]{1}[a-zA-Z0-9._-]*)*)+$');

  // Regex for checking url path_prefix, (avoid "~" because of Nginx settings)
  static RegExp urlPathPrefix = new RegExp(r'^(\/{1}[a-zA-Z0-9-.]*)+$');
}

