library models.mongodb_collection;

import 'package:sv_models/src/dart_only_models/id.dart';

class MongodbCollection extends Object with Id {
  String org_id;
  String pool_id;
  String service_id;
  String database_name;
  String collection_name;
  int count;
  int nindexes;
  num size;
  num totalIndexSize;
  Map indexSizes;
  bool capped;

  // Only present for capped collections
  num max;
  num maxSize;

  MongodbCollection([this.org_id, this.pool_id, this.service_id, this.database_name, this.collection_name]);

  MongodbCollection.fromJson(Map json) {
    org_id = json['org_id'];
    pool_id = json['pool_id'];
    service_id = json['service_id'];
    database_name = json['database_name'];
    collection_name = json['collection_name'];
    count = json['count'];
    nindexes = json['nindexes'];
    size = json['size'];
    totalIndexSize = json['totalIndexSize'];
    indexSizes = json['indexSizes'];
    capped = json['capped'];
    if(json['max'] != null) {
      max = json['max'];
    }
    if(json['maxSize'] != null) {
      maxSize = json['maxSize'];
    }
  }

  Map toJson() {
    return {
      "org_id": org_id,
      "pool_id": pool_id,
      "service_id": service_id,
      "database_name": database_name,
      "collection_name": collection_name,
      "count": count,
      "nindexes": nindexes,
      "size": size,
      "totalIndexSize": totalIndexSize,
      "indexSizes": indexSizes,
      "capped": capped,
      "max": max,
      "maxSize": maxSize
    };
  }

  // Show size in MB
  String get sizeMb => (size / (1024 * 1024)).toStringAsFixed(2);

  String get database_name_encoded {
    return Uri.encodeComponent(database_name);
  }
}

