library models.exceptions;

class ModelValidationException implements Exception {
  final String msg;
  const ModelValidationException([this.msg]);

  String toString() => msg ?? 'ValidationException';
}

void isNonEmptyString(String str, String msg) {
  if(str == null || str == "") {
    throw new ModelValidationException(msg);
  }
}

