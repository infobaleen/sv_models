// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class PoolsArgs {
  PoolsDeleteArgs delete = new PoolsDeleteArgs();
  PoolsInsertArgs insert = new PoolsInsertArgs();
  PoolsListArgs list = new PoolsListArgs();
  PoolsRetrieveArgs retrieve = new PoolsRetrieveArgs();
  PoolsUpdateArgs update = new PoolsUpdateArgs();
  PoolsLogsArgs logs = new PoolsLogsArgs();

  PoolsArgs({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.logs,
  });

  PoolsArgs.fromJson(Map j) {
    delete = new PoolsDeleteArgs.fromJson(j['delete']) ?? new PoolsDeleteArgs();
    insert = new PoolsInsertArgs.fromJson(j['insert']) ?? new PoolsInsertArgs();
    list = new PoolsListArgs.fromJson(j['list']) ?? new PoolsListArgs();
    retrieve = new PoolsRetrieveArgs.fromJson(j['retrieve']) ?? new PoolsRetrieveArgs();
    update = new PoolsUpdateArgs.fromJson(j['update']) ?? new PoolsUpdateArgs();
    logs = new PoolsLogsArgs.fromJson(j['logs']) ?? new PoolsLogsArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new PoolsDeleteArgs(),
      'insert': insert ?? new PoolsInsertArgs(),
      'list': list ?? new PoolsListArgs(),
      'retrieve': retrieve ?? new PoolsRetrieveArgs(),
      'update': update ?? new PoolsUpdateArgs(),
      'logs': logs ?? new PoolsLogsArgs(),
    };
  }
}

class PoolsDeleteArgs {
  String org_id = "";
  String pool_id = "";

  PoolsDeleteArgs([
    this.org_id,
    this.pool_id,
  ]);

  PoolsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class PoolsInsertArgs {
  String org_id = "";
  String name = "";
  int pool_type = 0;

  PoolsInsertArgs([
    this.org_id,
    this.name,
    this.pool_type,
  ]);

  PoolsInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    name = j['name'] ?? "";
    pool_type = j['pool_type'] ?? 0;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'name': name ?? "",
      'pool_type': pool_type ?? 0,
    };
  }
}

class PoolsListArgs {
  String org_id = "";

  PoolsListArgs([
    this.org_id,
  ]);

  PoolsListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class PoolsRetrieveArgs {
  String org_id = "";
  String pool_id = "";

  PoolsRetrieveArgs([
    this.org_id,
    this.pool_id,
  ]);

  PoolsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class PoolsUpdateArgs {
  String org_id = "";
  String pool_id = "";
  Pool pool = new Pool();

  PoolsUpdateArgs([
    this.org_id,
    this.pool_id,
    this.pool,
  ]);

  PoolsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    pool = new Pool.fromJson(j['pool']) ?? new Pool();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'pool': pool ?? new Pool(),
    };
  }
}

class PoolsLogsArgs {
  String org_id = "";
  String pool_id = "";

  PoolsLogsArgs([
    this.org_id,
    this.pool_id,
  ]);

  PoolsLogsArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class PoolsReply {
  PoolsDeleteReply delete = new PoolsDeleteReply();
  PoolsInsertReply insert = new PoolsInsertReply();
  PoolsListReply list = new PoolsListReply();
  PoolsRetrieveReply retrieve = new PoolsRetrieveReply();
  PoolsUpdateReply update = new PoolsUpdateReply();
  PoolsLogsReply logs = new PoolsLogsReply();

  PoolsReply({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.logs,
  });

  PoolsReply.fromJson(Map j) {
    delete = new PoolsDeleteReply.fromJson(j['delete']) ?? new PoolsDeleteReply();
    insert = new PoolsInsertReply.fromJson(j['insert']) ?? new PoolsInsertReply();
    list = new PoolsListReply.fromJson(j['list']) ?? new PoolsListReply();
    retrieve = new PoolsRetrieveReply.fromJson(j['retrieve']) ?? new PoolsRetrieveReply();
    update = new PoolsUpdateReply.fromJson(j['update']) ?? new PoolsUpdateReply();
    logs = new PoolsLogsReply.fromJson(j['logs']) ?? new PoolsLogsReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new PoolsDeleteReply(),
      'insert': insert ?? new PoolsInsertReply(),
      'list': list ?? new PoolsListReply(),
      'retrieve': retrieve ?? new PoolsRetrieveReply(),
      'update': update ?? new PoolsUpdateReply(),
      'logs': logs ?? new PoolsLogsReply(),
    };
  }
}

class PoolsDeleteReply {
  String emtpy = "";

  PoolsDeleteReply([
    this.emtpy,
  ]);

  PoolsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class PoolsInsertReply {
  String insert_id = "";

  PoolsInsertReply([
    this.insert_id,
  ]);

  PoolsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class PoolsListReply {
  List<Pool> pools = new List<Pool>();

  PoolsListReply([
    this.pools,
  ]);

  PoolsListReply.fromJson(Map j) {
    j['pools']?.map((item) => new Pool.fromJson(item))?.forEach(pools.add);
  }

  Map toJson() {
    return {
      'pools': pools?.map((item) => item.toJson())?.toList() ?? new List<Pool>(),
    };
  }
}

class PoolsRetrieveReply {
  Pool pool = new Pool();

  PoolsRetrieveReply([
    this.pool,
  ]);

  PoolsRetrieveReply.fromJson(Map j) {
    pool = new Pool.fromJson(j['pool']) ?? new Pool();
  }

  Map toJson() {
    return {
      'pool': pool ?? new Pool(),
    };
  }
}

class PoolsUpdateReply {
  String empty = "";

  PoolsUpdateReply([
    this.empty,
  ]);

  PoolsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class PoolsLogsReply {
  String database_uri = "";

  PoolsLogsReply([
    this.database_uri,
  ]);

  PoolsLogsReply.fromJson(Map j) {
    database_uri = j['database_uri'] ?? "";
  }

  Map toJson() {
    return {
      'database_uri': database_uri ?? "",
    };
  }
}

