// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class CertsArgs {
  CertsDeleteArgs delete = new CertsDeleteArgs();
  CertsInsertArgs insert = new CertsInsertArgs();
  CertsListArgs list = new CertsListArgs();
  CertsRetrieveArgs retrieve = new CertsRetrieveArgs();
  CertsUpdateArgs update = new CertsUpdateArgs();

  CertsArgs({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
  });

  CertsArgs.fromJson(Map j) {
    delete = new CertsDeleteArgs.fromJson(j['delete']) ?? new CertsDeleteArgs();
    insert = new CertsInsertArgs.fromJson(j['insert']) ?? new CertsInsertArgs();
    list = new CertsListArgs.fromJson(j['list']) ?? new CertsListArgs();
    retrieve = new CertsRetrieveArgs.fromJson(j['retrieve']) ?? new CertsRetrieveArgs();
    update = new CertsUpdateArgs.fromJson(j['update']) ?? new CertsUpdateArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new CertsDeleteArgs(),
      'insert': insert ?? new CertsInsertArgs(),
      'list': list ?? new CertsListArgs(),
      'retrieve': retrieve ?? new CertsRetrieveArgs(),
      'update': update ?? new CertsUpdateArgs(),
    };
  }
}

class CertsDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String cert_id = "";

  CertsDeleteArgs([
    this.org_id,
    this.pool_id,
    this.cert_id,
  ]);

  CertsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    cert_id = j['cert_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'cert_id': cert_id ?? "",
    };
  }
}

class CertsInsertArgs {
  String org_id = "";
  String pool_id = "";
  String name = "";
  String description = "";
  String certificate = "";
  String private_key = "";

  CertsInsertArgs([
    this.org_id,
    this.pool_id,
    this.name,
    this.description,
    this.certificate,
    this.private_key,
  ]);

  CertsInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    name = j['name'] ?? "";
    description = j['description'] ?? "";
    certificate = j['certificate'] ?? "";
    private_key = j['private_key'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'name': name ?? "",
      'description': description ?? "",
      'certificate': certificate ?? "",
      'private_key': private_key ?? "",
    };
  }
}

class CertsListArgs {
  String org_id = "";
  String pool_id = "";

  CertsListArgs([
    this.org_id,
    this.pool_id,
  ]);

  CertsListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class CertsRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String cert_id = "";

  CertsRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.cert_id,
  ]);

  CertsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    cert_id = j['cert_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'cert_id': cert_id ?? "",
    };
  }
}

class CertsUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String cert_id = "";
  Cert cert = new Cert();

  CertsUpdateArgs([
    this.org_id,
    this.pool_id,
    this.cert_id,
    this.cert,
  ]);

  CertsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    cert_id = j['cert_id'] ?? "";
    cert = new Cert.fromJson(j['cert']) ?? new Cert();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'cert_id': cert_id ?? "",
      'cert': cert ?? new Cert(),
    };
  }
}

class CertsReply {
  CertsDeleteReply delete = new CertsDeleteReply();
  CertsInsertReply insert = new CertsInsertReply();
  CertsListReply list = new CertsListReply();
  CertsRetrieveReply retrieve = new CertsRetrieveReply();
  CertsUpdateReply update = new CertsUpdateReply();

  CertsReply({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
  });

  CertsReply.fromJson(Map j) {
    delete = new CertsDeleteReply.fromJson(j['delete']) ?? new CertsDeleteReply();
    insert = new CertsInsertReply.fromJson(j['insert']) ?? new CertsInsertReply();
    list = new CertsListReply.fromJson(j['list']) ?? new CertsListReply();
    retrieve = new CertsRetrieveReply.fromJson(j['retrieve']) ?? new CertsRetrieveReply();
    update = new CertsUpdateReply.fromJson(j['update']) ?? new CertsUpdateReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new CertsDeleteReply(),
      'insert': insert ?? new CertsInsertReply(),
      'list': list ?? new CertsListReply(),
      'retrieve': retrieve ?? new CertsRetrieveReply(),
      'update': update ?? new CertsUpdateReply(),
    };
  }
}

class CertsDeleteReply {
  String emtpy = "";

  CertsDeleteReply([
    this.emtpy,
  ]);

  CertsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class CertsInsertReply {
  String insert_id = "";

  CertsInsertReply([
    this.insert_id,
  ]);

  CertsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class CertsListReply {
  List<Cert> certs = new List<Cert>();

  CertsListReply([
    this.certs,
  ]);

  CertsListReply.fromJson(Map j) {
    j['certs']?.map((item) => new Cert.fromJson(item))?.forEach(certs.add);
  }

  Map toJson() {
    return {
      'certs': certs?.map((item) => item.toJson())?.toList() ?? new List<Cert>(),
    };
  }
}

class CertsRetrieveReply {
  Cert cert = new Cert();

  CertsRetrieveReply([
    this.cert,
  ]);

  CertsRetrieveReply.fromJson(Map j) {
    cert = new Cert.fromJson(j['cert']) ?? new Cert();
  }

  Map toJson() {
    return {
      'cert': cert ?? new Cert(),
    };
  }
}

class CertsUpdateReply {
  String empty = "";

  CertsUpdateReply([
    this.empty,
  ]);

  CertsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

