// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class ServiceUsersArgs {
  ServiceUsersDeleteArgs delete = new ServiceUsersDeleteArgs();
  ServiceUsersInsertArgs insert = new ServiceUsersInsertArgs();
  ServiceUsersListArgs list = new ServiceUsersListArgs();
  ServiceUsersRetrieveArgs retrieve = new ServiceUsersRetrieveArgs();
  ServiceUsersUpdateArgs update = new ServiceUsersUpdateArgs();
  ServiceUsersResetArgs reset = new ServiceUsersResetArgs();

  ServiceUsersArgs({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.reset,
  });

  ServiceUsersArgs.fromJson(Map j) {
    delete = new ServiceUsersDeleteArgs.fromJson(j['delete']) ?? new ServiceUsersDeleteArgs();
    insert = new ServiceUsersInsertArgs.fromJson(j['insert']) ?? new ServiceUsersInsertArgs();
    list = new ServiceUsersListArgs.fromJson(j['list']) ?? new ServiceUsersListArgs();
    retrieve = new ServiceUsersRetrieveArgs.fromJson(j['retrieve']) ?? new ServiceUsersRetrieveArgs();
    update = new ServiceUsersUpdateArgs.fromJson(j['update']) ?? new ServiceUsersUpdateArgs();
    reset = new ServiceUsersResetArgs.fromJson(j['reset']) ?? new ServiceUsersResetArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServiceUsersDeleteArgs(),
      'insert': insert ?? new ServiceUsersInsertArgs(),
      'list': list ?? new ServiceUsersListArgs(),
      'retrieve': retrieve ?? new ServiceUsersRetrieveArgs(),
      'update': update ?? new ServiceUsersUpdateArgs(),
      'reset': reset ?? new ServiceUsersResetArgs(),
    };
  }
}

class ServiceUsersDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";
  bool service_resource = false;

  ServiceUsersDeleteArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service_username,
    this.service_resource,
  ]);

  ServiceUsersDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
    service_resource = j['service_resource'] ?? false;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
      'service_resource': service_resource ?? false,
    };
  }
}

class ServiceUsersInsertArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";
  String service_password = "";
  bool service_resource = false;
  String description = "";

  ServiceUsersInsertArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service_username,
    this.service_password,
    this.service_resource,
    this.description,
  ]);

  ServiceUsersInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
    service_password = j['service_password'] ?? "";
    service_resource = j['service_resource'] ?? false;
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
      'service_password': service_password ?? "",
      'service_resource': service_resource ?? false,
      'description': description ?? "",
    };
  }
}

class ServiceUsersListArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";

  ServiceUsersListArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
  ]);

  ServiceUsersListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
    };
  }
}

class ServiceUsersRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";

  ServiceUsersRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service_username,
  ]);

  ServiceUsersRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
    };
  }
}

class ServiceUsersUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";
  ServiceUser service_user = new ServiceUser();

  ServiceUsersUpdateArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service_username,
    this.service_user,
  ]);

  ServiceUsersUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
    service_user = new ServiceUser.fromJson(j['service_user']) ?? new ServiceUser();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
      'service_user': service_user ?? new ServiceUser(),
    };
  }
}

class ServiceUsersResetArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String service_username = "";

  ServiceUsersResetArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service_username,
  ]);

  ServiceUsersResetArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_username = j['service_username'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service_username': service_username ?? "",
    };
  }
}

class ServiceUsersReply {
  ServiceUsersDeleteReply delete = new ServiceUsersDeleteReply();
  ServiceUsersInsertReply insert = new ServiceUsersInsertReply();
  ServiceUsersListReply list = new ServiceUsersListReply();
  ServiceUsersRetrieveReply retrieve = new ServiceUsersRetrieveReply();
  ServiceUsersUpdateReply update = new ServiceUsersUpdateReply();
  ServiceUsersResetReply reset = new ServiceUsersResetReply();

  ServiceUsersReply({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.reset,
  });

  ServiceUsersReply.fromJson(Map j) {
    delete = new ServiceUsersDeleteReply.fromJson(j['delete']) ?? new ServiceUsersDeleteReply();
    insert = new ServiceUsersInsertReply.fromJson(j['insert']) ?? new ServiceUsersInsertReply();
    list = new ServiceUsersListReply.fromJson(j['list']) ?? new ServiceUsersListReply();
    retrieve = new ServiceUsersRetrieveReply.fromJson(j['retrieve']) ?? new ServiceUsersRetrieveReply();
    update = new ServiceUsersUpdateReply.fromJson(j['update']) ?? new ServiceUsersUpdateReply();
    reset = new ServiceUsersResetReply.fromJson(j['reset']) ?? new ServiceUsersResetReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServiceUsersDeleteReply(),
      'insert': insert ?? new ServiceUsersInsertReply(),
      'list': list ?? new ServiceUsersListReply(),
      'retrieve': retrieve ?? new ServiceUsersRetrieveReply(),
      'update': update ?? new ServiceUsersUpdateReply(),
      'reset': reset ?? new ServiceUsersResetReply(),
    };
  }
}

class ServiceUsersDeleteReply {
  String emtpy = "";

  ServiceUsersDeleteReply([
    this.emtpy,
  ]);

  ServiceUsersDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class ServiceUsersInsertReply {
  String insert_id = "";

  ServiceUsersInsertReply([
    this.insert_id,
  ]);

  ServiceUsersInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class ServiceUsersListReply {
  List<ServiceUser> service_users = new List<ServiceUser>();

  ServiceUsersListReply([
    this.service_users,
  ]);

  ServiceUsersListReply.fromJson(Map j) {
    j['service_users']?.map((item) => new ServiceUser.fromJson(item))?.forEach(service_users.add);
  }

  Map toJson() {
    return {
      'service_users': service_users?.map((item) => item.toJson())?.toList() ?? new List<ServiceUser>(),
    };
  }
}

class ServiceUsersRetrieveReply {
  ServiceUser service_user = new ServiceUser();

  ServiceUsersRetrieveReply([
    this.service_user,
  ]);

  ServiceUsersRetrieveReply.fromJson(Map j) {
    service_user = new ServiceUser.fromJson(j['service_user']) ?? new ServiceUser();
  }

  Map toJson() {
    return {
      'service_user': service_user ?? new ServiceUser(),
    };
  }
}

class ServiceUsersUpdateReply {
  ServiceUser service_user = new ServiceUser();

  ServiceUsersUpdateReply([
    this.service_user,
  ]);

  ServiceUsersUpdateReply.fromJson(Map j) {
    service_user = new ServiceUser.fromJson(j['service_user']) ?? new ServiceUser();
  }

  Map toJson() {
    return {
      'service_user': service_user ?? new ServiceUser(),
    };
  }
}

class ServiceUsersResetReply {
  String emtpy = "";

  ServiceUsersResetReply([
    this.emtpy,
  ]);

  ServiceUsersResetReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

