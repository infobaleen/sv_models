// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class AccountsArgs {
  AccountsDeleteArgs delete = new AccountsDeleteArgs();
  AccountsInsertArgs insert = new AccountsInsertArgs();
  AccountsResetArgs reset = new AccountsResetArgs();
  AccountsVerifyEmailArgs verifyEmail = new AccountsVerifyEmailArgs();
  AccountsVerifyPhoneArgs verifyPhone = new AccountsVerifyPhoneArgs();
  AccountsVerifyResetArgs verifyReset = new AccountsVerifyResetArgs();
  AccountsRetrieveArgs retrieve = new AccountsRetrieveArgs();
  AccountsUpdateArgs update = new AccountsUpdateArgs();
  AccountsSignInArgs signIn = new AccountsSignInArgs();
  AccountsSignOutArgs signOut = new AccountsSignOutArgs();
  AccountsSessionsArgs sessions = new AccountsSessionsArgs();
  AccountsOperationsArgs operations = new AccountsOperationsArgs();
  AccountsOrgsArgs orgs = new AccountsOrgsArgs();

  AccountsArgs({
    this.delete,
    this.insert,
    this.reset,
    this.verifyEmail,
    this.verifyPhone,
    this.verifyReset,
    this.retrieve,
    this.update,
    this.signIn,
    this.signOut,
    this.sessions,
    this.operations,
    this.orgs,
  });

  AccountsArgs.fromJson(Map j) {
    delete = new AccountsDeleteArgs.fromJson(j['delete']) ?? new AccountsDeleteArgs();
    insert = new AccountsInsertArgs.fromJson(j['insert']) ?? new AccountsInsertArgs();
    reset = new AccountsResetArgs.fromJson(j['reset']) ?? new AccountsResetArgs();
    verifyEmail = new AccountsVerifyEmailArgs.fromJson(j['verifyEmail']) ?? new AccountsVerifyEmailArgs();
    verifyPhone = new AccountsVerifyPhoneArgs.fromJson(j['verifyPhone']) ?? new AccountsVerifyPhoneArgs();
    verifyReset = new AccountsVerifyResetArgs.fromJson(j['verifyReset']) ?? new AccountsVerifyResetArgs();
    retrieve = new AccountsRetrieveArgs.fromJson(j['retrieve']) ?? new AccountsRetrieveArgs();
    update = new AccountsUpdateArgs.fromJson(j['update']) ?? new AccountsUpdateArgs();
    signIn = new AccountsSignInArgs.fromJson(j['signIn']) ?? new AccountsSignInArgs();
    signOut = new AccountsSignOutArgs.fromJson(j['signOut']) ?? new AccountsSignOutArgs();
    sessions = new AccountsSessionsArgs.fromJson(j['sessions']) ?? new AccountsSessionsArgs();
    operations = new AccountsOperationsArgs.fromJson(j['operations']) ?? new AccountsOperationsArgs();
    orgs = new AccountsOrgsArgs.fromJson(j['orgs']) ?? new AccountsOrgsArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new AccountsDeleteArgs(),
      'insert': insert ?? new AccountsInsertArgs(),
      'reset': reset ?? new AccountsResetArgs(),
      'verifyEmail': verifyEmail ?? new AccountsVerifyEmailArgs(),
      'verifyPhone': verifyPhone ?? new AccountsVerifyPhoneArgs(),
      'verifyReset': verifyReset ?? new AccountsVerifyResetArgs(),
      'retrieve': retrieve ?? new AccountsRetrieveArgs(),
      'update': update ?? new AccountsUpdateArgs(),
      'signIn': signIn ?? new AccountsSignInArgs(),
      'signOut': signOut ?? new AccountsSignOutArgs(),
      'sessions': sessions ?? new AccountsSessionsArgs(),
      'operations': operations ?? new AccountsOperationsArgs(),
      'orgs': orgs ?? new AccountsOrgsArgs(),
    };
  }
}

class AccountsDeleteArgs {

  AccountsDeleteArgs();

  AccountsDeleteArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsInsertArgs {
  String email = "";
  Utm utm = new Utm();

  AccountsInsertArgs([
    this.email,
    this.utm,
  ]);

  AccountsInsertArgs.fromJson(Map j) {
    email = j['email'] ?? "";
    utm = new Utm.fromJson(j['utm']) ?? new Utm();
  }

  Map toJson() {
    return {
      'email': email ?? "",
      'utm': utm ?? new Utm(),
    };
  }
}

class AccountsResetArgs {
  String email = "";

  AccountsResetArgs([
    this.email,
  ]);

  AccountsResetArgs.fromJson(Map j) {
    email = j['email'] ?? "";
  }

  Map toJson() {
    return {
      'email': email ?? "",
    };
  }
}

class AccountsVerifyEmailArgs {
  String email_token = "";
  String phone = "";

  AccountsVerifyEmailArgs([
    this.email_token,
    this.phone,
  ]);

  AccountsVerifyEmailArgs.fromJson(Map j) {
    email_token = j['email_token'] ?? "";
    phone = j['phone'] ?? "";
  }

  Map toJson() {
    return {
      'email_token': email_token ?? "",
      'phone': phone ?? "",
    };
  }
}

class AccountsVerifyPhoneArgs {
  String first_name = "";
  String last_name = "";
  String password = "";
  String email_token = "";
  String phone_code = "";
  bool news_letter = false;

  AccountsVerifyPhoneArgs([
    this.first_name,
    this.last_name,
    this.password,
    this.email_token,
    this.phone_code,
    this.news_letter,
  ]);

  AccountsVerifyPhoneArgs.fromJson(Map j) {
    first_name = j['first_name'] ?? "";
    last_name = j['last_name'] ?? "";
    password = j['password'] ?? "";
    email_token = j['email_token'] ?? "";
    phone_code = j['phone_code'] ?? "";
    news_letter = j['news_letter'] ?? false;
  }

  Map toJson() {
    return {
      'first_name': first_name ?? "",
      'last_name': last_name ?? "",
      'password': password ?? "",
      'email_token': email_token ?? "",
      'phone_code': phone_code ?? "",
      'news_letter': news_letter ?? false,
    };
  }
}

class AccountsVerifyResetArgs {
  String email = "";
  String reset_token = "";
  String password = "";

  AccountsVerifyResetArgs([
    this.email,
    this.reset_token,
    this.password,
  ]);

  AccountsVerifyResetArgs.fromJson(Map j) {
    email = j['email'] ?? "";
    reset_token = j['reset_token'] ?? "";
    password = j['password'] ?? "";
  }

  Map toJson() {
    return {
      'email': email ?? "",
      'reset_token': reset_token ?? "",
      'password': password ?? "",
    };
  }
}

class AccountsRetrieveArgs {

  AccountsRetrieveArgs();

  AccountsRetrieveArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsUpdateArgs {
  Account account = new Account();

  AccountsUpdateArgs([
    this.account,
  ]);

  AccountsUpdateArgs.fromJson(Map j) {
    account = new Account.fromJson(j['account']) ?? new Account();
  }

  Map toJson() {
    return {
      'account': account ?? new Account(),
    };
  }
}

class AccountsSignInArgs {
  String identifier = "";
  String password = "";
  String two_step_code = "";
  Duration ttl = new Duration();

  AccountsSignInArgs([
    this.identifier,
    this.password,
    this.two_step_code,
    this.ttl,
  ]);

  AccountsSignInArgs.fromJson(Map j) {
    identifier = j['identifier'] ?? "";
    password = j['password'] ?? "";
    two_step_code = j['two_step_code'] ?? "";
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
  }

  Map toJson() {
    return {
      'identifier': identifier ?? "",
      'password': password ?? "",
      'two_step_code': two_step_code ?? "",
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
    };
  }
}

class AccountsSignOutArgs {
  String session_id = "";

  AccountsSignOutArgs([
    this.session_id,
  ]);

  AccountsSignOutArgs.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
    };
  }
}

class AccountsSessionsArgs {

  AccountsSessionsArgs();

  AccountsSessionsArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsOperationsArgs {
  int limit = 0;
  int skip = 0;

  AccountsOperationsArgs([
    this.limit,
    this.skip,
  ]);

  AccountsOperationsArgs.fromJson(Map j) {
    limit = j['limit'] ?? 0;
    skip = j['skip'] ?? 0;
  }

  Map toJson() {
    return {
      'limit': limit ?? 0,
      'skip': skip ?? 0,
    };
  }
}

class AccountsOrgsArgs {

  AccountsOrgsArgs();

  AccountsOrgsArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsReply {
  AccountsDeleteReply delete = new AccountsDeleteReply();
  AccountsInsertReply insert = new AccountsInsertReply();
  AccountsResetReply reset = new AccountsResetReply();
  AccountsVerifyEmailReply verifyEmail = new AccountsVerifyEmailReply();
  AccountsVerifyPhoneReply verifyPhone = new AccountsVerifyPhoneReply();
  AccountsVerifyResetReply verifyReset = new AccountsVerifyResetReply();
  AccountsRetrieveReply retrieve = new AccountsRetrieveReply();
  AccountsUpdateReply update = new AccountsUpdateReply();
  AccountsSignInReply signIn = new AccountsSignInReply();
  AccountsSignOutReply signOut = new AccountsSignOutReply();
  AccountsSessionsReply sessions = new AccountsSessionsReply();
  AccountsOperationsReply operations = new AccountsOperationsReply();
  AccountsOrgsReply orgs = new AccountsOrgsReply();

  AccountsReply({
    this.delete,
    this.insert,
    this.reset,
    this.verifyEmail,
    this.verifyPhone,
    this.verifyReset,
    this.retrieve,
    this.update,
    this.signIn,
    this.signOut,
    this.sessions,
    this.operations,
    this.orgs,
  });

  AccountsReply.fromJson(Map j) {
    delete = new AccountsDeleteReply.fromJson(j['delete']) ?? new AccountsDeleteReply();
    insert = new AccountsInsertReply.fromJson(j['insert']) ?? new AccountsInsertReply();
    reset = new AccountsResetReply.fromJson(j['reset']) ?? new AccountsResetReply();
    verifyEmail = new AccountsVerifyEmailReply.fromJson(j['verifyEmail']) ?? new AccountsVerifyEmailReply();
    verifyPhone = new AccountsVerifyPhoneReply.fromJson(j['verifyPhone']) ?? new AccountsVerifyPhoneReply();
    verifyReset = new AccountsVerifyResetReply.fromJson(j['verifyReset']) ?? new AccountsVerifyResetReply();
    retrieve = new AccountsRetrieveReply.fromJson(j['retrieve']) ?? new AccountsRetrieveReply();
    update = new AccountsUpdateReply.fromJson(j['update']) ?? new AccountsUpdateReply();
    signIn = new AccountsSignInReply.fromJson(j['signIn']) ?? new AccountsSignInReply();
    signOut = new AccountsSignOutReply.fromJson(j['signOut']) ?? new AccountsSignOutReply();
    sessions = new AccountsSessionsReply.fromJson(j['sessions']) ?? new AccountsSessionsReply();
    operations = new AccountsOperationsReply.fromJson(j['operations']) ?? new AccountsOperationsReply();
    orgs = new AccountsOrgsReply.fromJson(j['orgs']) ?? new AccountsOrgsReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new AccountsDeleteReply(),
      'insert': insert ?? new AccountsInsertReply(),
      'reset': reset ?? new AccountsResetReply(),
      'verifyEmail': verifyEmail ?? new AccountsVerifyEmailReply(),
      'verifyPhone': verifyPhone ?? new AccountsVerifyPhoneReply(),
      'verifyReset': verifyReset ?? new AccountsVerifyResetReply(),
      'retrieve': retrieve ?? new AccountsRetrieveReply(),
      'update': update ?? new AccountsUpdateReply(),
      'signIn': signIn ?? new AccountsSignInReply(),
      'signOut': signOut ?? new AccountsSignOutReply(),
      'sessions': sessions ?? new AccountsSessionsReply(),
      'operations': operations ?? new AccountsOperationsReply(),
      'orgs': orgs ?? new AccountsOrgsReply(),
    };
  }
}

class AccountsDeleteReply {
  String emtpy = "";

  AccountsDeleteReply([
    this.emtpy,
  ]);

  AccountsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class AccountsInsertReply {
  String insert_id = "";

  AccountsInsertReply([
    this.insert_id,
  ]);

  AccountsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class AccountsResetReply {

  AccountsResetReply();

  AccountsResetReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsVerifyEmailReply {
  String phone = "";

  AccountsVerifyEmailReply([
    this.phone,
  ]);

  AccountsVerifyEmailReply.fromJson(Map j) {
    phone = j['phone'] ?? "";
  }

  Map toJson() {
    return {
      'phone': phone ?? "",
    };
  }
}

class AccountsVerifyPhoneReply {
  String session_id = "";
  String session_token = "";

  AccountsVerifyPhoneReply([
    this.session_id,
    this.session_token,
  ]);

  AccountsVerifyPhoneReply.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
    session_token = j['session_token'] ?? "";
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
      'session_token': session_token ?? "",
    };
  }
}

class AccountsVerifyResetReply {
  String session_id = "";
  String session_token = "";

  AccountsVerifyResetReply([
    this.session_id,
    this.session_token,
  ]);

  AccountsVerifyResetReply.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
    session_token = j['session_token'] ?? "";
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
      'session_token': session_token ?? "",
    };
  }
}

class AccountsRetrieveReply {
  Account account = new Account();

  AccountsRetrieveReply([
    this.account,
  ]);

  AccountsRetrieveReply.fromJson(Map j) {
    account = new Account.fromJson(j['account']) ?? new Account();
  }

  Map toJson() {
    return {
      'account': account ?? new Account(),
    };
  }
}

class AccountsUpdateReply {
  String empty = "";

  AccountsUpdateReply([
    this.empty,
  ]);

  AccountsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class AccountsSignInReply {
  String session_id = "";
  String session_token = "";

  AccountsSignInReply([
    this.session_id,
    this.session_token,
  ]);

  AccountsSignInReply.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
    session_token = j['session_token'] ?? "";
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
      'session_token': session_token ?? "",
    };
  }
}

class AccountsSignOutReply {

  AccountsSignOutReply();

  AccountsSignOutReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AccountsSessionsReply {
  List<AuthSession> sessions = new List<AuthSession>();

  AccountsSessionsReply([
    this.sessions,
  ]);

  AccountsSessionsReply.fromJson(Map j) {
    j['sessions']?.map((item) => new AuthSession.fromJson(item))?.forEach(sessions.add);
  }

  Map toJson() {
    return {
      'sessions': sessions?.map((item) => item.toJson())?.toList() ?? new List<AuthSession>(),
    };
  }
}

class AccountsOperationsReply {
  List<Operation> operations = new List<Operation>();

  AccountsOperationsReply([
    this.operations,
  ]);

  AccountsOperationsReply.fromJson(Map j) {
    j['operations']?.map((item) => new Operation.fromJson(item))?.forEach(operations.add);
  }

  Map toJson() {
    return {
      'operations': operations?.map((item) => item.toJson())?.toList() ?? new List<Operation>(),
    };
  }
}

class AccountsOrgsReply {
  List<Org> orgs = new List<Org>();

  AccountsOrgsReply([
    this.orgs,
  ]);

  AccountsOrgsReply.fromJson(Map j) {
    j['orgs']?.map((item) => new Org.fromJson(item))?.forEach(orgs.add);
  }

  Map toJson() {
    return {
      'orgs': orgs?.map((item) => item.toJson())?.toList() ?? new List<Org>(),
    };
  }
}

