// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class DisksArgs {
  DisksDeleteArgs delete = new DisksDeleteArgs();
  DisksInsertArgs insert = new DisksInsertArgs();
  DisksListArgs list = new DisksListArgs();
  DisksRetrieveArgs retrieve = new DisksRetrieveArgs();
  DisksUpdateArgs update = new DisksUpdateArgs();
  DisksAppsArgs apps = new DisksAppsArgs();

  DisksArgs({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.apps,
  });

  DisksArgs.fromJson(Map j) {
    delete = new DisksDeleteArgs.fromJson(j['delete']) ?? new DisksDeleteArgs();
    insert = new DisksInsertArgs.fromJson(j['insert']) ?? new DisksInsertArgs();
    list = new DisksListArgs.fromJson(j['list']) ?? new DisksListArgs();
    retrieve = new DisksRetrieveArgs.fromJson(j['retrieve']) ?? new DisksRetrieveArgs();
    update = new DisksUpdateArgs.fromJson(j['update']) ?? new DisksUpdateArgs();
    apps = new DisksAppsArgs.fromJson(j['apps']) ?? new DisksAppsArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new DisksDeleteArgs(),
      'insert': insert ?? new DisksInsertArgs(),
      'list': list ?? new DisksListArgs(),
      'retrieve': retrieve ?? new DisksRetrieveArgs(),
      'update': update ?? new DisksUpdateArgs(),
      'apps': apps ?? new DisksAppsArgs(),
    };
  }
}

class DisksDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";

  DisksDeleteArgs([
    this.org_id,
    this.pool_id,
    this.disk_id,
  ]);

  DisksDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_id': disk_id ?? "",
    };
  }
}

class DisksInsertArgs {
  String org_id = "";
  String pool_id = "";
  String name = "";
  int size_gb = 0;
  String disk_dir = "";
  String description = "";

  DisksInsertArgs([
    this.org_id,
    this.pool_id,
    this.name,
    this.size_gb,
    this.disk_dir,
    this.description,
  ]);

  DisksInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    name = j['name'] ?? "";
    size_gb = j['size_gb'] ?? 0;
    disk_dir = j['disk_dir'] ?? "";
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'name': name ?? "",
      'size_gb': size_gb ?? 0,
      'disk_dir': disk_dir ?? "",
      'description': description ?? "",
    };
  }
}

class DisksListArgs {
  String org_id = "";
  String pool_id = "";
  List<String> disk_types = new List<String>();

  DisksListArgs([
    this.org_id,
    this.pool_id,
    this.disk_types,
  ]);

  DisksListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    j['disk_types']?.forEach((val) => disk_types.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_types': disk_types ?? new List<String>(),
    };
  }
}

class DisksRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";

  DisksRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.disk_id,
  ]);

  DisksRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_id': disk_id ?? "",
    };
  }
}

class DisksUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";
  Disk disk = new Disk();

  DisksUpdateArgs([
    this.org_id,
    this.pool_id,
    this.disk_id,
    this.disk,
  ]);

  DisksUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
    disk = new Disk.fromJson(j['disk']) ?? new Disk();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_id': disk_id ?? "",
      'disk': disk ?? new Disk(),
    };
  }
}

class DisksAppsArgs {
  String org_id = "";
  String pool_id = "";
  String disk_id = "";

  DisksAppsArgs([
    this.org_id,
    this.pool_id,
    this.disk_id,
  ]);

  DisksAppsArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'disk_id': disk_id ?? "",
    };
  }
}

class DisksReply {
  DisksDeleteReply delete = new DisksDeleteReply();
  DisksInsertReply insert = new DisksInsertReply();
  DisksListReply list = new DisksListReply();
  DisksRetrieveReply retrieve = new DisksRetrieveReply();
  DisksUpdateReply update = new DisksUpdateReply();
  DisksAppsReply apps = new DisksAppsReply();

  DisksReply({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.apps,
  });

  DisksReply.fromJson(Map j) {
    delete = new DisksDeleteReply.fromJson(j['delete']) ?? new DisksDeleteReply();
    insert = new DisksInsertReply.fromJson(j['insert']) ?? new DisksInsertReply();
    list = new DisksListReply.fromJson(j['list']) ?? new DisksListReply();
    retrieve = new DisksRetrieveReply.fromJson(j['retrieve']) ?? new DisksRetrieveReply();
    update = new DisksUpdateReply.fromJson(j['update']) ?? new DisksUpdateReply();
    apps = new DisksAppsReply.fromJson(j['apps']) ?? new DisksAppsReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new DisksDeleteReply(),
      'insert': insert ?? new DisksInsertReply(),
      'list': list ?? new DisksListReply(),
      'retrieve': retrieve ?? new DisksRetrieveReply(),
      'update': update ?? new DisksUpdateReply(),
      'apps': apps ?? new DisksAppsReply(),
    };
  }
}

class DisksDeleteReply {
  String emtpy = "";

  DisksDeleteReply([
    this.emtpy,
  ]);

  DisksDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class DisksInsertReply {
  String insert_id = "";

  DisksInsertReply([
    this.insert_id,
  ]);

  DisksInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class DisksListReply {
  List<Disk> disks = new List<Disk>();

  DisksListReply([
    this.disks,
  ]);

  DisksListReply.fromJson(Map j) {
    j['disks']?.map((item) => new Disk.fromJson(item))?.forEach(disks.add);
  }

  Map toJson() {
    return {
      'disks': disks?.map((item) => item.toJson())?.toList() ?? new List<Disk>(),
    };
  }
}

class DisksRetrieveReply {
  Disk disk = new Disk();

  DisksRetrieveReply([
    this.disk,
  ]);

  DisksRetrieveReply.fromJson(Map j) {
    disk = new Disk.fromJson(j['disk']) ?? new Disk();
  }

  Map toJson() {
    return {
      'disk': disk ?? new Disk(),
    };
  }
}

class DisksUpdateReply {
  String empty = "";

  DisksUpdateReply([
    this.empty,
  ]);

  DisksUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class DisksAppsReply {
  List<App> apps = new List<App>();

  DisksAppsReply([
    this.apps,
  ]);

  DisksAppsReply.fromJson(Map j) {
    j['apps']?.map((item) => new App.fromJson(item))?.forEach(apps.add);
  }

  Map toJson() {
    return {
      'apps': apps?.map((item) => item.toJson())?.toList() ?? new List<App>(),
    };
  }
}

