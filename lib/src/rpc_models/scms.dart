// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class ScmsArgs {
  ScmsProviderConnectArgs providerConnect = new ScmsProviderConnectArgs();
  ScmsProviderInsertArgs providerInsert = new ScmsProviderInsertArgs();
  ScmsProviderListArgs providerList = new ScmsProviderListArgs();
  ScmsProviderDeleteArgs providerDelete = new ScmsProviderDeleteArgs();
  ScmsReposInsertArgs reposInsert = new ScmsReposInsertArgs();
  ScmsReposListArgs reposList = new ScmsReposListArgs();
  ScmsRepoBranchesListArgs repoBranchesList = new ScmsRepoBranchesListArgs();
  ScmsRepoTagsListArgs repoTagsList = new ScmsRepoTagsListArgs();

  ScmsArgs({
    this.providerConnect,
    this.providerInsert,
    this.providerList,
    this.providerDelete,
    this.reposInsert,
    this.reposList,
    this.repoBranchesList,
    this.repoTagsList,
  });

  ScmsArgs.fromJson(Map j) {
    providerConnect = new ScmsProviderConnectArgs.fromJson(j['providerConnect']) ?? new ScmsProviderConnectArgs();
    providerInsert = new ScmsProviderInsertArgs.fromJson(j['providerInsert']) ?? new ScmsProviderInsertArgs();
    providerList = new ScmsProviderListArgs.fromJson(j['providerList']) ?? new ScmsProviderListArgs();
    providerDelete = new ScmsProviderDeleteArgs.fromJson(j['providerDelete']) ?? new ScmsProviderDeleteArgs();
    reposInsert = new ScmsReposInsertArgs.fromJson(j['reposInsert']) ?? new ScmsReposInsertArgs();
    reposList = new ScmsReposListArgs.fromJson(j['reposList']) ?? new ScmsReposListArgs();
    repoBranchesList = new ScmsRepoBranchesListArgs.fromJson(j['repoBranchesList']) ?? new ScmsRepoBranchesListArgs();
    repoTagsList = new ScmsRepoTagsListArgs.fromJson(j['repoTagsList']) ?? new ScmsRepoTagsListArgs();
  }

  Map toJson() {
    return {
      'providerConnect': providerConnect ?? new ScmsProviderConnectArgs(),
      'providerInsert': providerInsert ?? new ScmsProviderInsertArgs(),
      'providerList': providerList ?? new ScmsProviderListArgs(),
      'providerDelete': providerDelete ?? new ScmsProviderDeleteArgs(),
      'reposInsert': reposInsert ?? new ScmsReposInsertArgs(),
      'reposList': reposList ?? new ScmsReposListArgs(),
      'repoBranchesList': repoBranchesList ?? new ScmsRepoBranchesListArgs(),
      'repoTagsList': repoTagsList ?? new ScmsRepoTagsListArgs(),
    };
  }
}

class ScmsProviderConnectArgs {
  String provider = "";
  bool client_redirect = false;

  ScmsProviderConnectArgs([
    this.provider,
    this.client_redirect,
  ]);

  ScmsProviderConnectArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    client_redirect = j['client_redirect'] ?? false;
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'client_redirect': client_redirect ?? false,
    };
  }
}

class ScmsProviderInsertArgs {
  String provider = "";
  String code = "";
  String state = "";

  ScmsProviderInsertArgs([
    this.provider,
    this.code,
    this.state,
  ]);

  ScmsProviderInsertArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    code = j['code'] ?? "";
    state = j['state'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'code': code ?? "",
      'state': state ?? "",
    };
  }
}

class ScmsProviderListArgs {

  ScmsProviderListArgs();

  ScmsProviderListArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class ScmsProviderDeleteArgs {
  String provider = "";

  ScmsProviderDeleteArgs([
    this.provider,
  ]);

  ScmsProviderDeleteArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
    };
  }
}

class ScmsReposInsertArgs {
  String provider = "";
  String repo_owner = "";
  String repo_name = "";
  bool private = false;

  ScmsReposInsertArgs([
    this.provider,
    this.repo_owner,
    this.repo_name,
    this.private,
  ]);

  ScmsReposInsertArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    repo_owner = j['repo_owner'] ?? "";
    repo_name = j['repo_name'] ?? "";
    private = j['private'] ?? false;
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'repo_owner': repo_owner ?? "",
      'repo_name': repo_name ?? "",
      'private': private ?? false,
    };
  }
}

class ScmsReposListArgs {
  String provider = "";
  String repo_owner = "";

  ScmsReposListArgs([
    this.provider,
    this.repo_owner,
  ]);

  ScmsReposListArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    repo_owner = j['repo_owner'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'repo_owner': repo_owner ?? "",
    };
  }
}

class ScmsRepoBranchesListArgs {
  String provider = "";
  String repo_owner = "";
  String repo_name = "";

  ScmsRepoBranchesListArgs([
    this.provider,
    this.repo_owner,
    this.repo_name,
  ]);

  ScmsRepoBranchesListArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    repo_owner = j['repo_owner'] ?? "";
    repo_name = j['repo_name'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'repo_owner': repo_owner ?? "",
      'repo_name': repo_name ?? "",
    };
  }
}

class ScmsRepoTagsListArgs {
  String provider = "";
  String repo_owner = "";
  String repo_name = "";

  ScmsRepoTagsListArgs([
    this.provider,
    this.repo_owner,
    this.repo_name,
  ]);

  ScmsRepoTagsListArgs.fromJson(Map j) {
    provider = j['provider'] ?? "";
    repo_owner = j['repo_owner'] ?? "";
    repo_name = j['repo_name'] ?? "";
  }

  Map toJson() {
    return {
      'provider': provider ?? "",
      'repo_owner': repo_owner ?? "",
      'repo_name': repo_name ?? "",
    };
  }
}

class ScmsReply {
  ScmsProviderConnectReply providerConnect = new ScmsProviderConnectReply();
  ScmsProviderInsertReply providerInsert = new ScmsProviderInsertReply();
  ScmsProviderListReply providerList = new ScmsProviderListReply();
  ScmsProviderDeleteReply providerDelete = new ScmsProviderDeleteReply();
  ScmsReposInsertReply reposInsert = new ScmsReposInsertReply();
  ScmsReposListReply reposList = new ScmsReposListReply();
  ScmsRepoBranchesListReply repoBranchesList = new ScmsRepoBranchesListReply();
  ScmsRepoTagsListReply repoTagsList = new ScmsRepoTagsListReply();
  ScmsScmInsertDummyReply insert = new ScmsScmInsertDummyReply();

  ScmsReply({
    this.providerConnect,
    this.providerInsert,
    this.providerList,
    this.providerDelete,
    this.reposInsert,
    this.reposList,
    this.repoBranchesList,
    this.repoTagsList,
    this.insert,
  });

  ScmsReply.fromJson(Map j) {
    providerConnect = new ScmsProviderConnectReply.fromJson(j['providerConnect']) ?? new ScmsProviderConnectReply();
    providerInsert = new ScmsProviderInsertReply.fromJson(j['providerInsert']) ?? new ScmsProviderInsertReply();
    providerList = new ScmsProviderListReply.fromJson(j['providerList']) ?? new ScmsProviderListReply();
    providerDelete = new ScmsProviderDeleteReply.fromJson(j['providerDelete']) ?? new ScmsProviderDeleteReply();
    reposInsert = new ScmsReposInsertReply.fromJson(j['reposInsert']) ?? new ScmsReposInsertReply();
    reposList = new ScmsReposListReply.fromJson(j['reposList']) ?? new ScmsReposListReply();
    repoBranchesList = new ScmsRepoBranchesListReply.fromJson(j['repoBranchesList']) ?? new ScmsRepoBranchesListReply();
    repoTagsList = new ScmsRepoTagsListReply.fromJson(j['repoTagsList']) ?? new ScmsRepoTagsListReply();
    insert = new ScmsScmInsertDummyReply.fromJson(j['insert']) ?? new ScmsScmInsertDummyReply();
  }

  Map toJson() {
    return {
      'providerConnect': providerConnect ?? new ScmsProviderConnectReply(),
      'providerInsert': providerInsert ?? new ScmsProviderInsertReply(),
      'providerList': providerList ?? new ScmsProviderListReply(),
      'providerDelete': providerDelete ?? new ScmsProviderDeleteReply(),
      'reposInsert': reposInsert ?? new ScmsReposInsertReply(),
      'reposList': reposList ?? new ScmsReposListReply(),
      'repoBranchesList': repoBranchesList ?? new ScmsRepoBranchesListReply(),
      'repoTagsList': repoTagsList ?? new ScmsRepoTagsListReply(),
      'insert': insert ?? new ScmsScmInsertDummyReply(),
    };
  }
}

class ScmsProviderConnectReply {
  String redirect_uri = "";
  bool client_redirect = false;

  ScmsProviderConnectReply([
    this.redirect_uri,
    this.client_redirect,
  ]);

  ScmsProviderConnectReply.fromJson(Map j) {
    redirect_uri = j['redirect_uri'] ?? "";
    client_redirect = j['client_redirect'] ?? false;
  }

  Map toJson() {
    return {
      'redirect_uri': redirect_uri ?? "",
      'client_redirect': client_redirect ?? false,
    };
  }
}

class ScmsProviderInsertReply {
  String redirect_uri = "";

  ScmsProviderInsertReply([
    this.redirect_uri,
  ]);

  ScmsProviderInsertReply.fromJson(Map j) {
    redirect_uri = j['redirect_uri'] ?? "";
  }

  Map toJson() {
    return {
      'redirect_uri': redirect_uri ?? "",
    };
  }
}

class ScmsProviderListReply {
  List<String> providers = new List<String>();

  ScmsProviderListReply([
    this.providers,
  ]);

  ScmsProviderListReply.fromJson(Map j) {
    j['providers']?.forEach((val) => providers.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'providers': providers ?? new List<String>(),
    };
  }
}

class ScmsProviderDeleteReply {

  ScmsProviderDeleteReply();

  ScmsProviderDeleteReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class ScmsReposInsertReply {
  String insert_id = "";

  ScmsReposInsertReply([
    this.insert_id,
  ]);

  ScmsReposInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class ScmsReposListReply {
  List<ScmRepo> repos = new List<ScmRepo>();

  ScmsReposListReply([
    this.repos,
  ]);

  ScmsReposListReply.fromJson(Map j) {
    j['repos']?.map((item) => new ScmRepo.fromJson(item))?.forEach(repos.add);
  }

  Map toJson() {
    return {
      'repos': repos?.map((item) => item.toJson())?.toList() ?? new List<ScmRepo>(),
    };
  }
}

class ScmsRepoBranchesListReply {
  List<String> branches = new List<String>();

  ScmsRepoBranchesListReply([
    this.branches,
  ]);

  ScmsRepoBranchesListReply.fromJson(Map j) {
    j['branches']?.forEach((val) => branches.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'branches': branches ?? new List<String>(),
    };
  }
}

class ScmsRepoTagsListReply {
  List<String> tags = new List<String>();

  ScmsRepoTagsListReply([
    this.tags,
  ]);

  ScmsRepoTagsListReply.fromJson(Map j) {
    j['tags']?.forEach((val) => tags.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'tags': tags ?? new List<String>(),
    };
  }
}

class ScmsScmInsertDummyReply {
  String insert_id = "";

  ScmsScmInsertDummyReply([
    this.insert_id,
  ]);

  ScmsScmInsertDummyReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

