// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class ServiceAccountsArgs {
  ServiceAccountsDeleteArgs delete = new ServiceAccountsDeleteArgs();
  ServiceAccountsInsertArgs insert = new ServiceAccountsInsertArgs();
  ServiceAccountsRetrieveArgs retrieve = new ServiceAccountsRetrieveArgs();
  ServiceAccountsListArgs list = new ServiceAccountsListArgs();
  ServiceAccountsUpdateArgs update = new ServiceAccountsUpdateArgs();
  ServiceAccountsCreateSessionArgs createSession = new ServiceAccountsCreateSessionArgs();
  ServiceAccountsDeleteSessionArgs deleteSession = new ServiceAccountsDeleteSessionArgs();
  ServiceAccountsOperationsArgs operations = new ServiceAccountsOperationsArgs();

  ServiceAccountsArgs({
    this.delete,
    this.insert,
    this.retrieve,
    this.list,
    this.update,
    this.createSession,
    this.deleteSession,
    this.operations,
  });

  ServiceAccountsArgs.fromJson(Map j) {
    delete = new ServiceAccountsDeleteArgs.fromJson(j['delete']) ?? new ServiceAccountsDeleteArgs();
    insert = new ServiceAccountsInsertArgs.fromJson(j['insert']) ?? new ServiceAccountsInsertArgs();
    retrieve = new ServiceAccountsRetrieveArgs.fromJson(j['retrieve']) ?? new ServiceAccountsRetrieveArgs();
    list = new ServiceAccountsListArgs.fromJson(j['list']) ?? new ServiceAccountsListArgs();
    update = new ServiceAccountsUpdateArgs.fromJson(j['update']) ?? new ServiceAccountsUpdateArgs();
    createSession = new ServiceAccountsCreateSessionArgs.fromJson(j['createSession']) ?? new ServiceAccountsCreateSessionArgs();
    deleteSession = new ServiceAccountsDeleteSessionArgs.fromJson(j['deleteSession']) ?? new ServiceAccountsDeleteSessionArgs();
    operations = new ServiceAccountsOperationsArgs.fromJson(j['operations']) ?? new ServiceAccountsOperationsArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServiceAccountsDeleteArgs(),
      'insert': insert ?? new ServiceAccountsInsertArgs(),
      'retrieve': retrieve ?? new ServiceAccountsRetrieveArgs(),
      'list': list ?? new ServiceAccountsListArgs(),
      'update': update ?? new ServiceAccountsUpdateArgs(),
      'createSession': createSession ?? new ServiceAccountsCreateSessionArgs(),
      'deleteSession': deleteSession ?? new ServiceAccountsDeleteSessionArgs(),
      'operations': operations ?? new ServiceAccountsOperationsArgs(),
    };
  }
}

class ServiceAccountsDeleteArgs {
  String org_id = "";
  String service_account_id = "";

  ServiceAccountsDeleteArgs([
    this.org_id,
    this.service_account_id,
  ]);

  ServiceAccountsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
    };
  }
}

class ServiceAccountsInsertArgs {
  String org_id = "";
  String name = "";

  ServiceAccountsInsertArgs([
    this.org_id,
    this.name,
  ]);

  ServiceAccountsInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    name = j['name'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'name': name ?? "",
    };
  }
}

class ServiceAccountsRetrieveArgs {
  String org_id = "";
  String service_account_id = "";

  ServiceAccountsRetrieveArgs([
    this.org_id,
    this.service_account_id,
  ]);

  ServiceAccountsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
    };
  }
}

class ServiceAccountsListArgs {
  String org_id = "";

  ServiceAccountsListArgs([
    this.org_id,
  ]);

  ServiceAccountsListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class ServiceAccountsUpdateArgs {
  String org_id = "";
  String service_account_id = "";
  ServiceAccount service_account = new ServiceAccount();

  ServiceAccountsUpdateArgs([
    this.org_id,
    this.service_account_id,
    this.service_account,
  ]);

  ServiceAccountsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    service_account = new ServiceAccount.fromJson(j['service_account']) ?? new ServiceAccount();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'service_account': service_account ?? new ServiceAccount(),
    };
  }
}

class ServiceAccountsCreateSessionArgs {
  String org_id = "";
  String service_account_id = "";
  Duration ttl = new Duration();

  ServiceAccountsCreateSessionArgs([
    this.org_id,
    this.service_account_id,
    this.ttl,
  ]);

  ServiceAccountsCreateSessionArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    ttl = (j['ttl'] != null) ? new Duration(microseconds: ((j['ttl'] / 1000) as double).floor()) : new Duration();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'ttl': (ttl != null) ? ttl.inMicroseconds * 1000 : new Duration().inMicroseconds * 1000,
    };
  }
}

class ServiceAccountsDeleteSessionArgs {
  String org_id = "";
  String service_account_id = "";
  String session_id = "";

  ServiceAccountsDeleteSessionArgs([
    this.org_id,
    this.service_account_id,
    this.session_id,
  ]);

  ServiceAccountsDeleteSessionArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    session_id = j['session_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'session_id': session_id ?? "",
    };
  }
}

class ServiceAccountsOperationsArgs {
  String org_id = "";
  String service_account_id = "";
  int limit = 0;
  int skip = 0;

  ServiceAccountsOperationsArgs([
    this.org_id,
    this.service_account_id,
    this.limit,
    this.skip,
  ]);

  ServiceAccountsOperationsArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    service_account_id = j['service_account_id'] ?? "";
    limit = j['limit'] ?? 0;
    skip = j['skip'] ?? 0;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'service_account_id': service_account_id ?? "",
      'limit': limit ?? 0,
      'skip': skip ?? 0,
    };
  }
}

class ServiceAccountsReply {
  ServiceAccountsDeleteReply delete = new ServiceAccountsDeleteReply();
  ServiceAccountsInsertReply insert = new ServiceAccountsInsertReply();
  ServiceAccountsRetrieveReply retrieve = new ServiceAccountsRetrieveReply();
  ServiceAccountsListReply list = new ServiceAccountsListReply();
  ServiceAccountsUpdateReply update = new ServiceAccountsUpdateReply();
  ServiceAccountsCreateSessionReply createSession = new ServiceAccountsCreateSessionReply();
  ServiceAccountsDeleteSessionReply deleteSession = new ServiceAccountsDeleteSessionReply();
  ServiceAccountsOperationsReply operations = new ServiceAccountsOperationsReply();

  ServiceAccountsReply({
    this.delete,
    this.insert,
    this.retrieve,
    this.list,
    this.update,
    this.createSession,
    this.deleteSession,
    this.operations,
  });

  ServiceAccountsReply.fromJson(Map j) {
    delete = new ServiceAccountsDeleteReply.fromJson(j['delete']) ?? new ServiceAccountsDeleteReply();
    insert = new ServiceAccountsInsertReply.fromJson(j['insert']) ?? new ServiceAccountsInsertReply();
    retrieve = new ServiceAccountsRetrieveReply.fromJson(j['retrieve']) ?? new ServiceAccountsRetrieveReply();
    list = new ServiceAccountsListReply.fromJson(j['list']) ?? new ServiceAccountsListReply();
    update = new ServiceAccountsUpdateReply.fromJson(j['update']) ?? new ServiceAccountsUpdateReply();
    createSession = new ServiceAccountsCreateSessionReply.fromJson(j['createSession']) ?? new ServiceAccountsCreateSessionReply();
    deleteSession = new ServiceAccountsDeleteSessionReply.fromJson(j['deleteSession']) ?? new ServiceAccountsDeleteSessionReply();
    operations = new ServiceAccountsOperationsReply.fromJson(j['operations']) ?? new ServiceAccountsOperationsReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServiceAccountsDeleteReply(),
      'insert': insert ?? new ServiceAccountsInsertReply(),
      'retrieve': retrieve ?? new ServiceAccountsRetrieveReply(),
      'list': list ?? new ServiceAccountsListReply(),
      'update': update ?? new ServiceAccountsUpdateReply(),
      'createSession': createSession ?? new ServiceAccountsCreateSessionReply(),
      'deleteSession': deleteSession ?? new ServiceAccountsDeleteSessionReply(),
      'operations': operations ?? new ServiceAccountsOperationsReply(),
    };
  }
}

class ServiceAccountsDeleteReply {

  ServiceAccountsDeleteReply();

  ServiceAccountsDeleteReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class ServiceAccountsInsertReply {
  String insert_id = "";
  String session_id = "";
  String session_token = "";

  ServiceAccountsInsertReply([
    this.insert_id,
    this.session_id,
    this.session_token,
  ]);

  ServiceAccountsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
    session_id = j['session_id'] ?? "";
    session_token = j['session_token'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
      'session_id': session_id ?? "",
      'session_token': session_token ?? "",
    };
  }
}

class ServiceAccountsRetrieveReply {
  ServiceAccount account = new ServiceAccount();

  ServiceAccountsRetrieveReply([
    this.account,
  ]);

  ServiceAccountsRetrieveReply.fromJson(Map j) {
    account = new ServiceAccount.fromJson(j['account']) ?? new ServiceAccount();
  }

  Map toJson() {
    return {
      'account': account ?? new ServiceAccount(),
    };
  }
}

class ServiceAccountsListReply {
  List<ServiceAccount> accounts = new List<ServiceAccount>();

  ServiceAccountsListReply([
    this.accounts,
  ]);

  ServiceAccountsListReply.fromJson(Map j) {
    j['accounts']?.map((item) => new ServiceAccount.fromJson(item))?.forEach(accounts.add);
  }

  Map toJson() {
    return {
      'accounts': accounts?.map((item) => item.toJson())?.toList() ?? new List<ServiceAccount>(),
    };
  }
}

class ServiceAccountsUpdateReply {

  ServiceAccountsUpdateReply();

  ServiceAccountsUpdateReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class ServiceAccountsCreateSessionReply {
  String session_id = "";
  String session_token = "";

  ServiceAccountsCreateSessionReply([
    this.session_id,
    this.session_token,
  ]);

  ServiceAccountsCreateSessionReply.fromJson(Map j) {
    session_id = j['session_id'] ?? "";
    session_token = j['session_token'] ?? "";
  }

  Map toJson() {
    return {
      'session_id': session_id ?? "",
      'session_token': session_token ?? "",
    };
  }
}

class ServiceAccountsDeleteSessionReply {

  ServiceAccountsDeleteSessionReply();

  ServiceAccountsDeleteSessionReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class ServiceAccountsOperationsReply {
  List<Operation> operations = new List<Operation>();

  ServiceAccountsOperationsReply([
    this.operations,
  ]);

  ServiceAccountsOperationsReply.fromJson(Map j) {
    j['operations']?.map((item) => new Operation.fromJson(item))?.forEach(operations.add);
  }

  Map toJson() {
    return {
      'operations': operations?.map((item) => item.toJson())?.toList() ?? new List<Operation>(),
    };
  }
}

