// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class OrgsArgs {
  OrgsDeleteArgs delete = new OrgsDeleteArgs();
  OrgsInsertArgs insert = new OrgsInsertArgs();
  OrgsRetrieveArgs retrieve = new OrgsRetrieveArgs();
  OrgsUpdateArgs update = new OrgsUpdateArgs();
  OrgsEnableBillingArgs enableBilling = new OrgsEnableBillingArgs();
  OrgsDisableBillingArgs disableBilling = new OrgsDisableBillingArgs();
  OrgsUpdateBillingArgs updateBilling = new OrgsUpdateBillingArgs();
  OrgsUpdateSupportPlanArgs updateSupportPlan = new OrgsUpdateSupportPlanArgs();
  OrgsUsageArgs usage = new OrgsUsageArgs();
  OrgsCreditsBalanceArgs creditsBalance = new OrgsCreditsBalanceArgs();
  OrgsInvoicesArgs invoices = new OrgsInvoicesArgs();
  OrgsInvoiceArgs invoice = new OrgsInvoiceArgs();
  OrgsRnsArgs rns = new OrgsRnsArgs();

  OrgsArgs({
    this.delete,
    this.insert,
    this.retrieve,
    this.update,
    this.enableBilling,
    this.disableBilling,
    this.updateBilling,
    this.updateSupportPlan,
    this.usage,
    this.creditsBalance,
    this.invoices,
    this.invoice,
    this.rns,
  });

  OrgsArgs.fromJson(Map j) {
    delete = new OrgsDeleteArgs.fromJson(j['delete']) ?? new OrgsDeleteArgs();
    insert = new OrgsInsertArgs.fromJson(j['insert']) ?? new OrgsInsertArgs();
    retrieve = new OrgsRetrieveArgs.fromJson(j['retrieve']) ?? new OrgsRetrieveArgs();
    update = new OrgsUpdateArgs.fromJson(j['update']) ?? new OrgsUpdateArgs();
    enableBilling = new OrgsEnableBillingArgs.fromJson(j['enableBilling']) ?? new OrgsEnableBillingArgs();
    disableBilling = new OrgsDisableBillingArgs.fromJson(j['disableBilling']) ?? new OrgsDisableBillingArgs();
    updateBilling = new OrgsUpdateBillingArgs.fromJson(j['updateBilling']) ?? new OrgsUpdateBillingArgs();
    updateSupportPlan = new OrgsUpdateSupportPlanArgs.fromJson(j['updateSupportPlan']) ?? new OrgsUpdateSupportPlanArgs();
    usage = new OrgsUsageArgs.fromJson(j['usage']) ?? new OrgsUsageArgs();
    creditsBalance = new OrgsCreditsBalanceArgs.fromJson(j['creditsBalance']) ?? new OrgsCreditsBalanceArgs();
    invoices = new OrgsInvoicesArgs.fromJson(j['invoices']) ?? new OrgsInvoicesArgs();
    invoice = new OrgsInvoiceArgs.fromJson(j['invoice']) ?? new OrgsInvoiceArgs();
    rns = new OrgsRnsArgs.fromJson(j['rns']) ?? new OrgsRnsArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new OrgsDeleteArgs(),
      'insert': insert ?? new OrgsInsertArgs(),
      'retrieve': retrieve ?? new OrgsRetrieveArgs(),
      'update': update ?? new OrgsUpdateArgs(),
      'enableBilling': enableBilling ?? new OrgsEnableBillingArgs(),
      'disableBilling': disableBilling ?? new OrgsDisableBillingArgs(),
      'updateBilling': updateBilling ?? new OrgsUpdateBillingArgs(),
      'updateSupportPlan': updateSupportPlan ?? new OrgsUpdateSupportPlanArgs(),
      'usage': usage ?? new OrgsUsageArgs(),
      'creditsBalance': creditsBalance ?? new OrgsCreditsBalanceArgs(),
      'invoices': invoices ?? new OrgsInvoicesArgs(),
      'invoice': invoice ?? new OrgsInvoiceArgs(),
      'rns': rns ?? new OrgsRnsArgs(),
    };
  }
}

class OrgsDeleteArgs {
  String org_id = "";

  OrgsDeleteArgs([
    this.org_id,
  ]);

  OrgsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class OrgsInsertArgs {
  String name = "";
  bool personal = false;
  String country = "";
  String url = "";
  String description = "";

  OrgsInsertArgs([
    this.name,
    this.personal,
    this.country,
    this.url,
    this.description,
  ]);

  OrgsInsertArgs.fromJson(Map j) {
    name = j['name'] ?? "";
    personal = j['personal'] ?? false;
    country = j['country'] ?? "";
    url = j['url'] ?? "";
    description = j['description'] ?? "";
  }

  Map toJson() {
    return {
      'name': name ?? "",
      'personal': personal ?? false,
      'country': country ?? "",
      'url': url ?? "",
      'description': description ?? "",
    };
  }
}

class OrgsRetrieveArgs {
  String org_id = "";

  OrgsRetrieveArgs([
    this.org_id,
  ]);

  OrgsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class OrgsUpdateArgs {
  String org_id = "";
  Org org = new Org();

  OrgsUpdateArgs([
    this.org_id,
    this.org,
  ]);

  OrgsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    org = new Org.fromJson(j['org']) ?? new Org();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'org': org ?? new Org(),
    };
  }
}

class OrgsEnableBillingArgs {
  String org_id = "";
  String email = "";
  String token = "";
  String client_ip = "";

  OrgsEnableBillingArgs([
    this.org_id,
    this.email,
    this.token,
    this.client_ip,
  ]);

  OrgsEnableBillingArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    email = j['email'] ?? "";
    token = j['token'] ?? "";
    client_ip = j['client_ip'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'email': email ?? "",
      'token': token ?? "",
      'client_ip': client_ip ?? "",
    };
  }
}

class OrgsDisableBillingArgs {
  String org_id = "";

  OrgsDisableBillingArgs([
    this.org_id,
  ]);

  OrgsDisableBillingArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class OrgsUpdateBillingArgs {
  String org_id = "";
  String email = "";
  String token = "";
  String client_ip = "";

  OrgsUpdateBillingArgs([
    this.org_id,
    this.email,
    this.token,
    this.client_ip,
  ]);

  OrgsUpdateBillingArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    email = j['email'] ?? "";
    token = j['token'] ?? "";
    client_ip = j['client_ip'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'email': email ?? "",
      'token': token ?? "",
      'client_ip': client_ip ?? "",
    };
  }
}

class OrgsUpdateSupportPlanArgs {
  String org_id = "";
  int support_plan = 0;

  OrgsUpdateSupportPlanArgs([
    this.org_id,
    this.support_plan,
  ]);

  OrgsUpdateSupportPlanArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    support_plan = j['support_plan'] ?? 0;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'support_plan': support_plan ?? 0,
    };
  }
}

class OrgsUsageArgs {
  String org_id = "";
  DateTime from = new DateTime(0).toUtc();
  DateTime to = new DateTime(0).toUtc();

  OrgsUsageArgs([
    this.org_id,
    this.from,
    this.to,
  ]);

  OrgsUsageArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    from = (j['from'] != null) ? DateTime.parse(j['from']).toUtc() : new DateTime(0).toUtc();
    to = (j['to'] != null) ? DateTime.parse(j['to']).toUtc() : new DateTime(0).toUtc();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'from': (from != null) ? from.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
      'to': (to != null) ? to.toUtc().toIso8601String() : new DateTime(0).toUtc().toIso8601String(),
    };
  }
}

class OrgsCreditsBalanceArgs {
  String org_id = "";

  OrgsCreditsBalanceArgs([
    this.org_id,
  ]);

  OrgsCreditsBalanceArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class OrgsInvoicesArgs {
  String org_id = "";

  OrgsInvoicesArgs([
    this.org_id,
  ]);

  OrgsInvoicesArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
    };
  }
}

class OrgsInvoiceArgs {
  String org_id = "";
  String invoice_id = "";
  String format = "";

  OrgsInvoiceArgs([
    this.org_id,
    this.invoice_id,
    this.format,
  ]);

  OrgsInvoiceArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    invoice_id = j['invoice_id'] ?? "";
    format = j['format'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'invoice_id': invoice_id ?? "",
      'format': format ?? "",
    };
  }
}

class OrgsRnsArgs {
  String org_id = "";
  String pool = "";
  String resource = "";
  String name = "";

  OrgsRnsArgs([
    this.org_id,
    this.pool,
    this.resource,
    this.name,
  ]);

  OrgsRnsArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool = j['pool'] ?? "";
    resource = j['resource'] ?? "";
    name = j['name'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool': pool ?? "",
      'resource': resource ?? "",
      'name': name ?? "",
    };
  }
}

class OrgsReply {
  OrgsDeleteReply delete = new OrgsDeleteReply();
  OrgsInsertReply insert = new OrgsInsertReply();
  OrgsRetrieveReply retrieve = new OrgsRetrieveReply();
  OrgsUpdateReply update = new OrgsUpdateReply();
  OrgsEnableBillingReply enableBilling = new OrgsEnableBillingReply();
  OrgsDisableBillingReply disableBilling = new OrgsDisableBillingReply();
  OrgsUpdateBillingReply updateBilling = new OrgsUpdateBillingReply();
  OrgsUpdateSupportPlanReply updateSupportPlan = new OrgsUpdateSupportPlanReply();
  OrgsUsageReply usage = new OrgsUsageReply();
  OrgsCreditsBalanceReply creditsBalance = new OrgsCreditsBalanceReply();
  OrgsInvoicesReply invoices = new OrgsInvoicesReply();
  OrgsInvoiceReply invoice = new OrgsInvoiceReply();
  OrgsRnsReply rns = new OrgsRnsReply();

  OrgsReply({
    this.delete,
    this.insert,
    this.retrieve,
    this.update,
    this.enableBilling,
    this.disableBilling,
    this.updateBilling,
    this.updateSupportPlan,
    this.usage,
    this.creditsBalance,
    this.invoices,
    this.invoice,
    this.rns,
  });

  OrgsReply.fromJson(Map j) {
    delete = new OrgsDeleteReply.fromJson(j['delete']) ?? new OrgsDeleteReply();
    insert = new OrgsInsertReply.fromJson(j['insert']) ?? new OrgsInsertReply();
    retrieve = new OrgsRetrieveReply.fromJson(j['retrieve']) ?? new OrgsRetrieveReply();
    update = new OrgsUpdateReply.fromJson(j['update']) ?? new OrgsUpdateReply();
    enableBilling = new OrgsEnableBillingReply.fromJson(j['enableBilling']) ?? new OrgsEnableBillingReply();
    disableBilling = new OrgsDisableBillingReply.fromJson(j['disableBilling']) ?? new OrgsDisableBillingReply();
    updateBilling = new OrgsUpdateBillingReply.fromJson(j['updateBilling']) ?? new OrgsUpdateBillingReply();
    updateSupportPlan = new OrgsUpdateSupportPlanReply.fromJson(j['updateSupportPlan']) ?? new OrgsUpdateSupportPlanReply();
    usage = new OrgsUsageReply.fromJson(j['usage']) ?? new OrgsUsageReply();
    creditsBalance = new OrgsCreditsBalanceReply.fromJson(j['creditsBalance']) ?? new OrgsCreditsBalanceReply();
    invoices = new OrgsInvoicesReply.fromJson(j['invoices']) ?? new OrgsInvoicesReply();
    invoice = new OrgsInvoiceReply.fromJson(j['invoice']) ?? new OrgsInvoiceReply();
    rns = new OrgsRnsReply.fromJson(j['rns']) ?? new OrgsRnsReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new OrgsDeleteReply(),
      'insert': insert ?? new OrgsInsertReply(),
      'retrieve': retrieve ?? new OrgsRetrieveReply(),
      'update': update ?? new OrgsUpdateReply(),
      'enableBilling': enableBilling ?? new OrgsEnableBillingReply(),
      'disableBilling': disableBilling ?? new OrgsDisableBillingReply(),
      'updateBilling': updateBilling ?? new OrgsUpdateBillingReply(),
      'updateSupportPlan': updateSupportPlan ?? new OrgsUpdateSupportPlanReply(),
      'usage': usage ?? new OrgsUsageReply(),
      'creditsBalance': creditsBalance ?? new OrgsCreditsBalanceReply(),
      'invoices': invoices ?? new OrgsInvoicesReply(),
      'invoice': invoice ?? new OrgsInvoiceReply(),
      'rns': rns ?? new OrgsRnsReply(),
    };
  }
}

class OrgsDeleteReply {
  String emtpy = "";

  OrgsDeleteReply([
    this.emtpy,
  ]);

  OrgsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class OrgsInsertReply {
  String insert_id = "";

  OrgsInsertReply([
    this.insert_id,
  ]);

  OrgsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class OrgsRetrieveReply {
  Org org = new Org();

  OrgsRetrieveReply([
    this.org,
  ]);

  OrgsRetrieveReply.fromJson(Map j) {
    org = new Org.fromJson(j['org']) ?? new Org();
  }

  Map toJson() {
    return {
      'org': org ?? new Org(),
    };
  }
}

class OrgsUpdateReply {
  String empty = "";

  OrgsUpdateReply([
    this.empty,
  ]);

  OrgsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class OrgsEnableBillingReply {

  OrgsEnableBillingReply();

  OrgsEnableBillingReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class OrgsDisableBillingReply {

  OrgsDisableBillingReply();

  OrgsDisableBillingReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class OrgsUpdateBillingReply {

  OrgsUpdateBillingReply();

  OrgsUpdateBillingReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class OrgsUpdateSupportPlanReply {
  String empty = "";

  OrgsUpdateSupportPlanReply([
    this.empty,
  ]);

  OrgsUpdateSupportPlanReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class OrgsUsageReply {
  OrgUsage usage = new OrgUsage();

  OrgsUsageReply([
    this.usage,
  ]);

  OrgsUsageReply.fromJson(Map j) {
    usage = new OrgUsage.fromJson(j['usage']) ?? new OrgUsage();
  }

  Map toJson() {
    return {
      'usage': usage ?? new OrgUsage(),
    };
  }
}

class OrgsCreditsBalanceReply {
  int credits_in_usd_bp = 0;

  OrgsCreditsBalanceReply([
    this.credits_in_usd_bp,
  ]);

  OrgsCreditsBalanceReply.fromJson(Map j) {
    credits_in_usd_bp = j['credits_in_usd_bp'] ?? 0;
  }

  Map toJson() {
    return {
      'credits_in_usd_bp': credits_in_usd_bp ?? 0,
    };
  }
}

class OrgsInvoicesReply {
  List<Invoice> invoices = new List<Invoice>();

  OrgsInvoicesReply([
    this.invoices,
  ]);

  OrgsInvoicesReply.fromJson(Map j) {
    j['invoices']?.map((item) => new Invoice.fromJson(item))?.forEach(invoices.add);
  }

  Map toJson() {
    return {
      'invoices': invoices?.map((item) => item.toJson())?.toList() ?? new List<Invoice>(),
    };
  }
}

class OrgsInvoiceReply {
  Invoice invoice = new Invoice();

  OrgsInvoiceReply([
    this.invoice,
  ]);

  OrgsInvoiceReply.fromJson(Map j) {
    invoice = new Invoice.fromJson(j['invoice']) ?? new Invoice();
  }

  Map toJson() {
    return {
      'invoice': invoice ?? new Invoice(),
    };
  }
}

class OrgsRnsReply {
  ResourceIds ids = new ResourceIds();

  OrgsRnsReply([
    this.ids,
  ]);

  OrgsRnsReply.fromJson(Map j) {
    ids = new ResourceIds.fromJson(j['ids']) ?? new ResourceIds();
  }

  Map toJson() {
    return {
      'ids': ids ?? new ResourceIds(),
    };
  }
}

