// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class ContainersArgs {
  ContainersInsertArgs insert = new ContainersInsertArgs();
  ContainersListArgs list = new ContainersListArgs();
  ContainersRetrieveArgs retrieve = new ContainersRetrieveArgs();
  ContainersUpdateArgs update = new ContainersUpdateArgs();
  ContainersExecArgs exec = new ContainersExecArgs();
  ContainersTunnelArgs tunnel = new ContainersTunnelArgs();

  ContainersArgs({
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.exec,
    this.tunnel,
  });

  ContainersArgs.fromJson(Map j) {
    insert = new ContainersInsertArgs.fromJson(j['insert']) ?? new ContainersInsertArgs();
    list = new ContainersListArgs.fromJson(j['list']) ?? new ContainersListArgs();
    retrieve = new ContainersRetrieveArgs.fromJson(j['retrieve']) ?? new ContainersRetrieveArgs();
    update = new ContainersUpdateArgs.fromJson(j['update']) ?? new ContainersUpdateArgs();
    exec = new ContainersExecArgs.fromJson(j['exec']) ?? new ContainersExecArgs();
    tunnel = new ContainersTunnelArgs.fromJson(j['tunnel']) ?? new ContainersTunnelArgs();
  }

  Map toJson() {
    return {
      'insert': insert ?? new ContainersInsertArgs(),
      'list': list ?? new ContainersListArgs(),
      'retrieve': retrieve ?? new ContainersRetrieveArgs(),
      'update': update ?? new ContainersUpdateArgs(),
      'exec': exec ?? new ContainersExecArgs(),
      'tunnel': tunnel ?? new ContainersTunnelArgs(),
    };
  }
}

class ContainersInsertArgs {
  String org_id = "";
  String pool_id = "";
  String runtime = "";
  String version = "";
  String cmd = "";
  List<String> args = new List<String>();

  ContainersInsertArgs([
    this.org_id,
    this.pool_id,
    this.runtime,
    this.version,
    this.cmd,
    this.args,
  ]);

  ContainersInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    runtime = j['runtime'] ?? "";
    version = j['version'] ?? "";
    cmd = j['cmd'] ?? "";
    j['args']?.forEach((val) => args.add(val)); // NOTE: Newly changed for Dart 2.0
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'runtime': runtime ?? "",
      'version': version ?? "",
      'cmd': cmd ?? "",
      'args': args ?? new List<String>(),
    };
  }
}

class ContainersListArgs {
  String org_id = "";
  String pool_id = "";

  ContainersListArgs([
    this.org_id,
    this.pool_id,
  ]);

  ContainersListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class ContainersRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String mate_id = "";

  ContainersRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.mate_id,
  ]);

  ContainersRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    mate_id = j['mate_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'mate_id': mate_id ?? "",
    };
  }
}

class ContainersUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String mate_id = "";
  Container container = new Container();

  ContainersUpdateArgs([
    this.org_id,
    this.pool_id,
    this.mate_id,
    this.container,
  ]);

  ContainersUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    mate_id = j['mate_id'] ?? "";
    container = new Container.fromJson(j['container']) ?? new Container();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'mate_id': mate_id ?? "",
      'container': container ?? new Container(),
    };
  }
}

class ContainersExecArgs {
  String org_id = "";
  String pool_id = "";
  String mate_id = "";
  String cmd = "";
  List<String> args = new List<String>();
  bool interactive = false;
  bool tty = false;
  String workdir = "";
  bool detach = false;

  ContainersExecArgs([
    this.org_id,
    this.pool_id,
    this.mate_id,
    this.cmd,
    this.args,
    this.interactive,
    this.tty,
    this.workdir,
    this.detach,
  ]);

  ContainersExecArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    mate_id = j['mate_id'] ?? "";
    cmd = j['cmd'] ?? "";
    j['args']?.forEach((val) => args.add(val)); // NOTE: Newly changed for Dart 2.0
    interactive = j['interactive'] ?? false;
    tty = j['tty'] ?? false;
    workdir = j['workdir'] ?? "";
    detach = j['detach'] ?? false;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'mate_id': mate_id ?? "",
      'cmd': cmd ?? "",
      'args': args ?? new List<String>(),
      'interactive': interactive ?? false,
      'tty': tty ?? false,
      'workdir': workdir ?? "",
      'detach': detach ?? false,
    };
  }
}

class ContainersTunnelArgs {
  String org_id = "";
  String pool_id = "";
  String mate_id = "";
  int port = 0;

  ContainersTunnelArgs([
    this.org_id,
    this.pool_id,
    this.mate_id,
    this.port,
  ]);

  ContainersTunnelArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    mate_id = j['mate_id'] ?? "";
    port = j['port'] ?? 0;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'mate_id': mate_id ?? "",
      'port': port ?? 0,
    };
  }
}

class ContainersReply {
  ContainersInsertReply insert = new ContainersInsertReply();
  ContainersListReply list = new ContainersListReply();
  ContainersRetrieveReply retrieve = new ContainersRetrieveReply();
  ContainersUpdateReply update = new ContainersUpdateReply();
  ContainersExecReply exec = new ContainersExecReply();
  ContainersTunnelReply tunnel = new ContainersTunnelReply();

  ContainersReply({
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.exec,
    this.tunnel,
  });

  ContainersReply.fromJson(Map j) {
    insert = new ContainersInsertReply.fromJson(j['insert']) ?? new ContainersInsertReply();
    list = new ContainersListReply.fromJson(j['list']) ?? new ContainersListReply();
    retrieve = new ContainersRetrieveReply.fromJson(j['retrieve']) ?? new ContainersRetrieveReply();
    update = new ContainersUpdateReply.fromJson(j['update']) ?? new ContainersUpdateReply();
    exec = new ContainersExecReply.fromJson(j['exec']) ?? new ContainersExecReply();
    tunnel = new ContainersTunnelReply.fromJson(j['tunnel']) ?? new ContainersTunnelReply();
  }

  Map toJson() {
    return {
      'insert': insert ?? new ContainersInsertReply(),
      'list': list ?? new ContainersListReply(),
      'retrieve': retrieve ?? new ContainersRetrieveReply(),
      'update': update ?? new ContainersUpdateReply(),
      'exec': exec ?? new ContainersExecReply(),
      'tunnel': tunnel ?? new ContainersTunnelReply(),
    };
  }
}

class ContainersInsertReply {
  String insert_id = "";

  ContainersInsertReply([
    this.insert_id,
  ]);

  ContainersInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class ContainersListReply {
  List<Container> containers = new List<Container>();

  ContainersListReply([
    this.containers,
  ]);

  ContainersListReply.fromJson(Map j) {
    j['containers']?.map((item) => new Container.fromJson(item))?.forEach(containers.add);
  }

  Map toJson() {
    return {
      'containers': containers?.map((item) => item.toJson())?.toList() ?? new List<Container>(),
    };
  }
}

class ContainersRetrieveReply {
  Container container = new Container();

  ContainersRetrieveReply([
    this.container,
  ]);

  ContainersRetrieveReply.fromJson(Map j) {
    container = new Container.fromJson(j['container']) ?? new Container();
  }

  Map toJson() {
    return {
      'container': container ?? new Container(),
    };
  }
}

class ContainersUpdateReply {
  String empty = "";

  ContainersUpdateReply([
    this.empty,
  ]);

  ContainersUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class ContainersExecReply {
  String ip_address = "";
  int port = 0;
  String token = "";

  ContainersExecReply([
    this.ip_address,
    this.port,
    this.token,
  ]);

  ContainersExecReply.fromJson(Map j) {
    ip_address = j['ip_address'] ?? "";
    port = j['port'] ?? 0;
    token = j['token'] ?? "";
  }

  Map toJson() {
    return {
      'ip_address': ip_address ?? "",
      'port': port ?? 0,
      'token': token ?? "",
    };
  }
}

class ContainersTunnelReply {
  String ip_address = "";
  int port = 0;
  String token = "";

  ContainersTunnelReply([
    this.ip_address,
    this.port,
    this.token,
  ]);

  ContainersTunnelReply.fromJson(Map j) {
    ip_address = j['ip_address'] ?? "";
    port = j['port'] ?? 0;
    token = j['token'] ?? "";
  }

  Map toJson() {
    return {
      'ip_address': ip_address ?? "",
      'port': port ?? 0,
      'token': token ?? "",
    };
  }
}

