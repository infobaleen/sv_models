// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class AppsArgs {
  AppsInsertArgs insert = new AppsInsertArgs();
  AppsListArgs list = new AppsListArgs();
  AppsRetrieveArgs retrieve = new AppsRetrieveArgs();
  AppsUpdateArgs update = new AppsUpdateArgs();
  AppsDeleteArgs delete = new AppsDeleteArgs();
  AppsVerifyDomainArgs verifyDomain = new AppsVerifyDomainArgs();
  AppsAttachDiskArgs attachDisk = new AppsAttachDiskArgs();
  AppsDetachDiskArgs detachDisk = new AppsDetachDiskArgs();
  AppsSwapPathsArgs swapPaths = new AppsSwapPathsArgs();

  AppsArgs({
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.delete,
    this.verifyDomain,
    this.attachDisk,
    this.detachDisk,
    this.swapPaths,
  });

  AppsArgs.fromJson(Map j) {
    insert = new AppsInsertArgs.fromJson(j['insert']) ?? new AppsInsertArgs();
    list = new AppsListArgs.fromJson(j['list']) ?? new AppsListArgs();
    retrieve = new AppsRetrieveArgs.fromJson(j['retrieve']) ?? new AppsRetrieveArgs();
    update = new AppsUpdateArgs.fromJson(j['update']) ?? new AppsUpdateArgs();
    delete = new AppsDeleteArgs.fromJson(j['delete']) ?? new AppsDeleteArgs();
    verifyDomain = new AppsVerifyDomainArgs.fromJson(j['verifyDomain']) ?? new AppsVerifyDomainArgs();
    attachDisk = new AppsAttachDiskArgs.fromJson(j['attachDisk']) ?? new AppsAttachDiskArgs();
    detachDisk = new AppsDetachDiskArgs.fromJson(j['detachDisk']) ?? new AppsDetachDiskArgs();
    swapPaths = new AppsSwapPathsArgs.fromJson(j['swapPaths']) ?? new AppsSwapPathsArgs();
  }

  Map toJson() {
    return {
      'insert': insert ?? new AppsInsertArgs(),
      'list': list ?? new AppsListArgs(),
      'retrieve': retrieve ?? new AppsRetrieveArgs(),
      'update': update ?? new AppsUpdateArgs(),
      'delete': delete ?? new AppsDeleteArgs(),
      'verifyDomain': verifyDomain ?? new AppsVerifyDomainArgs(),
      'attachDisk': attachDisk ?? new AppsAttachDiskArgs(),
      'detachDisk': detachDisk ?? new AppsDetachDiskArgs(),
      'swapPaths': swapPaths ?? new AppsSwapPathsArgs(),
    };
  }
}

class AppsInsertArgs {
  String org_id = "";
  String pool_id = "";
  String name = "";

  AppsInsertArgs([
    this.org_id,
    this.pool_id,
    this.name,
  ]);

  AppsInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    name = j['name'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'name': name ?? "",
    };
  }
}

class AppsListArgs {
  String org_id = "";
  String pool_id = "";

  AppsListArgs([
    this.org_id,
    this.pool_id,
  ]);

  AppsListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
    };
  }
}

class AppsRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";

  AppsRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
  ]);

  AppsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
    };
  }
}

class AppsUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  App app = new App();

  AppsUpdateArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.app,
  ]);

  AppsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    app = new App.fromJson(j['app']) ?? new App();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'app': app ?? new App(),
    };
  }
}

class AppsDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";

  AppsDeleteArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
  ]);

  AppsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
    };
  }
}

class AppsVerifyDomainArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";

  AppsVerifyDomainArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
  ]);

  AppsVerifyDomainArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
    };
  }
}

class AppsAttachDiskArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String disk_id = "";

  AppsAttachDiskArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.disk_id,
  ]);

  AppsAttachDiskArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'disk_id': disk_id ?? "",
    };
  }
}

class AppsDetachDiskArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String disk_id = "";

  AppsDetachDiskArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.disk_id,
  ]);

  AppsDetachDiskArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    disk_id = j['disk_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'disk_id': disk_id ?? "",
    };
  }
}

class AppsSwapPathsArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String path_prefix_x = "";
  String path_prefix_y = "";

  AppsSwapPathsArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.path_prefix_x,
    this.path_prefix_y,
  ]);

  AppsSwapPathsArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    path_prefix_x = j['path_prefix_x'] ?? "";
    path_prefix_y = j['path_prefix_y'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'path_prefix_x': path_prefix_x ?? "",
      'path_prefix_y': path_prefix_y ?? "",
    };
  }
}

class AppsReply {
  AppsInsertReply insert = new AppsInsertReply();
  AppsListReply list = new AppsListReply();
  AppsRetrieveReply retrieve = new AppsRetrieveReply();
  AppsUpdateReply update = new AppsUpdateReply();
  AppsDeleteReply delete = new AppsDeleteReply();
  AppsVerifyDomainReply verifyDomain = new AppsVerifyDomainReply();
  AppsAttachDiskReply attachDisk = new AppsAttachDiskReply();
  AppsDetachDiskReply detachDisk = new AppsDetachDiskReply();
  AppsSwapPathsReply swapPaths = new AppsSwapPathsReply();

  AppsReply({
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.delete,
    this.verifyDomain,
    this.attachDisk,
    this.detachDisk,
    this.swapPaths,
  });

  AppsReply.fromJson(Map j) {
    insert = new AppsInsertReply.fromJson(j['insert']) ?? new AppsInsertReply();
    list = new AppsListReply.fromJson(j['list']) ?? new AppsListReply();
    retrieve = new AppsRetrieveReply.fromJson(j['retrieve']) ?? new AppsRetrieveReply();
    update = new AppsUpdateReply.fromJson(j['update']) ?? new AppsUpdateReply();
    delete = new AppsDeleteReply.fromJson(j['delete']) ?? new AppsDeleteReply();
    verifyDomain = new AppsVerifyDomainReply.fromJson(j['verifyDomain']) ?? new AppsVerifyDomainReply();
    attachDisk = new AppsAttachDiskReply.fromJson(j['attachDisk']) ?? new AppsAttachDiskReply();
    detachDisk = new AppsDetachDiskReply.fromJson(j['detachDisk']) ?? new AppsDetachDiskReply();
    swapPaths = new AppsSwapPathsReply.fromJson(j['swapPaths']) ?? new AppsSwapPathsReply();
  }

  Map toJson() {
    return {
      'insert': insert ?? new AppsInsertReply(),
      'list': list ?? new AppsListReply(),
      'retrieve': retrieve ?? new AppsRetrieveReply(),
      'update': update ?? new AppsUpdateReply(),
      'delete': delete ?? new AppsDeleteReply(),
      'verifyDomain': verifyDomain ?? new AppsVerifyDomainReply(),
      'attachDisk': attachDisk ?? new AppsAttachDiskReply(),
      'detachDisk': detachDisk ?? new AppsDetachDiskReply(),
      'swapPaths': swapPaths ?? new AppsSwapPathsReply(),
    };
  }
}

class AppsInsertReply {
  String insert_id = "";

  AppsInsertReply([
    this.insert_id,
  ]);

  AppsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class AppsListReply {
  List<App> apps = new List<App>();

  AppsListReply([
    this.apps,
  ]);

  AppsListReply.fromJson(Map j) {
    j['apps']?.map((item) => new App.fromJson(item))?.forEach(apps.add);
  }

  Map toJson() {
    return {
      'apps': apps?.map((item) => item.toJson())?.toList() ?? new List<App>(),
    };
  }
}

class AppsRetrieveReply {
  App app = new App();

  AppsRetrieveReply([
    this.app,
  ]);

  AppsRetrieveReply.fromJson(Map j) {
    app = new App.fromJson(j['app']) ?? new App();
  }

  Map toJson() {
    return {
      'app': app ?? new App(),
    };
  }
}

class AppsUpdateReply {
  String empty = "";

  AppsUpdateReply([
    this.empty,
  ]);

  AppsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class AppsDeleteReply {
  String emtpy = "";

  AppsDeleteReply([
    this.emtpy,
  ]);

  AppsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class AppsVerifyDomainReply {

  AppsVerifyDomainReply();

  AppsVerifyDomainReply.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class AppsAttachDiskReply {
  String empty = "";

  AppsAttachDiskReply([
    this.empty,
  ]);

  AppsAttachDiskReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class AppsDetachDiskReply {
  String empty = "";

  AppsDetachDiskReply([
    this.empty,
  ]);

  AppsDetachDiskReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class AppsSwapPathsReply {
  String empty = "";

  AppsSwapPathsReply([
    this.empty,
  ]);

  AppsSwapPathsReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

