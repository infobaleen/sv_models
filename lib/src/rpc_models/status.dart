// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class StatusArgs {
  StatusStatusArgs status = new StatusStatusArgs();

  StatusArgs({
    this.status,
  });

  StatusArgs.fromJson(Map j) {
    status = new StatusStatusArgs.fromJson(j['status']) ?? new StatusStatusArgs();
  }

  Map toJson() {
    return {
      'status': status ?? new StatusStatusArgs(),
    };
  }
}

class StatusStatusArgs {

  StatusStatusArgs();

  StatusStatusArgs.fromJson(Map j) {
  }

  Map toJson() {
    return {
    };
  }
}

class StatusReply {
  StatusStatusReply status = new StatusStatusReply();

  StatusReply({
    this.status,
  });

  StatusReply.fromJson(Map j) {
    status = new StatusStatusReply.fromJson(j['status']) ?? new StatusStatusReply();
  }

  Map toJson() {
    return {
      'status': status ?? new StatusStatusReply(),
    };
  }
}

class StatusStatusReply {
  String status = "";

  StatusStatusReply([
    this.status,
  ]);

  StatusStatusReply.fromJson(Map j) {
    status = j['status'] ?? "";
  }

  Map toJson() {
    return {
      'status': status ?? "",
    };
  }
}

