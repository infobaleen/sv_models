// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class EnvsArgs {
  EnvsAttachMongodbArgs attachMongodb = new EnvsAttachMongodbArgs();
  EnvsDeleteArgs delete = new EnvsDeleteArgs();
  EnvsDetachMongodbArgs detachMongodb = new EnvsDetachMongodbArgs();
  EnvsEnableArgs enable = new EnvsEnableArgs();
  EnvsDisableArgs disable = new EnvsDisableArgs();
  EnvsPrimaryArgs primary = new EnvsPrimaryArgs();
  EnvsConsoleArgs console = new EnvsConsoleArgs();
  EnvsInsertArgs insert = new EnvsInsertArgs();
  EnvsListArgs list = new EnvsListArgs();
  EnvsRetrieveArgs retrieve = new EnvsRetrieveArgs();
  EnvsUpdateArgs update = new EnvsUpdateArgs();
  EnvsDeployWebhookArgs deployWebhook = new EnvsDeployWebhookArgs();
  EnvsDeployScmArgs deployScm = new EnvsDeployScmArgs();
  EnvsDeployArchiveArgs deployArchive = new EnvsDeployArchiveArgs();

  EnvsArgs({
    this.attachMongodb,
    this.delete,
    this.detachMongodb,
    this.enable,
    this.disable,
    this.primary,
    this.console,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.deployWebhook,
    this.deployScm,
    this.deployArchive,
  });

  EnvsArgs.fromJson(Map j) {
    attachMongodb = new EnvsAttachMongodbArgs.fromJson(j['attachMongodb']) ?? new EnvsAttachMongodbArgs();
    delete = new EnvsDeleteArgs.fromJson(j['delete']) ?? new EnvsDeleteArgs();
    detachMongodb = new EnvsDetachMongodbArgs.fromJson(j['detachMongodb']) ?? new EnvsDetachMongodbArgs();
    enable = new EnvsEnableArgs.fromJson(j['enable']) ?? new EnvsEnableArgs();
    disable = new EnvsDisableArgs.fromJson(j['disable']) ?? new EnvsDisableArgs();
    primary = new EnvsPrimaryArgs.fromJson(j['primary']) ?? new EnvsPrimaryArgs();
    console = new EnvsConsoleArgs.fromJson(j['console']) ?? new EnvsConsoleArgs();
    insert = new EnvsInsertArgs.fromJson(j['insert']) ?? new EnvsInsertArgs();
    list = new EnvsListArgs.fromJson(j['list']) ?? new EnvsListArgs();
    retrieve = new EnvsRetrieveArgs.fromJson(j['retrieve']) ?? new EnvsRetrieveArgs();
    update = new EnvsUpdateArgs.fromJson(j['update']) ?? new EnvsUpdateArgs();
    deployWebhook = new EnvsDeployWebhookArgs.fromJson(j['deployWebhook']) ?? new EnvsDeployWebhookArgs();
    deployScm = new EnvsDeployScmArgs.fromJson(j['deployScm']) ?? new EnvsDeployScmArgs();
    deployArchive = new EnvsDeployArchiveArgs.fromJson(j['deployArchive']) ?? new EnvsDeployArchiveArgs();
  }

  Map toJson() {
    return {
      'attachMongodb': attachMongodb ?? new EnvsAttachMongodbArgs(),
      'delete': delete ?? new EnvsDeleteArgs(),
      'detachMongodb': detachMongodb ?? new EnvsDetachMongodbArgs(),
      'enable': enable ?? new EnvsEnableArgs(),
      'disable': disable ?? new EnvsDisableArgs(),
      'primary': primary ?? new EnvsPrimaryArgs(),
      'console': console ?? new EnvsConsoleArgs(),
      'insert': insert ?? new EnvsInsertArgs(),
      'list': list ?? new EnvsListArgs(),
      'retrieve': retrieve ?? new EnvsRetrieveArgs(),
      'update': update ?? new EnvsUpdateArgs(),
      'deployWebhook': deployWebhook ?? new EnvsDeployWebhookArgs(),
      'deployScm': deployScm ?? new EnvsDeployScmArgs(),
      'deployArchive': deployArchive ?? new EnvsDeployArchiveArgs(),
    };
  }
}

class EnvsAttachMongodbArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String database_id = "";

  EnvsAttachMongodbArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
    this.database_id,
  ]);

  EnvsAttachMongodbArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    database_id = j['database_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'database_id': database_id ?? "",
    };
  }
}

class EnvsDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsDeleteArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsDetachMongodbArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String database_id = "";

  EnvsDetachMongodbArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
    this.database_id,
  ]);

  EnvsDetachMongodbArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    database_id = j['database_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'database_id': database_id ?? "",
    };
  }
}

class EnvsEnableArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsEnableArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsEnableArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsDisableArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsDisableArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsDisableArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsPrimaryArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsPrimaryArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsPrimaryArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsConsoleArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsConsoleArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsConsoleArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsInsertArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  NewEnv env = new NewEnv();

  EnvsInsertArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env,
  ]);

  EnvsInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env = new NewEnv.fromJson(j['env']) ?? new NewEnv();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env': env ?? new NewEnv(),
    };
  }
}

class EnvsListArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";

  EnvsListArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
  ]);

  EnvsListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
    };
  }
}

class EnvsRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  Env env = new Env();

  EnvsUpdateArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
    this.env,
  ]);

  EnvsUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    env = new Env.fromJson(j['env']) ?? new Env();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'env': env ?? new Env(),
    };
  }
}

class EnvsDeployWebhookArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String provider = "";
  String webhook_token = "";
  String json_body = "";
  String json_headers = "";

  EnvsDeployWebhookArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.provider,
    this.webhook_token,
    this.json_body,
    this.json_headers,
  ]);

  EnvsDeployWebhookArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    provider = j['provider'] ?? "";
    webhook_token = j['webhook_token'] ?? "";
    json_body = j['json_body'] ?? "";
    json_headers = j['json_headers'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'provider': provider ?? "",
      'webhook_token': webhook_token ?? "",
      'json_body': json_body ?? "",
      'json_headers': json_headers ?? "",
    };
  }
}

class EnvsDeployScmArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";

  EnvsDeployScmArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
  ]);

  EnvsDeployScmArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
    };
  }
}

class EnvsDeployArchiveArgs {
  String org_id = "";
  String pool_id = "";
  String app_id = "";
  String env_id = "";
  String scm_subtype = "";
  String archive_base_64 = "";

  EnvsDeployArchiveArgs([
    this.org_id,
    this.pool_id,
    this.app_id,
    this.env_id,
    this.scm_subtype,
    this.archive_base_64,
  ]);

  EnvsDeployArchiveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    app_id = j['app_id'] ?? "";
    env_id = j['env_id'] ?? "";
    scm_subtype = j['scm_subtype'] ?? "";
    archive_base_64 = j['archive_base_64'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'app_id': app_id ?? "",
      'env_id': env_id ?? "",
      'scm_subtype': scm_subtype ?? "",
      'archive_base_64': archive_base_64 ?? "",
    };
  }
}

class EnvsReply {
  EnvsAttachMongodbReply attachMongodb = new EnvsAttachMongodbReply();
  EnvsDeleteReply delete = new EnvsDeleteReply();
  EnvsDetachMongodbReply detachMongodb = new EnvsDetachMongodbReply();
  EnvsEnableReply enable = new EnvsEnableReply();
  EnvsDisableReply disable = new EnvsDisableReply();
  EnvsPrimaryReply primary = new EnvsPrimaryReply();
  EnvsConsoleReply console = new EnvsConsoleReply();
  EnvsInsertReply insert = new EnvsInsertReply();
  EnvsListReply list = new EnvsListReply();
  EnvsRetrieveReply retrieve = new EnvsRetrieveReply();
  EnvsUpdateReply update = new EnvsUpdateReply();
  EnvsDeployWebhookReply deployWebhook = new EnvsDeployWebhookReply();
  EnvsDeployScmReply deployScm = new EnvsDeployScmReply();
  EnvsDeployArchiveReply deployArchive = new EnvsDeployArchiveReply();

  EnvsReply({
    this.attachMongodb,
    this.delete,
    this.detachMongodb,
    this.enable,
    this.disable,
    this.primary,
    this.console,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.deployWebhook,
    this.deployScm,
    this.deployArchive,
  });

  EnvsReply.fromJson(Map j) {
    attachMongodb = new EnvsAttachMongodbReply.fromJson(j['attachMongodb']) ?? new EnvsAttachMongodbReply();
    delete = new EnvsDeleteReply.fromJson(j['delete']) ?? new EnvsDeleteReply();
    detachMongodb = new EnvsDetachMongodbReply.fromJson(j['detachMongodb']) ?? new EnvsDetachMongodbReply();
    enable = new EnvsEnableReply.fromJson(j['enable']) ?? new EnvsEnableReply();
    disable = new EnvsDisableReply.fromJson(j['disable']) ?? new EnvsDisableReply();
    primary = new EnvsPrimaryReply.fromJson(j['primary']) ?? new EnvsPrimaryReply();
    console = new EnvsConsoleReply.fromJson(j['console']) ?? new EnvsConsoleReply();
    insert = new EnvsInsertReply.fromJson(j['insert']) ?? new EnvsInsertReply();
    list = new EnvsListReply.fromJson(j['list']) ?? new EnvsListReply();
    retrieve = new EnvsRetrieveReply.fromJson(j['retrieve']) ?? new EnvsRetrieveReply();
    update = new EnvsUpdateReply.fromJson(j['update']) ?? new EnvsUpdateReply();
    deployWebhook = new EnvsDeployWebhookReply.fromJson(j['deployWebhook']) ?? new EnvsDeployWebhookReply();
    deployScm = new EnvsDeployScmReply.fromJson(j['deployScm']) ?? new EnvsDeployScmReply();
    deployArchive = new EnvsDeployArchiveReply.fromJson(j['deployArchive']) ?? new EnvsDeployArchiveReply();
  }

  Map toJson() {
    return {
      'attachMongodb': attachMongodb ?? new EnvsAttachMongodbReply(),
      'delete': delete ?? new EnvsDeleteReply(),
      'detachMongodb': detachMongodb ?? new EnvsDetachMongodbReply(),
      'enable': enable ?? new EnvsEnableReply(),
      'disable': disable ?? new EnvsDisableReply(),
      'primary': primary ?? new EnvsPrimaryReply(),
      'console': console ?? new EnvsConsoleReply(),
      'insert': insert ?? new EnvsInsertReply(),
      'list': list ?? new EnvsListReply(),
      'retrieve': retrieve ?? new EnvsRetrieveReply(),
      'update': update ?? new EnvsUpdateReply(),
      'deployWebhook': deployWebhook ?? new EnvsDeployWebhookReply(),
      'deployScm': deployScm ?? new EnvsDeployScmReply(),
      'deployArchive': deployArchive ?? new EnvsDeployArchiveReply(),
    };
  }
}

class EnvsAttachMongodbReply {
  String empty = "";

  EnvsAttachMongodbReply([
    this.empty,
  ]);

  EnvsAttachMongodbReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsDeleteReply {
  String emtpy = "";

  EnvsDeleteReply([
    this.emtpy,
  ]);

  EnvsDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class EnvsDetachMongodbReply {
  String empty = "";

  EnvsDetachMongodbReply([
    this.empty,
  ]);

  EnvsDetachMongodbReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsEnableReply {
  String empty = "";

  EnvsEnableReply([
    this.empty,
  ]);

  EnvsEnableReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsDisableReply {
  String empty = "";

  EnvsDisableReply([
    this.empty,
  ]);

  EnvsDisableReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsPrimaryReply {
  String empty = "";

  EnvsPrimaryReply([
    this.empty,
  ]);

  EnvsPrimaryReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsConsoleReply {
  String database_uri = "";

  EnvsConsoleReply([
    this.database_uri,
  ]);

  EnvsConsoleReply.fromJson(Map j) {
    database_uri = j['database_uri'] ?? "";
  }

  Map toJson() {
    return {
      'database_uri': database_uri ?? "",
    };
  }
}

class EnvsInsertReply {
  String insert_id = "";

  EnvsInsertReply([
    this.insert_id,
  ]);

  EnvsInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class EnvsListReply {
  List<Env> envs = new List<Env>();

  EnvsListReply([
    this.envs,
  ]);

  EnvsListReply.fromJson(Map j) {
    j['envs']?.map((item) => new Env.fromJson(item))?.forEach(envs.add);
  }

  Map toJson() {
    return {
      'envs': envs?.map((item) => item.toJson())?.toList() ?? new List<Env>(),
    };
  }
}

class EnvsRetrieveReply {
  Env env = new Env();

  EnvsRetrieveReply([
    this.env,
  ]);

  EnvsRetrieveReply.fromJson(Map j) {
    env = new Env.fromJson(j['env']) ?? new Env();
  }

  Map toJson() {
    return {
      'env': env ?? new Env(),
    };
  }
}

class EnvsUpdateReply {
  String empty = "";

  EnvsUpdateReply([
    this.empty,
  ]);

  EnvsUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsDeployWebhookReply {
  String empty = "";

  EnvsDeployWebhookReply([
    this.empty,
  ]);

  EnvsDeployWebhookReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsDeployScmReply {
  String empty = "";

  EnvsDeployScmReply([
    this.empty,
  ]);

  EnvsDeployScmReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class EnvsDeployArchiveReply {
  String empty = "";

  EnvsDeployArchiveReply([
    this.empty,
  ]);

  EnvsDeployArchiveReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

