// AUTO GENERATED MODEL CLASS, DO NOT EDIT BY HAND.

part of rpc_models;

class ServicesArgs {
  ServicesDeleteArgs delete = new ServicesDeleteArgs();
  ServicesInsertArgs insert = new ServicesInsertArgs();
  ServicesListArgs list = new ServicesListArgs();
  ServicesRetrieveArgs retrieve = new ServicesRetrieveArgs();
  ServicesUpdateArgs update = new ServicesUpdateArgs();
  ServicesUserArgs user = new ServicesUserArgs();
  ServicesTunnelArgs tunnel = new ServicesTunnelArgs();

  ServicesArgs({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.user,
    this.tunnel,
  });

  ServicesArgs.fromJson(Map j) {
    delete = new ServicesDeleteArgs.fromJson(j['delete']) ?? new ServicesDeleteArgs();
    insert = new ServicesInsertArgs.fromJson(j['insert']) ?? new ServicesInsertArgs();
    list = new ServicesListArgs.fromJson(j['list']) ?? new ServicesListArgs();
    retrieve = new ServicesRetrieveArgs.fromJson(j['retrieve']) ?? new ServicesRetrieveArgs();
    update = new ServicesUpdateArgs.fromJson(j['update']) ?? new ServicesUpdateArgs();
    user = new ServicesUserArgs.fromJson(j['user']) ?? new ServicesUserArgs();
    tunnel = new ServicesTunnelArgs.fromJson(j['tunnel']) ?? new ServicesTunnelArgs();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServicesDeleteArgs(),
      'insert': insert ?? new ServicesInsertArgs(),
      'list': list ?? new ServicesListArgs(),
      'retrieve': retrieve ?? new ServicesRetrieveArgs(),
      'update': update ?? new ServicesUpdateArgs(),
      'user': user ?? new ServicesUserArgs(),
      'tunnel': tunnel ?? new ServicesTunnelArgs(),
    };
  }
}

class ServicesDeleteArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";

  ServicesDeleteArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
  ]);

  ServicesDeleteArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
    };
  }
}

class ServicesInsertArgs {
  String org_id = "";
  String pool_id = "";
  String service_type = "";
  String service_version = "";
  String description = "";
  String resource_name = "";
  int su = 0;

  ServicesInsertArgs([
    this.org_id,
    this.pool_id,
    this.service_type,
    this.service_version,
    this.description,
    this.resource_name,
    this.su,
  ]);

  ServicesInsertArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service_version = j['service_version'] ?? "";
    description = j['description'] ?? "";
    resource_name = j['resource_name'] ?? "";
    su = j['su'] ?? 0;
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_type': service_type ?? "",
      'service_version': service_version ?? "",
      'description': description ?? "",
      'resource_name': resource_name ?? "",
      'su': su ?? 0,
    };
  }
}

class ServicesListArgs {
  String org_id = "";
  String pool_id = "";
  String service_type = "";

  ServicesListArgs([
    this.org_id,
    this.pool_id,
    this.service_type,
  ]);

  ServicesListArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_type = j['service_type'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_type': service_type ?? "",
    };
  }
}

class ServicesRetrieveArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";

  ServicesRetrieveArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
  ]);

  ServicesRetrieveArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
    };
  }
}

class ServicesUpdateArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  Service service = new Service();

  ServicesUpdateArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.service,
  ]);

  ServicesUpdateArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    service = new Service.fromJson(j['service']) ?? new Service();
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'service': service ?? new Service(),
    };
  }
}

class ServicesUserArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";
  String resource_name = "";

  ServicesUserArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
    this.resource_name,
  ]);

  ServicesUserArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
    resource_name = j['resource_name'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
      'resource_name': resource_name ?? "",
    };
  }
}

class ServicesTunnelArgs {
  String org_id = "";
  String pool_id = "";
  String service_id = "";
  String service_type = "";

  ServicesTunnelArgs([
    this.org_id,
    this.pool_id,
    this.service_id,
    this.service_type,
  ]);

  ServicesTunnelArgs.fromJson(Map j) {
    org_id = j['org_id'] ?? "";
    pool_id = j['pool_id'] ?? "";
    service_id = j['service_id'] ?? "";
    service_type = j['service_type'] ?? "";
  }

  Map toJson() {
    return {
      'org_id': org_id ?? "",
      'pool_id': pool_id ?? "",
      'service_id': service_id ?? "",
      'service_type': service_type ?? "",
    };
  }
}

class ServicesReply {
  ServicesDeleteReply delete = new ServicesDeleteReply();
  ServicesInsertReply insert = new ServicesInsertReply();
  ServicesListReply list = new ServicesListReply();
  ServicesRetrieveReply retrieve = new ServicesRetrieveReply();
  ServicesUpdateReply update = new ServicesUpdateReply();
  ServicesUserReply user = new ServicesUserReply();
  ServicesTunnelReply tunnel = new ServicesTunnelReply();

  ServicesReply({
    this.delete,
    this.insert,
    this.list,
    this.retrieve,
    this.update,
    this.user,
    this.tunnel,
  });

  ServicesReply.fromJson(Map j) {
    delete = new ServicesDeleteReply.fromJson(j['delete']) ?? new ServicesDeleteReply();
    insert = new ServicesInsertReply.fromJson(j['insert']) ?? new ServicesInsertReply();
    list = new ServicesListReply.fromJson(j['list']) ?? new ServicesListReply();
    retrieve = new ServicesRetrieveReply.fromJson(j['retrieve']) ?? new ServicesRetrieveReply();
    update = new ServicesUpdateReply.fromJson(j['update']) ?? new ServicesUpdateReply();
    user = new ServicesUserReply.fromJson(j['user']) ?? new ServicesUserReply();
    tunnel = new ServicesTunnelReply.fromJson(j['tunnel']) ?? new ServicesTunnelReply();
  }

  Map toJson() {
    return {
      'delete': delete ?? new ServicesDeleteReply(),
      'insert': insert ?? new ServicesInsertReply(),
      'list': list ?? new ServicesListReply(),
      'retrieve': retrieve ?? new ServicesRetrieveReply(),
      'update': update ?? new ServicesUpdateReply(),
      'user': user ?? new ServicesUserReply(),
      'tunnel': tunnel ?? new ServicesTunnelReply(),
    };
  }
}

class ServicesDeleteReply {
  String emtpy = "";

  ServicesDeleteReply([
    this.emtpy,
  ]);

  ServicesDeleteReply.fromJson(Map j) {
    emtpy = j['emtpy'] ?? "";
  }

  Map toJson() {
    return {
      'emtpy': emtpy ?? "",
    };
  }
}

class ServicesInsertReply {
  String insert_id = "";

  ServicesInsertReply([
    this.insert_id,
  ]);

  ServicesInsertReply.fromJson(Map j) {
    insert_id = j['insert_id'] ?? "";
  }

  Map toJson() {
    return {
      'insert_id': insert_id ?? "",
    };
  }
}

class ServicesListReply {
  List<Service> services = new List<Service>();

  ServicesListReply([
    this.services,
  ]);

  ServicesListReply.fromJson(Map j) {
    j['services']?.map((item) => new Service.fromJson(item))?.forEach(services.add);
  }

  Map toJson() {
    return {
      'services': services?.map((item) => item.toJson())?.toList() ?? new List<Service>(),
    };
  }
}

class ServicesRetrieveReply {
  Service service = new Service();

  ServicesRetrieveReply([
    this.service,
  ]);

  ServicesRetrieveReply.fromJson(Map j) {
    service = new Service.fromJson(j['service']) ?? new Service();
  }

  Map toJson() {
    return {
      'service': service ?? new Service(),
    };
  }
}

class ServicesUpdateReply {
  String empty = "";

  ServicesUpdateReply([
    this.empty,
  ]);

  ServicesUpdateReply.fromJson(Map j) {
    empty = j['empty'] ?? "";
  }

  Map toJson() {
    return {
      'empty': empty ?? "",
    };
  }
}

class ServicesUserReply {
  String service_address = "";
  int service_port = 0;
  String service_username = "";
  String service_password = "";

  ServicesUserReply([
    this.service_address,
    this.service_port,
    this.service_username,
    this.service_password,
  ]);

  ServicesUserReply.fromJson(Map j) {
    service_address = j['service_address'] ?? "";
    service_port = j['service_port'] ?? 0;
    service_username = j['service_username'] ?? "";
    service_password = j['service_password'] ?? "";
  }

  Map toJson() {
    return {
      'service_address': service_address ?? "",
      'service_port': service_port ?? 0,
      'service_username': service_username ?? "",
      'service_password': service_password ?? "",
    };
  }
}

class ServicesTunnelReply {
  String service_address = "";
  int service_port = 0;

  ServicesTunnelReply([
    this.service_address,
    this.service_port,
  ]);

  ServicesTunnelReply.fromJson(Map j) {
    service_address = j['service_address'] ?? "";
    service_port = j['service_port'] ?? 0;
  }

  Map toJson() {
    return {
      'service_address': service_address ?? "",
      'service_port': service_port ?? 0,
    };
  }
}

